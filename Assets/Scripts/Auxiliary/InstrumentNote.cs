﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace PlantParty
{
    /// <summary>
    /// Represents a particular instrument note (refer to specific audio clips). </summary>
    [System.Serializable]
    public class InstrumentNote
    {
        [SerializeField, Tooltip("Musical definition of the note.")]
        MusicalNoteAbsolute info;

        [SerializeField, Tooltip("Instrument AudioClips for the note.")]
        AudioClip[] audioClips;

        /// <summary>
        /// Musical definition of the note. </summary>
        public MusicalNoteAbsolute Info
        {
            get { return info; }
        }

        /// <summary>
        /// Instrument AudioClips for the note. </summary>
        public AudioClip[] AudioClips
        {
            get { return audioClips; }
        }

        /// <summary>
        /// Creates an InstrumentNote instance by shallow copying a specified instance (own audio clips list referring to the same audio clip objects). </summary>
        public InstrumentNote(InstrumentNote instrumentNote)
        {
            info = instrumentNote.info;
            audioClips = (AudioClip[])instrumentNote.audioClips.Clone();
        }

        /// <summary>
        /// Returns a random audio clip or a null if there isn't any. </summary>
        public AudioClip RandomClip()
        {
            return (audioClips.Length > 0 ? audioClips[Random.Range(0, audioClips.Length)] : null);
        }
    }
}
