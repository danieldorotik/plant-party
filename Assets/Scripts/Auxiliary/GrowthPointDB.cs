﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras;
using UnityExtras.PointDB;
using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;
using PlantParty.Definitions;

namespace PlantParty
{
    /// <summary>
    /// Specifies available world-space circular area. </summary>
    public struct GrowthPoint
    {
        public Vector2 position;
        public float maxRadius;
    }

    /// <summary>
    /// Database of available world-space aimed for randomly placing PlantParty.Flower objects into a scene.
    /// Note: Rationale of having an own implementation over the standard Physics2D is a significant performance overhead 
    ///       if a Collider2D component would have been instanced for each growth point. Caching & moving & activating / deactivating 
    ///       each collider on scene load would be an option, but by far less efficient and clean. </summary>
    public class GrowthPointsDB : PointDB<float>
    {
        /// <summary>
        /// Returns true if a given position meets custom requirements. </summary>        
        public delegate bool PositionTestDelegate(Vector2 position);

        PointRecord<float>[] growthPointResultsBuffer;

        const float GrowthPointsDBPartitionSize = .5f;
        const int DefaultMaxRadiusSearchCount = 10;

        /// <summary>
        /// Initializes a GrowthPointsDB instance. Optionally it is possible to specify a PointDB partition size (refer to a PointDB documentation). </summary>        
        public GrowthPointsDB(float partitionSize = GrowthPointsDBPartitionSize) : base(partitionSize)
        {
            const int growthPointResultsBufferSize = 7000;
            growthPointResultsBuffer = new PointRecord<float>[growthPointResultsBufferSize];
        }

        /// <summary>
        /// Retrieves a random growth point from a specified area or null if there is not any.
        /// Repeats random selection maxRadiusSearchCount times in order to retrieve a random value with the largest maxRadius.
        /// Optionally it is possible to filter complying results by providing a custom PositionTestDelegate.
        ///   - allows for an expensive operation, as it is called for a minimum necessary number of times
        ///   - due to this optimization a very restrictive filter can cause the first complying value to be returned instead of a random one </summary>
        public GrowthPoint? GetGrowthPoint(Area2 searchArea, PositionTestDelegate positionTest = null, int maxRadiusSearchCount = DefaultMaxRadiusSearchCount)
        {
            int resultCount = Auxiliaries.QueryAutoExpand(results => QueryOverlapAreaNonAlloc(searchArea, results), ref growthPointResultsBuffer);
            if (resultCount == 0)
                return null;

            PointRecord<float> validRecord;
            if (positionTest != null)
            {
                // Note: Deferred nature of LINQ query will lead to calling the positionTest delegate only as many times as it is needed to find a first complying record.
                validRecord = growthPointResultsBuffer.Take(resultCount).FirstOrDefault(record => positionTest(record.Position));

                if (validRecord == null)
                    return null;
            }
            else
                validRecord = growthPointResultsBuffer[Random.Range(0, resultCount)];

            const float invalidGrowthPointRadius = float.MinValue;
            GrowthPoint growthPoint = new GrowthPoint
            {
                maxRadius = invalidGrowthPointRadius
            };

            for (int i = 0; i < maxRadiusSearchCount; i++)
            {
                var randomResult = growthPointResultsBuffer[Random.Range(0, resultCount)];
                if (positionTest != null && !positionTest(randomResult.Position))
                    continue;

                if (growthPoint.maxRadius < randomResult.Value)
                {
                    growthPoint.position = randomResult.Position;
                    growthPoint.maxRadius = randomResult.Value;
                }
            }

            if(Mathf.Approximately(growthPoint.maxRadius, invalidGrowthPointRadius))
            {
                growthPoint.position = validRecord.Position;
                growthPoint.maxRadius = validRecord.Value;
            }

            return growthPoint;
        }

        /// <summary>
        /// Applies specified area occupied by a PlantParty.Flower to the growth point database.
        /// Note: Removes growth points which overlap the specified area and those which are too close to it (as specified by a minFlowerRadius).
        ///       Updates growth points for which the specified area represents the closest occupied space (as specified by a maxFlowerRadius). </summary>
        public void TakeGrowthSpace(Vector2 center, float radius, float minFlowerRadius, float maxFlowerRadius)
        {
            int resultCount = Auxiliaries.QueryAutoExpand(results => QueryOverlapCircleNonAlloc(center, radius + minFlowerRadius, results), ref growthPointResultsBuffer);
            for (int i = 0; i < resultCount; ++i)
            {
                Remove(growthPointResultsBuffer[i]);
            }

            resultCount = Auxiliaries.QueryAutoExpand(results => QueryOverlapCircleNonAlloc(center, radius + maxFlowerRadius, results), ref growthPointResultsBuffer);
            for (int i = 0; i < resultCount; ++i)
            {
                var growthPoint = growthPointResultsBuffer[i];

                float maximumRadius = Vector2.Distance(center, growthPoint.Position) - radius;
                if (growthPoint.Value > maximumRadius)
                    growthPoint.Value = maximumRadius;
            }
        }

        /// <summary>
        /// Copies the database growth points into a provided list. </summary>        
        public void Save(List<StoredGrowthPoint> saveList)
        {
            saveList.Clear();

            int resultCount = Auxiliaries.QueryAutoExpand(QueryAllNonAlloc, ref growthPointResultsBuffer);
            for (int i = 0; i < resultCount; ++i)
            {
                var queryItem = growthPointResultsBuffer[i];

                StoredGrowthPoint growthPoint;
                growthPoint.pos.x = Round(queryItem.Position.x);
                growthPoint.pos.y = Round(queryItem.Position.y);
                growthPoint.rad = Round(queryItem.Value);

                saveList.Add(growthPoint);
            }
        }

        /// <summary>
        /// Reinitializes the growth point database with data from a specified list. </summary>
        public void Load(List<StoredGrowthPoint> loadList)
        {
            RemoveAll();
            
            foreach (var growthPoint in loadList)
            {
                Vector2 position;
                position.x = (float)growthPoint.pos.x;
                position.y = (float)growthPoint.pos.y;
                Add(position, (float)growthPoint.rad);
            }
        }

        /// <summary>
        /// Rounds specified value to a format suitable for serialization (2 decimal places). </summary>
        public static double Round(double value)
        {
            const int roundingPrecision = 2;
            return Math.Round(value, roundingPrecision, MidpointRounding.AwayFromZero);
        }
#if UNITY_EDITOR
        /// <summary>
        /// Reinitializes the growth point database with a newly generated random data.
        /// Details: Generates growth points up to a specified count (if there is enough space) located in a specified area.
        ///          Makes sure that a generated growth point is not closer to the area border than it's specified by a minimumRadius
        ///          and that the growth point is positively validated by a provided PositionTestDelegate. </summary>
        public void GenerateGrowthPoints(int count, Area2 generateArea, PositionTestDelegate positionTest, float minimumRadius, float maximumRadius)
        {
            RemoveAll();
            
            if (generateArea.Min < minimumRadius * 2)
            {
                Debug.LogWarning("Generated 0 growth points, since the generate area is too small for the specified minimumRadius");
                return;
            }

            Area2 growthArea = Area2.ApplyBorder(generateArea, -minimumRadius);

            for (int i = 0; i < count; ++i)
            {
                Vector2? growthPoint = GenerateGrowthPoint(growthArea, positionTest);
                if (growthPoint == null)
                {
                    Debug.LogWarning(string.Format("Cannot generate more than {0} growth points", i));
                    return;
                }

                Vector2 finalGrowthPoint = growthPoint.Value;
                float borderDistance = generateArea.BorderDistance(finalGrowthPoint);

                Add(finalGrowthPoint, (borderDistance > maximumRadius) ? maximumRadius : borderDistance);
            }
        }

        Vector2? GenerateGrowthPoint(Area2 growthArea, PositionTestDelegate positionTest)
        {
            const int growthPointGenMaxTries = 1000;
            const float growthPointGenMinPromixityRadius = .2f;
            int tryCount = 0;

            while (++tryCount <= growthPointGenMaxTries)
            {
                Vector2 growthPoint;
                growthPoint.x = Random.Range(growthArea.BottomLeft.x, growthArea.TopRight.x);
                growthPoint.y = Random.Range(growthArea.BottomLeft.y, growthArea.TopRight.y);

                if (positionTest(growthPoint) && QueryOverlapCircleNonAlloc(growthPoint, growthPointGenMinPromixityRadius, growthPointResultsBuffer) == 0)
                    return growthPoint;
            }

            return null;
        }
#endif
    }
}