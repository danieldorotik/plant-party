﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Linq;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Returns true if a given GameObject meets custom requirements. </summary>
    public delegate bool QueryTestObjectDelegate(GameObject testedObject);

    /// <summary>
    /// Provides a convenient way to query PlayObjects. </summary>
    public class PlayObjectsQuery
    {
        readonly Transform playObjectParent;
        readonly int playObjectMask;
        GameObject[] gameObjectQueryResults;
        Collider2D[] colliderResultsBuffer;
        
        /// <summary>
        /// Initializes a PlayObjectsQuery instance. PlayObjects which will be queried are children of a specified parent GameObject. </summary>
        public PlayObjectsQuery(GameObject playObjectParent)
        {
            if (!playObjectParent)
                throw new UnityException("playObjectParent invalid");

            this.playObjectParent = playObjectParent.transform;

            const string playObjectLayer = "PlayObject";
            playObjectMask = LayerMask.GetMask(playObjectLayer);

            const int gameObjectsQueryDefaultSize = 50;
            gameObjectQueryResults = new GameObject[gameObjectsQueryDefaultSize];
        }

        /// <summary>
        /// Returns average position of a group of PlayObjects which are located in a specified radius around a PlayObject which is closest to a specified position.
        /// If there is no PlayObject, returns a position identical to the one specified. </summary>
        public Vector3 ClosestPlayObjectGroup(Vector3 position, float groupRadius)
        {
            GameObject closestPlayObject;
            if (!(closestPlayObject = ClosestPlayObject(position)))
            {
                Debug.LogError("No play object found");
                return position;
            }

            Vector2 queryAreaCenter = closestPlayObject.transform.position;

            int resultCount = Auxiliaries.QueryAutoExpand(buffer => QueryPlayObjectsNonAlloc(queryAreaCenter, groupRadius, buffer), ref gameObjectQueryResults);
            if (resultCount == 0)
                return queryAreaCenter;

            return gameObjectQueryResults.Take(resultCount).AveragePosition();
        }

        /// <summary>
        /// Returns a PlayObject GameObject which is closest to a specified position or returns null if there is no PlayObject. </summary>
        public GameObject ClosestPlayObject(Vector3 position)
        {
            GameObject closestObject = null;
            float closestObjectDistance = float.MaxValue;

            foreach (Transform child in playObjectParent)
            {
                float sqrMagnitude = (position - child.position).sqrMagnitude;
                if (sqrMagnitude < closestObjectDistance)
                {
                    closestObjectDistance = sqrMagnitude;
                    closestObject = child.gameObject;
                }
            }

            return closestObject;
        }

        /// <summary>
        /// Returns the first PlayObject located in a specified area which meets custom requirements specified by a testDelegate. Returns null if there is no such object. </summary>
        public GameObject QueryPlayObject(Area2 queryArea, QueryTestObjectDelegate testDelegate)
        {
            return QueryPlayObject(testDelegate, buffer => QueryPlayObjectsNonAlloc(queryArea, buffer));
        }

        /// <summary>
        /// Returns the first PlayObject located in a specified area which meets custom requirements specified by a testDelegate. Returns null if there is no such object. </summary>
        public GameObject QueryPlayObject(Vector2 queryAreaCenter, float queryAreaRadius, QueryTestObjectDelegate testDelegate)
        {
            return QueryPlayObject(testDelegate, buffer => QueryPlayObjectsNonAlloc(queryAreaCenter, queryAreaRadius, buffer));
        }

        /// <summary>
        /// Queries PlayObject GameObjects which have a Physics2D collider overlapping a specified area and places them into a specified results array (in a NonAlloc fashion).
        /// Returns the number of PlayObject stored in the results array.        
        /// Note: PlayObject is expected to have only 1 collider in the 'PlayObject' layer, otherwise it will occur multiple times in the results array. </summary>
        int QueryPlayObjectsNonAlloc(Area2 queryArea, GameObject[] results)
        {
            return QueryPlayObjectsNonAlloc(buffer => Physics2D.OverlapAreaNonAlloc(queryArea.BottomLeft, queryArea.TopRight, buffer, playObjectMask), results);
        }

        /// <summary>
        /// Queries PlayObject GameObjects which have a Physics2D collider overlapping a specified area and places them into a specified results array (in a NonAlloc fashion).
        /// Returns the number of PlayObject stored in the results array.        
        /// Note: PlayObject is expected to have only 1 collider in the 'PlayObject' layer, otherwise it will occur multiple times in the results array. </summary>
        int QueryPlayObjectsNonAlloc(Vector2 queryAreaCenter, float queryAreaRadius, GameObject[] results)
        {
            return QueryPlayObjectsNonAlloc(buffer => Physics2D.OverlapCircleNonAlloc(queryAreaCenter, queryAreaRadius, buffer, playObjectMask), results);
        }

        int QueryPlayObjectsNonAlloc(Auxiliaries.QueryDelegate<Collider2D> queryDelegate, GameObject[] results)
        {
            if (colliderResultsBuffer == null)
                InitColliderResultsBuffer();

            int colliderCount = Auxiliaries.QueryAutoExpand(queryDelegate, ref colliderResultsBuffer);

            int playObjectCount = 0;            
            for (int i = 0; i < colliderCount; ++i)
            {
                if (playObjectCount >= results.Length)
                    break;

                Collider2D collider = colliderResultsBuffer[i];

                if (collider && collider.transform.parent)
                {
                    results[playObjectCount] = collider.transform.parent.gameObject;
                    playObjectCount++;
                }
                else
                {
                    string invalidObjectName = collider ? collider.ToString() : "Unknown";
                    Debug.LogWarning(string.Format("PlayObject collider '{0}' has no parent GameObject", invalidObjectName));
                }
            }

            return playObjectCount;
        }

        GameObject QueryPlayObject(QueryTestObjectDelegate testDelegate, Auxiliaries.QueryDelegate<GameObject> queryDelegate)
        {
            int resultCount = Auxiliaries.QueryAutoExpand(queryDelegate, ref gameObjectQueryResults);
            for (int i = 0; i < resultCount; ++i)
            {
                if (testDelegate(gameObjectQueryResults[i]))
                    return gameObjectQueryResults[i];
            }

            return null;
        }

        void InitColliderResultsBuffer()
        {
            const int colliderResultsDefaultSize = 100;
            colliderResultsBuffer = new Collider2D[colliderResultsDefaultSize];
        }
    }
}