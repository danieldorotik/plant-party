﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Definitions;
using UnityEngine;
using UnityExtras;

namespace PlantParty
{
    /// <summary>
    /// Contains information regarding the currently playing music track, including its AudioSource status. </summary>
    public class PlayingTrack
    {
        /// <summary>
        /// Specifies a music track tempo (as beats per minute). </summary>
        public double BPM
        {
            get { return musicTrack.bpm; }
        }

        /// <summary>
        /// Specifies a music track tempo offset (in seconds). </summary>
        public double BPMOffset
        {
            get { return musicTrack.bpmOffset; }
        }

        /// <summary>
        /// Specifies a music track playback start time (accordingly to the AudioSettings.dspTime). </summary>
        public double StartTime
        {
            get { return startTime; }
        }

        /// <summary>
        /// Specifies a music track scale. </summary>
        public MusicalScale MusicalScale
        {
            get { return musicTrack.scale; }
        }

        /// <summary>
        /// Specifies a music track key. </summary>
        public MusicalNote MusicalKey
        {
            get { return musicTrack.key; }
        }

        readonly AudioSource audioSource;
        readonly MusicTrack musicTrack;
        readonly double startTime;

        /// <summary>
        /// Initializes a new instance which uses a specified AudioSource and has specified track information. </summary>
        public PlayingTrack(AudioSource audioSource, MusicTrack musicTrack, double startTime)
        {
            if (!audioSource)
                Debug.LogWarning(string.Format(Constants.DebugMessages.ParameterNull, typeof(AudioSource)));

            this.audioSource = audioSource;

            if (!musicTrack)
            {
                Debug.LogWarning(string.Format(Constants.DebugMessages.ParameterNullDefault, typeof(MusicTrack)));
                this.musicTrack = new MusicTrack();
            }
            else
                this.musicTrack = musicTrack;

            this.startTime = startTime;
        }

        /// <summary>
        /// Returns true if track's AudioSource is currently playing.
        /// Note: Paused AudioSource is considered as playing. </summary>
        public bool IsPlaying()
        {
            if (!audioSource)
                return false;

            // Paused AudioSource always returns false to isPlaying, which is especially a problem during the Application unpause frame.
            // Since there is currently no way to find out if AudioSource is paused, best solution is to check whether its playback position jumped back to a start.
            return (audioSource.isPlaying || !Mathf.Approximately(audioSource.time, 0f));
        }
    }
}