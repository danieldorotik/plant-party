﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
namespace PlantParty.GameCollections
{
    /// <summary>
    /// Serves as a GameCollectionItem wrapper, allowing safe collection item manipulation when returned by GameCollection. </summary>
    public class GameCollectionRecord
    {
        /// <summary>
        /// Specifies a GameCollectible used by the collection record. </summary>
        public GameCollectible Collectible
        {
            get { return collectionItem.item; }
        }

        /// <summary>
        /// Returns true if the record is unlocked in its GameCollection. </summary>
        public bool Unlocked
        {
            get { return collectionItem.unlocked; }
        }

        /// <summary>
        /// Returns true if the record is marked as featured in its GameCollection.
        /// Gives possibility to change the value. </summary>
        public bool Featured
        {
            get { return collectionItem.featured; }
            set { collectionItem.featured = value; }
        }

        readonly GameCollectionItem collectionItem;

        /// <summary>
        /// Initializes a GameCollectionRecord. </summary>
        public GameCollectionRecord(GameCollectionItem item)
        {
            collectionItem = item;
        }
    }
}