﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace PlantParty
{
    /// <summary>
    /// Represents a chromatic scale pitch. </summary>
    public enum MusicalNote
    {
        C, CSharp, D, DSharp, E, F, FSharp, G, GSharp, A, ASharp, B
    }

    /// <summary>
    /// Specifies a musical pitch defined using the scientific pitch notation. </summary>
    [System.Serializable]
    public struct MusicalNoteAbsolute
    {
        [SerializeField, Tooltip("Specifies a chromatic scale pitch.")]
        MusicalNote note;

        [SerializeField, Tooltip("Specifies a musical octave.")]
        int octave;

        /// <summary>
        /// Specifies a chromatic scale pitch. </summary>
        public MusicalNote Note
        {
            get { return note; }
        }

        /// <summary>
        /// Specifies a musical octave. </summary>
        public int Octave
        {
            get { return octave; }
        }

        /// <summary>
        /// Creates a scientific pitch. </summary>
        public MusicalNoteAbsolute(MusicalNote note, int octave)
        {
            this.note = note;
            this.octave = octave;
        }
    }

    /// <summary>
    /// Specifies a musical scale. </summary>
    public enum MusicalScale
    {
        Major, MajorPentatonic, Minor, MinorPentatonic
    }
}