﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.GameCollections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityExtras;

namespace PlantParty.Data
{
    /// <summary>
    /// Represents a stored object for GameCollection.
    /// Handles GameCollection-specific serialization / deserialization, where the GameCollection is not stored directly
    /// due to its GameCollectionItem / GameCollectible object references.  </summary>
    [CreateAssetMenu(menuName = Constants.AssetMenu.StoredObject + "StoredGameCollection")]
    public class StoredGameCollection : StoredObject<GameCollection>
    {
        [Serializable]
        struct CollectibleData
        {
            public string collectibleName;
            public bool unlocked;
        }

        [Serializable]
        class CollectionData
        {
            public List<CollectibleData> items;
        }

        protected override void SaveData(GameCollection data, IDataSource source)
        {
            if (source == null)
                throw new UnityException(DatasourceMissing);

            CollectionData collectionData = new CollectionData {items = new List<CollectibleData>()};

            foreach (var collectionItem in data.Items)
            {
                if (!collectionItem.item)
                {
                    Debug.LogWarning(string.Format("Collection item '{0}' is missing a GameCollectible. Skipped its saving.", collectionItem));
                    continue;
                }

                CollectibleData collectibleData;
                collectibleData.collectibleName = collectionItem.item.name;
                collectibleData.unlocked = collectionItem.unlocked;

                collectionData.items.Add(collectibleData);
            }

            source.Save(collectionData);
        }

        protected override GameCollection LoadData(IDataSource source, bool loadDefault = false)
        {
            GameCollection gameCollection = Instantiate(OriginalObject);

            if (!loadDefault)
                LoadCollectionData(gameCollection, source);

            gameCollection.Initialize();

            return gameCollection;
        }

        static void LoadCollectionData(GameCollection gameCollection, IDataSource source)
        {
            if (source == null)
                throw new UnityException(DatasourceMissing);

            if (!source.Exists())
                return;

            CollectionData collectionData = (CollectionData)source.Load(typeof(CollectionData));
            if (collectionData == null || collectionData.items == null)
                throw new UnityException("Loaded collection data invalid");

            Dictionary<string, CollectibleData> collectibleDataLookup = new Dictionary<string, CollectibleData>();
            foreach (var collectibleData in collectionData.items)
            {
                if (!collectibleDataLookup.ContainsKey(collectibleData.collectibleName))
                    collectibleDataLookup.Add(collectibleData.collectibleName, collectibleData);
                else
                    Debug.LogWarning(string.Format("Collectible '{0}' occurs multiple times in the collection data. Duplicity skipped.", collectibleData.collectibleName));
            }

            foreach (var collectionItem in gameCollection.Items)
            {
                CollectibleData collectibleData;
                if (collectionItem.item && collectibleDataLookup.TryGetValue(collectionItem.item.name, out collectibleData))
                    collectionItem.unlocked = collectibleData.unlocked;
            }
        }

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static StoredGameCollection FallbackInstance()
        {
            StoredGameCollection storedGameCollection = CreateInstance<StoredGameCollection>();
            storedGameCollection.OriginalObject = CreateInstance<GameCollection>();

            return storedGameCollection;
        }
    }
}