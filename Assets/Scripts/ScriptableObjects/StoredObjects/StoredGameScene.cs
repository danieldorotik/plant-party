﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Definitions;
using PlantParty.GameCollections;
using UnityEngine;
using UnityExtras;
using UnityExtras.Services;

namespace PlantParty.Data
{
    /// <summary>
    /// Represents a stored object for GameScene.
    /// Handles GameScene-specific preparation for serialization / initialization after deserialization.
    /// In scope of UNITY_EDITOR is able to operate the scene generation feature of GameScene.  </summary>
    [CreateAssetMenu(menuName = Constants.AssetMenu.StoredObject + "StoredGameScene")]
    public class StoredGameScene : StoredObject<GameScene>
    {
        [SerializeField, Tooltip("Specifies configuration for the stored game scene.")]
        StoredGameSceneDefinition storedGameSceneDefinition;
#if UNITY_EDITOR
        [SerializeField, Tooltip("Specifies whether the originalObject data shall be re-generated and dataSource data wiped before loading a GameScene instance.\nFor development purposes only.")]
        bool generateSceneData;
#endif
        protected override void SaveData(GameScene data, IDataSource source)
        {
            data.Save();
            base.SaveData(data, source);
        }

        protected override GameScene LoadData(IDataSource source, bool loadDefault = false)
        {
#if UNITY_EDITOR
            if (generateSceneData || storedGameSceneDefinition && storedGameSceneDefinition.forceGenerateSceneData)
            {
                SafeInvoke(GenerateSceneData);
                loadDefault = true;
            }
#endif
            GameScene scene = base.LoadData(source, loadDefault);
            InitializeScene(scene, storedGameSceneDefinition);
            return scene;
        }

        static void InitializeScene(GameScene scene, StoredGameSceneDefinition definition)
        {
            LinkBackground(scene);
            LinkPlayObjectsParent(scene);
            scene.AddActivatedObject(scene.SceneObjectsParent = GameScene.GetEmptyObject(GameScene.SceneObjectsParentName));

            if (definition)
            {
                scene.CollectionsManager = definition.gameCollectionsManagerProvider.AsServiceProvider<GameCollectionsManager>();
                scene.FallbackPlayObject = definition.fallbackPlayObject;
            }
            else
                Debug.LogWarning("Cannot initialize the scene completely since a definition is missing.");

            scene.Initialize();
        }

        static void LinkBackground(GameScene scene)
        {
            GameObject backgroundObject = GameObject.FindWithTag(GameBackground.Tag);
            if (backgroundObject)
            {
                scene.BackgroundRenderer = backgroundObject.GetComponent<SpriteRenderer>();
                scene.BackgroundScript = backgroundObject.GetComponent<GameBackground>();
                scene.AddActivatedObject(backgroundObject);
            }
            else
                Debug.LogWarning("Cannot find the Background object");
        }

        static void LinkPlayObjectsParent(GameScene scene)
        {
            bool useBackground = (scene.BackgroundScript && scene.BackgroundScript.PlayObjectsParent);
            scene.PlayObjectsParent = (useBackground ? scene.BackgroundScript.PlayObjectsParent : GameScene.GetEmptyObject(GameScene.PlayObjectsParentName));
            scene.AddActivatedObject(scene.PlayObjectsParent);
        }
#if UNITY_EDITOR
        void GenerateSceneData()
        {
            if(!OriginalObject)
            {
                Debug.LogError("Original stored scene object missing");
                return;
            }

            if(!storedGameSceneDefinition || !storedGameSceneDefinition.generateSceneFlowerDefinition)
            {
                Debug.LogError("Missing generate scene flower definition");
                return;
            }

            LinkBackground(OriginalObject);
            LinkPlayObjectsParent(OriginalObject);
            OriginalObject.CollectionsManager = storedGameSceneDefinition.gameCollectionsManagerProvider.AsServiceProvider<GameCollectionsManager>();
            OriginalObject.GenerateScene(storedGameSceneDefinition.generateSceneFlowerDefinition);

            IDataSource source = (IDataSource)DataSource;
            if(source != null)
                source.Clear();
            else
                Debug.LogWarning("Cannot clear the data source since it is not available");
        }
#endif
    }
}