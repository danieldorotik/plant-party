﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using UnityEditor;

namespace PlantParty
{
    /// <summary>
    /// Represents a custom editor for the FakeTouch class. Simplifies its use in the Unity Editor. </summary>
    [CustomEditor(typeof(FakeTouch))]
    public class FakeTouchEditor : Editor
    {
        bool showSerialized = true;
        bool showNonSerialized = true;

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            FakeTouch fakeTouch = (FakeTouch)target;

            showSerialized = EditorGUILayout.Foldout(showSerialized, "Serialized properties (modifiable only in Editor)");
            if (showSerialized)
            {
                RecordedChange(fakeTouch, fakeTouch.DefaultActive, (text, value) => EditorGUILayout.Toggle(text, value), fakeTouch.ChangeDefaultActive, "Default Active");
                RecordedChange(fakeTouch, fakeTouch.FingerId, (text, value) => EditorGUILayout.DelayedIntField(text, value), fakeTouch.ChangeFingerId, "FingerId");
            }

            showNonSerialized = EditorGUILayout.Foldout(showNonSerialized, "Non-serialized properties");
            if (showNonSerialized)
            {
                fakeTouch.Active = EditorGUILayout.Toggle("Active", fakeTouch.Active);
                fakeTouch.Pressed = EditorGUILayout.Toggle("Pressed", fakeTouch.Pressed);
                fakeTouch.Position = EditorGUILayout.Vector2Field("Position", fakeTouch.Position);
            }

            Repaint();
        }

        static void RecordedChange<TObject, TValue>(TObject changedObject, TValue value, Func<string, TValue, TValue> changeFunction, Action<TValue> setAction, string info) where TObject : UnityEngine.Object
        {
            TValue newValue = changeFunction(info, value);
            if (newValue.Equals(value))
                return;

            Undo.RecordObject(changedObject, string.Format("Set {0}: {1}", info, newValue));
            setAction(newValue);                
            EditorUtility.SetDirty(changedObject);
        }
    }
}