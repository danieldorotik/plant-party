﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Identifies a FeaturedEffect emission shape. </summary>
    public enum FeaturedEffectEmitShape
    {
        Circle,
        Rectangle
    }

    /// <summary>
    /// Represents a FeaturedEffect emission configuration. </summary>
    [System.Serializable]
    public class FeaturedEffectEmission
    {
        /// <summary>
        /// Specifies a shape of an emission area. </summary>
        public FeaturedEffectEmitShape shape;

        /// <summary>
        /// Specifies dimensions of an emission area. </summary>
        public Vector2 shapeDimensions;

        /// <summary>
        /// Specifies allowed deviation for a random emission origin position from the configured emission shape. </summary>
        public Span shapeSpan;

        /// <summary>
        /// Specifies allowed degree angle span for a random emission origin position in case of a Circle emission shape. </summary>
        public Span circleShapeAngleSpan;

        /// <summary>
        /// Specifies an emission frequency. </summary>
        public float frequency;

        /// <summary>
        /// Returns an emission period calculated from a configured frequency. </summary>
        public float EmissionPeriod()
        {
            float emissionPeriod = 1 / frequency;
            if (emissionPeriod.IsFinite())
                return emissionPeriod;

            const float fallbackEmissionFrequency = .2f;
            Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "emission frequency", frequency, fallbackEmissionFrequency));
            return 1 / fallbackEmissionFrequency;
        }

        /// <summary>
        /// Returns a random origin position for a particle, based on a FeaturedEffectEmission instance configuration. </summary>
        public Vector2 RandomOrigin()
        {
            if (shape == FeaturedEffectEmitShape.Circle)
                return RandomCircleOrigin();

            if (shape == FeaturedEffectEmitShape.Rectangle)
                return RandomRectangleOrigin();

            Debug.LogError(string.Format("Unsupported emission shape {0}", shape));
            return Vector2.zero;
        }

        Vector2 RandomCircleOrigin()
        {
            float radius = shapeDimensions.Max();
            radius += shapeSpan.RandomContained();

            if (radius < 0f)
                radius = 0f;

            return Auxiliaries.DirectionNormalized(circleShapeAngleSpan.RandomContained()) * radius;
        }

        Vector2 RandomRectangleOrigin()
        {
            float x = Random.Range(-Constants.Arithmetic.Half, Constants.Arithmetic.Half);
            float y = RandomBool() ? Constants.Arithmetic.Half : -Constants.Arithmetic.Half;

            if (RandomBool())
            {
                float swap = x;
                x = y;
                y = swap;
            }

            Area2 shapeSpanArea = Area2.ApplyBorder(new Area2(shapeDimensions), shapeSpan.RandomContained());
            return Vector2.Scale(new Vector2(x, y), shapeSpanArea.Size);
        }

        static bool RandomBool()
        {
            return (Random.Range(0, 2) == 1);
        }
    }

    /// <summary>
    /// Represents a FeaturedEffect particle configuration. </summary>
    [System.Serializable]
    public class FeaturedEffectParticle
    {
        public Span scaleSpan, lifetimeSpan, speedSpan;
        public AnimationCurve lifetimeSpeed, lifetimeTransparency;
    }

    /// <summary>
    /// Represents a FeaturedEffect object configuration. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "FeaturedEffectDefinition")]
    public class FeaturedEffectDefinition : ScriptableObject
    {
        /// <summary>
        /// Specifies a FeaturedEffect emission configuration. </summary>
        public FeaturedEffectEmission emission;

        /// <summary>
        /// Specifies a FeaturedEffect particle configuration. </summary>
        public FeaturedEffectParticle particle;
#if UNITY_EDITOR
        /// <summary>
        /// Specifies whether debug information for a FeaturedEffect shall be drawn in the Unity Editor. </summary>
        public bool debugDrawParticleSpan;
#endif
        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static FeaturedEffectDefinition FallbackInstance()
        {
            FeaturedEffectDefinition definition = CreateInstance<FeaturedEffectDefinition>();
            definition.emission = new FeaturedEffectEmission();
            definition.particle = new FeaturedEffectParticle
            {
                lifetimeSpeed = new AnimationCurve(),
                lifetimeTransparency = new AnimationCurve()
            };
            return definition;
        }
    }
}