﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace PlantParty
{
    /// <summary>
    /// Represents a simulated touch, intended to be assigned to the GameInputModule. 
    /// Can be arbitrarily driven and will cause Unity Events to be invoked as if it was a real input.
    /// The FakeTouchEditor custom editor for this class simplifies its use in the Unity Editor. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "FakeTouch")]
    public class FakeTouch : ScriptableObject
    {
        /// <summary>
        /// Returns true if a FakeTouch is activated upon its creation.
        /// Only Active fake touches can be regarded as Pressed.
        /// Note: This returns a serialized member, editable only through the FakeTouchEditor custom editor. </summary>
        public bool DefaultActive
        {
            get { return defaultActive; }
        }

        /// <summary>
        /// Specifies a unique touch index.
        /// This value is used for the Unity Events pointer registration, thus it is recommended to be smaller than -3.
        /// Note: This returns a serialized member, editable only through the FakeTouchEditor custom editor. </summary>
        public int FingerId
        {
            get { return fingerId; }
        }

        /// <summary>
        /// Returns / sets an activated status of a FakeTouch. Only Active fake touches can be regarded as Pressed. </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Returns / sets a pressed status of a FakeTouch. </summary>
        public bool Pressed { get; set; }

        /// <summary>
        /// Returns / sets a position of a FakeTouch. </summary>
        public Vector2 Position { get; set; }

#if UNITY_EDITOR
        /// <summary>
        /// Updates the defaultActive serialized member. Intended for the FakeTouchEditor. </summary>
        public void ChangeDefaultActive(bool value)
        {
            defaultActive = value;
        }

        /// <summary>
        /// Updates the fingerId serialized member. Intended for the FakeTouchEditor. </summary>
        public void ChangeFingerId(int value)
        {
            fingerId = value;
        }
#endif
        [SerializeField, HideInInspector] bool defaultActive;
        [SerializeField, HideInInspector] int fingerId;

        private void Awake()
        {
            Active = DefaultActive;
        }
    }
}
