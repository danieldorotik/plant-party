﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Collections.Generic;
using UnityEngine;
using UnityExtras;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Represents a BeatSway object configuration. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "BeatSway")]
    public class BeatSwayDefinition : ScriptableObject
    {
        /// <summary>
        /// Specifies a maximum euler angle which us used in both the positive and negative direction when randomly choosing a default rotation. </summary>
        [Range(0f, 90f)]
        public float maxDefaultRotation;

        /// <summary>
        /// Specifies a span from which a rotation span is randomly chosen. </summary>
        [SpanRange(0f, 90f)]
        public Span minMaxRotationSpan;

        /// <summary>
        /// Specifies an AnimationCurve used during the evaluation of a resulting BeatSway rotation. </summary>
        public AnimationCurve animationCurve;

        /// <summary>
        /// Specifies an AnimationCurve used for influencing the BeatSway rotation by a track change event. </summary>
        public AnimationCurve trackChangeCurve;

        /// <summary>
        /// Specifies the duration of a track change animation. </summary>
        public float trackChangeDuration;

        /// <summary>
        /// Specifies the allowed beat multipliers which can be randomly used by a BeatSway instance. </summary>
        public List<float> beatMultipliers;

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static BeatSwayDefinition FallbackInstance()
        {
            BeatSwayDefinition definition = CreateInstance<BeatSwayDefinition>();
            definition.animationCurve = new AnimationCurve();
            definition.trackChangeCurve = new AnimationCurve();
            definition.beatMultipliers = new List<float>();
            return definition;
        }
    }
}