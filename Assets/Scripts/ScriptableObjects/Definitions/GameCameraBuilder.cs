﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityEngine.EventSystems;
using UnityExtras;

namespace PlantParty
{
    /// <summary>
    /// Represents a GameCamera injector, instantiating the returned object by cloning a configured prefab.
    /// Always succeeds to return a usable PlantParty GameCamera, even if the builder is incorrectly configured. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "GameCameraBuilder")]
    public class GameCameraBuilder : ScriptableObject
    {
        [SerializeField, Tooltip("Specifies a prefab to be used for cloning a returned instance.")]
        GameObject gameCameraPrefab;
        
        /// <summary>
        /// Clones a configured camera prefab and returns its GameCamera component. The cloned object is parented to a specified parent.
        /// Note: Always succeeds to return a usable PlantParty GameCamera, even if the builder is incorrectly configured. </summary>
        public GameCamera Build(Transform parent)
        {
            GameObject objectInstance;

            if (!gameCameraPrefab)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, "gameCameraPrefab"));

                const string gameCameraName = "GameCamera";
                objectInstance = new GameObject(gameCameraName);
                objectInstance.transform.parent = parent;

                const float fallbackPosition = -10f;
                objectInstance.transform.position = Vector3.forward * fallbackPosition;   
            }
            else
                objectInstance = Instantiate(gameCameraPrefab, parent);

            if (!objectInstance.GetComponent<Camera>())
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotFoundDefault, typeof(Camera)));
                objectInstance.AddComponent<Camera>();
            }

            Camera cam = objectInstance.GetComponent<Camera>();
            if(!cam.orthographic)
            {
                Debug.LogWarning("The camera component is not set to orthographic. Fixing...");
                cam.orthographic = true;
            }

            if (!objectInstance.GetComponent<Physics2DRaycaster>())
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotFoundDefault, typeof(Physics2DRaycaster)));
                objectInstance.AddComponent<Physics2DRaycaster>();
            }

            if (!objectInstance.GetComponent<GameCamera>())
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotFoundDefault, typeof(GameCamera)));
                objectInstance.AddComponent<GameCamera>();
            }

            return objectInstance.GetComponent<GameCamera>();
        }

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static GameCameraBuilder FallbackInstance()
        {
            return CreateInstance<GameCameraBuilder>();
        }
    }
}