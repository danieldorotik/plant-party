﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.GameCollections;
using UnityEngine;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Specifies configuration of a StoredGameScene instance (useful as a shared configuration). </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "StoredGameScene")]
    public class StoredGameSceneDefinition : ScriptableObject
    {
        /// <summary>
        /// GameCollectionsManager provider to be passed to a GameScene instance. </summary>
        public Object gameCollectionsManagerProvider;

        /// <summary>
        /// Specifies a fallback PlayObjectCollectible to be passed to a GameScene instance. </summary>
        public PlayObjectCollectible fallbackPlayObject;
#if UNITY_EDITOR
        /// <summary>
        /// Specifies a FlowerDefinition required for the game scene data generation. </summary>
        public FlowerDefinition generateSceneFlowerDefinition;

        /// <summary>
        /// Specifies an override for the generateSceneData preference of a StoredGameScene instance. </summary>
        public bool forceGenerateSceneData;
#endif
    }
}