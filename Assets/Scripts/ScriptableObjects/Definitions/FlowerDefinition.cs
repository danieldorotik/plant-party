﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Specifies parameters for the determination of a random grown Flower radius. </summary>
    [System.Serializable]
    public class FlowerRadius
    {
        /// <summary>
        /// Specifies a color used for a Flower radius debug draw in the Unity Editor. </summary>
        public Color debugDrawColor;

        /// <summary>
        /// Specifies a limit for a randomly chosen grown Flower radius. </summary>
        public float minimum, maximum;

        /// <summary>
        /// Specifies a probability distribution for a randomly chosen grown Flower radius. </summary>
        public AnimationCurve probability;        
    }

    /// <summary>
    /// Specifies parameters for the determination of a growing Flower outcome. </summary>
    [System.Serializable]
    public class FlowerGrowth
    {
        /// <summary>
        /// Specifies a value used as the exponential moving average (EMA) time constant.
        /// Note: The immediate user input is filtered using EMA and its result is then used as a flower growth speed. </summary>
        public float speedEMATimeConstant;

        /// <summary>
        /// Influences the outcome of a flower growth progress to its actual radius during its growth. </summary>
        public AnimationCurve progressOutcome;

        /// <summary>
        /// Affects transparency of specified Flower sprites based on its growth progress. </summary>
        public AnimationCurve progressTransparency;
    }

    /// <summary>
    /// Specifies common Flower state parameters. </summary>
    [System.Serializable]
    public class FlowerState
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        /// <summary>
        /// Specifies a color used for the Flower sprites debug coloring in the Unity Editor. </summary>
        public Color debugColor;

        /// <summary>
        /// Specifies whether to use the Flower sprites debug coloring in the Unity Editor. </summary>
        public bool useDebugColor;
#endif
        /// <summary>
        /// Specifies a threshold of a Flower growth progress for switching into this state. </summary>
        public float startGrowthProgress;

        /// <summary>
        /// Specifies a growth speed automatically applied to a Flower in this state. </summary>
        public float growthSpeed;        
    }

    /// <summary>
    /// Specifies a Flower growing state. </summary>
    [System.Serializable]
    public class FlowerStateGrowing : FlowerState
    {
        /// <summary>
        /// Specifies a manual shrink behaviour parameter. </summary>
        public float manualShrinkSpeed, manualShrinkDuration;

        /// <summary>
        /// Specifies an AudioClip played when a Flower reaches its full growth. </summary>
        public AudioClip successEffectClip;
    }

    /// <summary>
    /// Specifies a Flower dying state. </summary>
    [System.Serializable]
    public class FlowerStateDying : FlowerState
    {
        /// <summary>
        /// Specifies a growth progress below which a Flower instance destroys itself. </summary>
        public float dieProgress;
    }

    /// <summary>
    /// Specifies a Flower grown state. </summary>
    [System.Serializable]
    public class FlowerStateGrown
    {
        /// <summary>
        /// Specifies an acceleration of a Flower charge velocity. </summary>
        public AnimationCurve chargeAcceleration;

        /// <summary>
        /// Specifies a duration of a Flower charge acceleration. </summary>
        public float chargeAccelerationDuration;

        /// <summary>
        /// Specifies a maximum Flower charge speed (on its maximum acceleration). </summary>
        public float chargeSpeedMax;

        /// <summary>
        /// Specifies a constant discharge speed. </summary>
        public float dischargeSpeed;

        /// <summary>
        /// Specifies a grown Flower charge capacity. </summary>
        public float capacity;

        /// <summary>
        /// Specifies a constant speed applied to a growing flower when a grown Flower discharges. </summary>
        public float addGrowthSpeed;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
        /// <summary>
        /// Specifies a prefab used for the Flower charge debug visualization using the ProgressBar MonoBehaviour. </summary>
        public GameObject debugCapacityProgressbar;

        /// <summary>
        /// Specifies whether a debug progress bar prefab shall be instanced and used. </summary>
        public bool showDebugCapacityProgresbar;
#endif
    }

    /// <summary>
    /// Specifies states of a Flower object. </summary>
    [System.Serializable]
    public class FlowerStates
    {        
        public FlowerState emerging;
        public FlowerStateGrowing growing;
        public FlowerStateDying dying;
        public FlowerStateGrown grown;        
    }

    /// <summary>
    /// Represents a Flower object configuration. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "Flower")]
    public class FlowerDefinition : ScriptableObject
    {
#if UNITY_EDITOR
        /// <summary>
        /// Specifies a color used for a Flower object collider debug draw in the Unity Editor. </summary>
        public Color debugDrawCollidersColor;
#endif
        /// <summary>
        /// Specifies parameters for the determination of a random grown Flower radius. </summary>
        public FlowerRadius flowerRadius;

        /// <summary>
        /// Specifies parameters for the determination of a growing Flower outcome. </summary>
        public FlowerGrowth flowerGrowth;

        /// <summary>
        /// Specifies states of a Flower object. </summary>
        public FlowerStates flowerStates;

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static FlowerDefinition FallbackInstance()
        {
            FlowerDefinition definition = CreateInstance<FlowerDefinition>();

            definition.flowerRadius = new FlowerRadius
            {
                probability = new AnimationCurve()
            };

            definition.flowerGrowth = new FlowerGrowth
            {
                progressOutcome = new AnimationCurve(),
                progressTransparency = new AnimationCurve()
            };

            definition.flowerStates = new FlowerStates
            {
                emerging = new FlowerState(),
                growing = new FlowerStateGrowing(),
                dying = new FlowerStateDying(),
                grown = new FlowerStateGrown()
            };

            definition.flowerStates.grown.chargeAcceleration = new AnimationCurve();            

            return definition;
        }
    }
}
