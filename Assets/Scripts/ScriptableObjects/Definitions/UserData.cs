﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using UnityEngine;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Specifies user audio preferences. </summary>
    [Serializable]
    public class UserDataAudio
    {
        /// <summary>
        /// Specifies given audio mixer group volume level. </summary>
        public float master, music, instrument;
    }

    /// <summary>
    /// Contains data regarding user progression and preferences. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "UserData")]
    public class UserData : ScriptableObject
    {
        /// <summary>
        /// Number of flowers fully grown during the course of all the user's play sessions.
        /// The progression-related gameplay logic is based on this value. </summary>
        public int grownFlowersCount;

        /// <summary>
        /// Specifies the last entered game scene. </summary>
        public string lastScene;

        /// <summary>
        /// Specifies user audio preferences. </summary>
        public UserDataAudio audio;
    }
}