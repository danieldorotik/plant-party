﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Represents a DischargeEffect object configuration. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "DischargeEffect")]
    public class DischargeEffectDefinition : ScriptableObject
    {
        /// <summary>
        /// Specifies a particle prefab for cloning. </summary>
        public GameObject effectPrefab;

        /// <summary>
        /// Particle speed (units per second). </summary>
        public float speed;

        /// <summary>
        /// Specifies a span for a random particle scale multiplier selection. </summary>
        public Span scaleSpan;

        /// <summary>
        /// Particle rotation speed (degrees per second). </summary>
        public float rotationSpeed;

        /// <summary>
        /// Specifies a number of emitted particles per second. </summary>
        public float emitFrequency;

        /// <summary>
        /// Turned on emission effectively starts after this duration in seconds. </summary>
        public float emitStartAfter;

        /// <summary>
        /// Emitted particle origin position is randomized by a random direction vector of length up to 1f, multiplied by this value. </summary>
        public float emitStartPositionDeviation;
    }
}
