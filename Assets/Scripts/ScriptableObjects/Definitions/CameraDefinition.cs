﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Represents a configuration for the GameCamera scrolling animation. </summary>
    [System.Serializable]
    public class GameCameraScrolling
    {
        public AnimationCurve animationCurve;
        public float startAfterSecs = 3f;

        // units per second
        public float speed = 1f;

        public float minimumTravelDuration = 1f;
    }

    /// <summary>
    /// Represents a configuration for the GameCamera zooming animation. </summary>
    [System.Serializable]
    public class GameCameraZooming
    {
        public AnimationCurve animationCurve;
        public float animationLengthSecs = 1f;

        // Desired size of the zoomed camera area (the smaller dimension) 
        public float desiredZoomedSize = 10f; 
    }

    /// <summary>
    /// Represents a GameCamera object configuration. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "Camera")]
    public class CameraDefinition : ScriptableObject
    {
        /// <summary>
        /// Specifies a configuration for the GameCamera scrolling animation. </summary>
        public GameCameraScrolling autoScrolling;

        /// <summary>
        /// Specifies a configuration for the GameCamera zooming animation. </summary>
        public GameCameraZooming zooming;

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static CameraDefinition FallbackInstance()
        {
            CameraDefinition definition = CreateInstance<CameraDefinition>();
            definition.autoScrolling = new GameCameraScrolling { animationCurve = new AnimationCurve() };
            definition.zooming = new GameCameraZooming { animationCurve = new AnimationCurve() };
            return definition;
        }
    }
}