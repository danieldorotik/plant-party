﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Collections.Generic;
using UnityEngine;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Represents a musical instrument. 
    /// Contains a list of supported notes which refer to specific audio clips 
    /// and contains information regarding its preferred octave. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "Instrument")]
    public class Instrument : ScriptableObject
    {
        [SerializeField, Tooltip("Specifies a preferred octave of the instrument.")]
        int defaultOctave;

        [SerializeField, Tooltip("Contains notes supported by the instrument, refering to specific audio clips.")]
        List<InstrumentNote> notes;

        /// <summary>
        /// Specifies a preferred octave of the instrument. </summary>
        public int DefaultOctave
        {
            get { return defaultOctave; }
        }

        Dictionary<MusicalNoteAbsolute, InstrumentNote> noteDictionary;

        /// <summary>
        /// Reinitializes the internal note lookup dictionary (optional). </summary>
        public void UpdateNoteDictionary()
        {
            noteDictionary = new Dictionary<MusicalNoteAbsolute, InstrumentNote>();

            foreach (var instrumentNote in notes)
                noteDictionary[instrumentNote.Info] = instrumentNote;
        }

        /// <summary>
        /// Returns the instrument's InstrumentNote accordingly to a specified note.
        /// Returns null if such note is not supported by the instrument. </summary>
        public InstrumentNote GetNote(MusicalNoteAbsolute note)
        {
            if (noteDictionary == null)
                UpdateNoteDictionary();

            InstrumentNote foundNote;
            return (noteDictionary.TryGetValue(note, out foundNote) ? new InstrumentNote(foundNote) : null);
        }

        /// <summary>
        /// Creates a functional fallback instance. </summary>
        public static Instrument FallbackInstance()
        {
            Instrument instrument = CreateInstance<Instrument>();
            instrument.notes = new List<InstrumentNote>();
            return instrument;
        }
    }
}
