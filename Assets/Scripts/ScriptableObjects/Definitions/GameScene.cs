﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.GameCollections;
using PlantParty.Services;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;
using UnityExtras.Services;

namespace PlantParty.Definitions
{
    /// <summary>
    /// Specifies a 2D position designated for more effective serialization (round before saving). </summary>
    [Serializable]
    public struct StoredPosition
    {
        public double x, y;
    }

    /// <summary>
    /// Specifies a BeatSway configuration designated for serialization. </summary>
    [Serializable]
    public struct BeatSwayInfo
    {
        public double defaultRotation, rotationSpan, beatMultiplier;
        public SwayDirection firstBeatDirection;
    }

    /// <summary>
    /// Specifies a PlantParty play object configuration designated for serialization. </summary>
    [Serializable]
    public struct PlayObject
    {
        public string collectibleName;
        public StoredPosition position;
        public double radius;
        public BeatSwayInfo beatSway;
    }

    /// <summary>
    /// Specifies a PlantParty growth point data designated for serialization. </summary>
    [Serializable]
    public struct StoredGrowthPoint
    {
        public StoredPosition pos;
        public double rad;
    }

    /// <summary>
    /// Represents a PlantParty scene and provides facility for its loading, saving and generation.
    /// Also can be used as a shortcut for locating a currently growing flower and for activating / deactivating designated scene objects. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.Definition + "GameScene")]
    public class GameScene : ScriptableObject, IInitializable
    {
        [SerializeField, Tooltip("Contains serializable data for scene PlayObjects.")]
        List<PlayObject> playObjects;

        [SerializeField, Tooltip("Contains serializable data for scene GrowthPoints.")]
        List<StoredGrowthPoint> growthPoints;

#if UNITY_EDITOR
        [SerializeField, Tooltip("Specifies target number of growth points to be generated during the scene generation.")]
        int generateGrowthPointsCount;
#endif
        /// <summary>
        /// GameCollectionsManager provider used for resolving PlayObject collectibles during a scene load / generation. </summary>
        public IServiceProvider<GameCollectionsManager> CollectionsManager { get; set; }

        /// <summary>
        /// Specifies a Fallback PlayObject used during the scene PlayObject instantiation. </summary>
        public PlayObjectCollectible FallbackPlayObject { get; set; }

        /// <summary>
        /// Specifies a game scene background SpriteRenderer (if any). </summary>
        public SpriteRenderer BackgroundRenderer { get; set; }

        /// <summary>
        /// Specifies a game scene background script (if any). </summary>
        public GameBackground BackgroundScript { get; set; }

        /// <summary>
        /// Specifies a game scene PlayObjects parent object (always exists after Initialize). </summary>
        public GameObject PlayObjectsParent { get; set; }

        /// <summary>
        /// Specifies a game scene SceneObjects parent object (always exists after Initialize).
        /// Note: Unlike the PlayObjects, the SceneObjects don't get serialized. </summary>
        public GameObject SceneObjectsParent { get; set; }

        /// <summary>
        /// Specifies a game scene growth points database (always exists after Initialize). </summary>
        public GrowthPointsDB GrowthPointsDatabase { get; private set; }

        /// <summary>
        /// Specifies a game scene PlayObjects query (always exists after Initialize). </summary>
        public PlayObjectsQuery PlayObjectsQuery { get; private set; }

        /// <summary>
        /// Activates / deactivates the objects assigned through the AddActivatedObject method. </summary>
        public bool Active
        {
            get { return active; }
            set { Activate(value); }
        }

        public const string PlayObjectsParentName = "PlayObjects";
        public const string SceneObjectsParentName = "SceneObjects";

        List<GameObject> activatedObjects;
        bool active;

        /// <summary>
        /// Initializes a game scene by deserializing its PlayObject data into actual instances of game objects
        /// and by deserializing its StoredGrowthPoint data into a live growth points database.
        /// Also initializes the play objects query and the scene objects parents. </summary>
        public void Initialize()
        {
            if (CollectionsManager == null || !CollectionsManager.ProviderValid)
                throw new UnityException(string.Format(Constants.DebugMessages.ObjectNotLinkedInvalid, typeof(GameCollectionsManagerProvider)));

            TestLinkedObjects();

            if (!PlayObjectsParent)
                AddActivatedObject(PlayObjectsParent = GetEmptyObject(PlayObjectsParentName));

            if (!SceneObjectsParent)
                AddActivatedObject(SceneObjectsParent = GetEmptyObject(SceneObjectsParentName));

            GrowthPointsDatabase = new GrowthPointsDB();
            GrowthPointsDatabase.Load(growthPoints);

            DestroyPlayObjectInstances();
            ActivatePlayObjectsParent();
            InstantiatePlayObjects();

            PlayObjectsQuery = new PlayObjectsQuery(PlayObjectsParent);

            Active = true;
        }

        /// <summary>
        /// Stores information about PlayObjectsParent child objects into the PlayObject member list for a subsequent serialization.
        /// Stores growth points from the growth point database into the StoredGrowthPoint member list for a subsequent serialization. </summary>
        public void Save()
        {
            GrowthPointsDatabase.Save(growthPoints);
            SavePlayObjects();
        }

        /// <summary>
        /// Registers a specified game object into the list of objects affected by the GameScene's Active property. </summary>
        public void AddActivatedObject(GameObject gameObject)
        {
            activatedObjects.Add(gameObject);
        }

        /// <summary>
        /// Attempts to find a Flower PlayObject within the world-space bounds designated by a specified area, which has its InGrowth property set to true. </summary>
        public Flower FindGrowingFlower(Area2 area)
        {
            GameObject foundObject = PlayObjectsQuery.QueryPlayObject(area, TestGrowingFlower);
            return foundObject ? foundObject.GetComponent<Flower>() : null;
        }
#if UNITY_EDITOR
        /// <summary>
        /// Stores the information about the original scene PlayObjects into the PlayObject member list for a subsequent serialization.
        /// Generates scene growth points while respecting the original scene PlayObjects and stores the growth points
        /// into the StoredGrowthPoint member list for a subsequent serialization. </summary>
        public void GenerateScene(FlowerDefinition flowerDefinition)
        {
            if (!BackgroundRenderer || !BackgroundScript)
                throw new UnityException("Background is not linked");

            if (!flowerDefinition)
                throw new UnityException("FlowerDefinition missing");

            UnityEditor.Undo.RecordObject(this, "Generate scene");

            if(!PlayObjectsParent)
                PlayObjectsParent = GetEmptyObject(PlayObjectsParentName);

            ActivatePlayObjectsParent();
            InitializeDefaultPlayObjects();
            SavePlayObjects();
            PlayObjectsParent.SetActive(false);

            GrowthPointsDatabase = new GrowthPointsDB();
            GenerateGrowthPoints(flowerDefinition);
            TakePlayObjectsGrowthSpace(flowerDefinition);
            GrowthPointsDatabase.Save(growthPoints);

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
        /// <summary>
        /// Returns either existing GameObject of a specified name or returns a new instance named as specified. </summary>
        public static GameObject GetEmptyObject(string objectName)
        {
            return GameObject.Find(objectName) ?? new GameObject(objectName);
        }

        void Awake()
        {
            activatedObjects = new List<GameObject>();  
        }

        void Activate(bool activate)
        {
            active = activate;
            foreach(var gameObject in activatedObjects)
            {
                gameObject.SetActive(active);
            }
        }

        void TestLinkedObjects()
        {
            TestLinked(FallbackPlayObject, "FallbackPlayObject");
            TestLinked(BackgroundRenderer, "BackgroundRenderer");
            TestLinked(BackgroundScript, "BackgroundScript");
            TestLinked(PlayObjectsParent, "PlayObjectsParent");
            TestLinked(SceneObjectsParent, "SceneObjectsParent");
        }

        void SavePlayObjects()
        {
            playObjects.Clear();            

            foreach (Transform playObject in PlayObjectsParent.transform)
            {
                Flower flower = playObject.GetComponent<Flower>();
                if(!flower || !flower.OriginCollectible)
                {
                    Debug.LogError(string.Format("Skipped saving of PlayObject '{0}' due to its missing Flower script or OriginCollectible", playObject.name));
                    continue;
                }

                if (flower.State != PlantParty.FlowerState.Grown)
                    continue;

                Vector3 objectPosition = playObject.transform.position;

                PlayObject savedPlayObject;
                savedPlayObject.position.x = Round(objectPosition.x);
                savedPlayObject.position.y = Round(objectPosition.y);

                savedPlayObject.collectibleName = flower.OriginCollectible.name;                
                savedPlayObject.radius = Round(flower.Radius);

                BeatSway beatSway = playObject.GetComponent<BeatSway>();
                if(!beatSway)
                {
                    Debug.LogWarning(string.Format("Saving a default BeatSwayInfo to the PlayObject '{0}', since it is missing a BeatSway component", playObject.name));
                    savedPlayObject.beatSway = default(BeatSwayInfo);
                }
                else
                {
                    savedPlayObject.beatSway.defaultRotation = Round(beatSway.DefaultRotation);
                    savedPlayObject.beatSway.rotationSpan = Round(beatSway.RotationSpan);
                    savedPlayObject.beatSway.beatMultiplier = Round(beatSway.BeatMultiplier);
                    savedPlayObject.beatSway.firstBeatDirection = beatSway.FirstBeatDirection;
                }

                playObjects.Add(savedPlayObject);
            }
        }

        void ActivatePlayObjectsParent()
        {
            if (!PlayObjectsParent.activeSelf)
                PlayObjectsParent.SetActive(true);
            else
                Debug.LogWarning("Scene did not contain an inactive play objects parent. Default play objects were unnecessarily started.");
        }

        void DestroyPlayObjectInstances()
        {
            while (PlayObjectsParent.transform.childCount > 0)
            {
                int lastChildIndex = PlayObjectsParent.transform.childCount - 1;
                MoveToDestroyed(PlayObjectsParent.transform.GetChild(lastChildIndex).gameObject);
            }
        }

        void InstantiatePlayObjects()
        {
            foreach(var playObject in playObjects)
            {
                PlayObjectCollectible collectible = CollectionsManager.Instance.PlayObjectCollection.GetItem(playObject.collectibleName) as PlayObjectCollectible;
                if (!collectible)
                {
                    Debug.LogError(string.Format("PlayObject '{0}' collectible missing, using a FallbackPlayObject collectible instead", playObject.collectibleName));
                    collectible = FallbackPlayObject;
                }

                InstantiatePlayObject(playObject, collectible);
            }
        }

        void InstantiatePlayObject(PlayObject playObject, PlayObjectCollectible collectible)
        {
            if (!collectible || !collectible.unlockedPrefab)
            {
                if(collectible != FallbackPlayObject)
                {
                    Debug.LogError(string.Format("PlayObject '{0}' is missing a collectible unlockedPrefab, using a FallbackPlayObject collectible instead", playObject.collectibleName));
                    InstantiatePlayObject(playObject, FallbackPlayObject);
                }
                else
                    Debug.LogError(string.Format("PlayObject '{0}' instantiation skipped due to the invalid FallbackPlayObject collectible", playObject.collectibleName));

                return;
            }

            Vector2 objectPosition;
            objectPosition.x = (float)playObject.position.x;
            objectPosition.y = (float)playObject.position.y;

            GameObject flowerInstance = Instantiate(collectible.unlockedPrefab, objectPosition, Quaternion.identity, PlayObjectsParent.transform);

            Flower flower = flowerInstance.GetComponent<Flower>();
            if (!flower)
            {
                MoveToDestroyed(flowerInstance);

                if (collectible != FallbackPlayObject)
                {
                    Debug.LogError(string.Format("PlayObject '{0}' instance is missing a Flower script. Using a FallbackPlayObject collectible instead", playObject.collectibleName));
                    InstantiatePlayObject(playObject, FallbackPlayObject);
                }
                else
                    Debug.LogError(string.Format("PlayObject '{0}' instantiation skipped, since the FallbackPlayObject collectible is missing a Flower script", playObject.collectibleName));

                return;
            }

            flower.OriginCollectible = collectible;
            flower.SetGrownRadius((float)playObject.radius);
            flower.GrownCharge = flower.GrownChargeMax;

            BeatSway beatSway = flowerInstance.GetComponent<BeatSway>();
            if(!beatSway)
            {
                Debug.LogWarning(string.Format("Skipping BeatSway configuration for the GameObject instance '{0}', since it is missing the BeatSway script", flowerInstance.name));
                return;
            }

            beatSway.DefaultRotation = (float)playObject.beatSway.defaultRotation;
            beatSway.RotationSpan = (float)playObject.beatSway.rotationSpan;
            beatSway.BeatMultiplier = (float)playObject.beatSway.beatMultiplier;
            beatSway.FirstBeatDirection = playObject.beatSway.firstBeatDirection;
            beatSway.enabled = true;
            beatSway.UpdateSway();
        }
#if UNITY_EDITOR
        void InitializeDefaultPlayObjects()
        {
            foreach (Transform playObject in PlayObjectsParent.transform)
            {
                BeatSway beatSway;
                if ((beatSway = playObject.GetComponent<BeatSway>()) != null)
                    beatSway.SetRandomSway();
                else
                    Debug.LogWarning(string.Format("PlayObject {0} is missing a BeatSway script", playObject.name));

                Flower flower;
                if (!(flower = playObject.GetComponent<Flower>()))
                {
                    Debug.LogError(string.Format("PlayObject {0} is missing a Flower script", playObject.name));
                    continue;
                }

                if (!(flower.OriginCollectible = FindPlayObjectCollectible(UnityEditor.PrefabUtility.GetPrefabParent(playObject))))
                    Debug.LogError(string.Format("OriginCollectible for the PlayObject {0} was not located", playObject.name));
            }
        }

        PlayObjectCollectible FindPlayObjectCollectible(UnityEngine.Object unlockedPrefab)
        {
            foreach (var collectionItem in CollectionsManager.Instance.PlayObjectCollection.Items)
            {
                PlayObjectCollectible playObjectCollectible = collectionItem.item as PlayObjectCollectible;

                if (playObjectCollectible && playObjectCollectible.unlockedPrefab && playObjectCollectible.unlockedPrefab.transform == unlockedPrefab)
                    return playObjectCollectible;
            }

            return null;
        }

        void GenerateGrowthPoints(FlowerDefinition flowerDefinition)
        {            
            GrowthPointsDatabase.GenerateGrowthPoints(generateGrowthPointsCount, BackgroundRenderer.Area(), BackgroundScript.TestGrowthMaskPoint, flowerDefinition.flowerRadius.minimum, flowerDefinition.flowerRadius.maximum);
        }

        void TakePlayObjectsGrowthSpace(FlowerDefinition flowerDefinition)
        {
            foreach (Transform playObject in PlayObjectsParent.transform)
            {
                Flower flower = playObject.GetComponent<Flower>();
                if (flower)
                    GrowthPointsDatabase.TakeGrowthSpace(flower.transform.position, flower.Radius, flowerDefinition.flowerRadius.minimum, flowerDefinition.flowerRadius.maximum);
                else
                    Debug.LogWarning(string.Format("PlayObject {0} is missing a Flower script", playObject.name));
            }
        }
#endif
        static double Round(double value)
        {
            return GrowthPointsDB.Round(value);
        }

        static bool TestGrowingFlower(GameObject gameObject)
        {
            Flower flower;
            return (gameObject && (flower = gameObject.GetComponent<Flower>()) && flower.InGrowth);
        }

        static void TestLinked(UnityEngine.Object testedObject, string infoName)
        {
            if (!testedObject)
                Debug.LogWarning(string.Format(Constants.DebugMessages.ObjectNotLinked, infoName));
        }

        /// <summary>
        /// Reparents a specified GameObject into a parent GameObject which will be destroyed by the end of the frame.
        /// Thus the object will not appear between PlayObjectsParent children. </summary>
        static void MoveToDestroyed(GameObject objectToDestroy)
        {
            const string destroyParentName = "DestroyedObjects";
            GameObject destroyParent = GetEmptyObject(destroyParentName);
            Destroy(destroyParent);

            objectToDestroy.transform.parent = destroyParent.transform;
        }
    }
}