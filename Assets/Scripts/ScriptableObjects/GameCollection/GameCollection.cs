﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityExtras;
using UnityExtras.ExtensionMethods;
using Random = UnityEngine.Random;

namespace PlantParty.GameCollections
{
    /// <summary>
    /// Specifies a state of a GameCollectible item within a GameCollection.
    /// Intended to be used only for GameCollection serialization / deserialization and its internal use.
    /// For public use there is a dedicated GameCollectionRecord class. </summary>
    [Serializable]
    public class GameCollectionItem
    {
        /// <summary>
        /// Specifies a GameCollectible of the game collection item. </summary>
        public GameCollectible item;

        /// <summary>
        /// Specifies whether the collection item is noted as unlocked in its game collection. </summary>
        public bool unlocked;

        /// <summary>
        /// Specifies whether the collection item is noted as featured in its game collection. </summary>
        [NonSerialized] public bool featured;
    }

    /// <summary>
    /// Represents a collection of GameCollectible items.
    /// Provides a functionality to unlock / retrieve a random collection item, having an O(1) complexity.
    /// Uses a retrieval history to avoid returning historical (previously retrieved) collection items. </summary>
    [CreateAssetMenu(menuName = GameAssetMenu.GameCollection + "GameCollection")]
    public class GameCollection : ScriptableObject, IInitializable
    {
#if UNITY_EDITOR
        [SerializeField, Tooltip("Specifies a GameCollectible always returned by GetRandomUnlockedItem when the useDebugItem is set to true.\nFor development purposes only.")]
        GameCollectible debugItem;

        [SerializeField, Tooltip("Specifies whether GetRandomUnlockedItem shall always return a debugItem GameCollectible.\nFor development purposes only.")]
        bool useDebugItem;
#endif
        /// <summary>
        /// Specifies the maximum number of previously retrieved collection items kept in the retrieval history.
        /// These are the items lastly returned by GetRandomUnlockedItem and avoided in future retrievals.
        /// Note: The retrieval history is automatically ignored in case it would completely block an available item retrieval. </summary>
        [SerializeField, Tooltip("Specifies the maximum number of previously retrieved collection items kept in the retrieval history.")]
        int historyDepth;

        [SerializeField, Tooltip("Contains the collection items and their state within the collection.")]
        List<GameCollectionItem> items;

        /// <summary>
        /// Contains the collection items and their state within the collection. </summary>
        public IList<GameCollectionItem> Items
        {
            get { return items; }
        }

        /// <summary>
        /// Returns the number of locked collection items. </summary>
        public int LockedItemsCount
        {
            get { return lockedShuffledItems.Count; }
        }

        GenericHistory<GameCollectible> retrievalHistory;
        Dictionary<GameCollectible, GameCollectionRecord> recordLookup;
        Dictionary<string, GameCollectible> namedLookup;
        List<GameCollectionItem> lockedShuffledItems, unlockedItems;

        /// <summary>
        /// Initializes the collection so its public lookup interface can function properly. </summary>
        public void Initialize()
        {
            retrievalHistory = new GenericHistory<GameCollectible> { Depth = historyDepth };
            recordLookup = new Dictionary<GameCollectible, GameCollectionRecord>();
            namedLookup = new Dictionary<string, GameCollectible>();
            lockedShuffledItems = new List<GameCollectionItem>();
            unlockedItems = new List<GameCollectionItem>();

            foreach (var currentItem in items)
            {
                if (namedLookup.ContainsKey(currentItem.item.name))
                {
                    Debug.LogWarning(string.Format("Collection item '{0}' duplicity found. The duplicate item is skipped.", currentItem.item.name));
                    continue;
                }

                recordLookup[currentItem.item] = new GameCollectionRecord(currentItem);
                namedLookup[currentItem.item.name] = currentItem.item;
                (currentItem.unlocked ? unlockedItems : lockedShuffledItems).Add(currentItem);
            }

            lockedShuffledItems.Shuffle();
        }

        /// <summary>
        /// Unlocks, features and returns a random locked collection item. 
        /// Returns null if there is no more locked items.
        /// This is an O(1) operation. </summary>
        public GameCollectible UnlockRandomItem()
        {
            if (lockedShuffledItems.Count == 0)
                return null;

            int lastItemIndex = lockedShuffledItems.Count - 1;
            var unlockedItem = lockedShuffledItems[lastItemIndex];
            lockedShuffledItems.RemoveAt(lastItemIndex);

            unlockedItem.unlocked = unlockedItem.featured = true;

            unlockedItems.Add(unlockedItem);

            return unlockedItem.item;
        }

        /// <summary>
        /// Returns a random unlocked item while respecting the retrieval history (avoiding to return a historical item).
        /// Returns null if there is no unlocked item.
        /// Note: The retrieval history is ignored if it would completely block the unlocked item retrieval.
        /// DEBUG use: Returns a debugItem if the feature is enabled.
        /// Normally this is an O(1) operation. </summary>
        public GameCollectible GetRandomUnlockedItem()
        {
#if UNITY_EDITOR
            if (useDebugItem)
                return debugItem;
#endif
            if (unlockedItems.Count == 0)
                return null;

            bool ignoreHistory = (retrievalHistory.Count >= unlockedItems.Count);

            // Optimized O(1) pick
            const int maxRandomPickCount = 10;
            for(int i = 0; i < maxRandomPickCount; ++i)
            {
                var randomItem = unlockedItems[Random.Range(0, unlockedItems.Count)];

                if (ignoreHistory)
                    return randomItem.item;

                if (retrievalHistory.Contains(randomItem.item))
                    continue;

                retrievalHistory.Add(randomItem.item);
                return randomItem.item;
            }

            // Fallback O(n) pick
            var availableItems = unlockedItems.Where(unlockedItem => ignoreHistory || !retrievalHistory.Contains(unlockedItem.item)).ToList();
            if (availableItems.Count != 0)
            {
                var randomItem = availableItems[Random.Range(0, availableItems.Count)];

                if(!ignoreHistory)
                    retrievalHistory.Add(randomItem.item);

                return randomItem.item;
            }

            Debug.LogError("Retrieval logic error");
            return null;
        }

        /// <summary>
        /// Returns a collection item by looking it up it using a specified GameCollectible name.
        /// Returns null if the specified collection item is not found.
        /// Approaches an O(1) operation. </summary>
        public GameCollectible GetItem(string itemName)
        {
            GameCollectible foundItem;
            namedLookup.TryGetValue(itemName, out foundItem);
            return foundItem;
        }

        /// <summary>
        /// Returns a collection record for a specified collection item. 
        /// The record provides collection item status information within the scope of its game collection.
        /// Returns null if the specified collection item is not found.
        /// Approaches an O(1) operation. </summary>
        public GameCollectionRecord GetItemRecord(GameCollectible collectionItem)
        {
            GameCollectionRecord foundRecord;
            recordLookup.TryGetValue(collectionItem, out foundRecord);
            return foundRecord;
        }

        void Awake()
        {
            // In case the GameCollection is not created by the Unity deserialization
            if (items == null)
                items = new List<GameCollectionItem>();
        }
    }
}
