﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

#if UNITY_EDITOR
using UnityExtras;
using UnityExtras.PointDB;
using UnityExtras.Services;
using UnityExtras.ExtensionMethods;
#endif

namespace PlantParty
{
    /// <summary>
    /// Visualizes the GrowthPointsDB inner workings in the Unity Editor.
    /// Intended for development diagnostic purposes.
    /// The database is retrieved from a current game scene through the assigned GameSceneManager provider. </summary>
    public class GrowthPointsDebug : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField, Tooltip("GameSceneManager provider used to retrieve the growth point database of a current game scene.")]
        Object gameSceneManagerProvider;

        [SerializeField, Tooltip("Specifies whether the database's growth points shall be visualized in the Unity Editor")]
        bool drawGrowthPoints;

        [SerializeField, Tooltip("Specifies whether the database's query debug areas shall be visualized in the Unity Editor.")]
        bool drawQueryAreas;

        GrowthPointsDB GrowthPointsDatabase
        {
            get { return GetGrowthPointsDatabase(); }
        }

        IServiceProvider<GameSceneManager> gameSceneManager;
        Span debugGrowthPointRadiusSpan;
        GameObject debugAreaHandle, debugCircleHandle;
        PartitionInfo[] debugQueryResults;
        PointRecord<float>[] resultBuffer;        
        
        void Awake()
        {
            gameSceneManager = gameSceneManagerProvider.AsServiceProvider<GameSceneManager>();

            const int debugResultsDefaultSize = 100;
            debugQueryResults = new PartitionInfo[debugResultsDefaultSize];

            const int resultBufferSize = 10000;
            resultBuffer = new PointRecord<float>[resultBufferSize];
        }

        void OnDrawGizmos()
        {
            if(drawQueryAreas)
            {
                DrawQueryAreaPartitions();
                DrawQueryCirclePartitions();
            }
        }

        void OnDrawGizmosSelected()
        {
            if(drawGrowthPoints)
                DrawGrowthPoints();
        }

        void DrawQueryAreaPartitions()
        {
            if (!CheckDatabase())
                return;

            GameObject areaHandle = GetDebugAreaHandle();

            UnityEditor.Handles.color = Color.magenta;
            UnityEditor.Handles.DrawWireCube(areaHandle.transform.position, areaHandle.transform.localScale);

            Vector3 areaHalfSize = areaHandle.transform.localScale * .5f;
            Area2 debugArea = new Area2(areaHandle.transform.position - areaHalfSize, areaHandle.transform.position + areaHalfSize);

            DrawDebugQuery(results => GrowthPointsDatabase.QueryOverlapAreaNonAllocDebug(debugArea, results));
        }

        void DrawQueryCirclePartitions()
        {
            if (!CheckDatabase())
                return;

            GameObject areaHandle = GetDebugCircleHandle();
            float debugHandleSize = ((Vector2)areaHandle.transform.localScale).Max();

            UnityEditor.Handles.color = Color.magenta;
            UnityEditor.Handles.DrawWireDisc(areaHandle.transform.position, Vector3.forward, debugHandleSize);

            DrawDebugQuery(results => GrowthPointsDatabase.QueryOverlapCircleNonAllocDebug(areaHandle.transform.position, debugHandleSize, results));
        }

        void DrawGrowthPoints()
        {
            if (!CheckDatabase())
                return;

            const float invalidRadiusRange = 0f;
            float radiusRange = debugGrowthPointRadiusSpan.Length;
            if (Mathf.Approximately(radiusRange, invalidRadiusRange))
            {
                const float fallbackRadiusRange = 1f;
                radiusRange = fallbackRadiusRange;
            }

            float minimumRadius = float.MaxValue;
            float maximumRadius = float.MinValue;

            int resultCount = Auxiliaries.QueryAutoExpand(GrowthPointsDatabase.QueryAllNonAlloc, ref resultBuffer);            
            for (int i = 0; i < resultCount; ++i)
            {
                var queryItem = resultBuffer[i];
                float radius = queryItem.Value;

                if (radius < minimumRadius)
                    minimumRadius = radius;

                if (radius > maximumRadius)
                    maximumRadius = radius;

                float radiusRangeRatio = (radius - debugGrowthPointRadiusSpan.Min) / radiusRange;
                if (!radiusRangeRatio.IsFinite())
                {
                    const float fallbackRadiusRangeRatio = 0f;
                    Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "radius range ratio", radiusRangeRatio, fallbackRadiusRangeRatio));
                    radiusRangeRatio = fallbackRadiusRangeRatio;
                }
                
                Gizmos.color = Color.Lerp(Color.magenta, Color.green, radiusRangeRatio);

                // Note: The alternative use of UnityEditor.Handles.DrawSolidDisc or DrawWireDisc would be much slower.
                const float debugGrowthPointRadius = .03f;
                Gizmos.DrawSphere(queryItem.Position, debugGrowthPointRadius);
            }

            debugGrowthPointRadiusSpan = new Span(minimumRadius, maximumRadius);
        }        

        void DrawDebugQuery(Auxiliaries.QueryDelegate<PartitionInfo> queryDelegate)
        {
            if (!CheckDatabase())
                return;

            int resultCount = Auxiliaries.QueryAutoExpand(queryDelegate, ref debugQueryResults);

            float parititionSize = GrowthPointsDatabase.PartitionSize;
            Vector3 drawCubeSize = new Vector3(parititionSize, parititionSize);
            Vector3 partitionHalfSize = drawCubeSize * Constants.Arithmetic.Half;
            Vector3 drawCubePosition = Vector3.zero;            
            for (int i = 0; i < resultCount; ++i)
            {
                drawCubePosition.x = debugQueryResults[i].x;
                drawCubePosition.y = debugQueryResults[i].y;
                drawCubePosition *= parititionSize;
                drawCubePosition += partitionHalfSize;

                const float queryAreaTransparency = .3f;
                Color color = debugQueryResults[i].fullyContained ? Color.green : Color.yellow;
                color.a = queryAreaTransparency;
                Gizmos.color = color;

                DrawOutlinedCube(drawCubePosition, drawCubeSize);
            }
        }

        GameObject GetDebugAreaHandle()
        {
            const string debugAreaHandleName = "DebugAreaHandle";
            return (debugAreaHandle ?? (debugAreaHandle = GetHandle(debugAreaHandleName).gameObject));
        }

        GameObject GetDebugCircleHandle()
        {
            const string debugCircleHandleName = "DebugCircleHandle";
            return (debugCircleHandle ?? (debugCircleHandle = GetHandle(debugCircleHandleName).gameObject));
        }

        Transform GetHandle(string handleName)
        {
            return transform.Find(handleName) ?? CreateHandle(handleName, transform);
        }

        bool CheckDatabase()
        {
            if (!gameSceneManager.Instance.CurrentScene)
                return false;
 
            if (GrowthPointsDatabase == null)
                Debug.LogWarning("GrowthPoints database missing");

            return (GrowthPointsDatabase != null);
        }

        GrowthPointsDB GetGrowthPointsDatabase()
        {
            var currentScene = gameSceneManager.Instance.CurrentScene;
            return (currentScene && currentScene.Data) ? currentScene.Data.GrowthPointsDatabase : null;
        }

        static Transform CreateHandle(string name, Transform parent)
        {
            GameObject handle = new GameObject(name);
            handle.transform.parent = parent;
            return handle.transform;
        }

        static void DrawOutlinedCube(Vector3 position, Vector3 size)
        {
            const float drawCubeOutline = .9f;

            Gizmos.DrawCube(position, size);
            Gizmos.DrawCube(position, size * drawCubeOutline);
        }
#endif
    }
}