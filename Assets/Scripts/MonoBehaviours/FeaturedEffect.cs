﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras;
using UnityExtras.Services;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Represents effect of particles radiating from a circular or rectangular shape.
    /// Note: Reparents pooled particles to its GameObject and then pools them internally. 
    ///       Due to this the effect can be also used in a Canvas without impacting performance.
    /// Supports MonoBehaviour enable / disable.</summary>
    public class FeaturedEffect : MonoBehaviour
    {
        [SerializeField, Tooltip("GameObjectPool provider used for the effect particle pooling.")]
        Object gameObjectPoolProvider;

        [SerializeField, Tooltip("Configuration for the FeaturedEffect behavour.")]
        FeaturedEffectDefinition effectDefinition;

        /// <summary>
        /// Represents a particle GameObject pooled from the GameObjectPool.
        /// Note: This allows for having the same FeaturedEffectDefinition configuration 
        ///       while using a canvas prefab (RectTransform) or a standard prefab (Transform). </summary>
        [SerializeField, Tooltip("Represents a particle GameObject pooled from the GameObjectPool.")]
        GameObject particlePrefab;

        [SerializeField, Tooltip("Forces particle emission to be turned on if set to true. Intended mainly for development purposes.")]
        bool forceEmitting;

        /// <summary>
        /// Enables / disables the particle emission.
        /// Default: false </summary>
        public bool Emitting
        {
            get { return emitting; }
            set
            {
                bool started = (!emitting && value);

                if(started)
                    lastEmittedTime = internalTime - emissionPeriod;

                emitting = value;
            }
        }

        /// <summary>
        /// Automatically destroys the FeaturedEffect's GameObject if the effect is not Emitting and no longer has any active particles.
        /// Default: false </summary>
        public bool AutoDestroy { get; set; }

        struct PooledObjectInfo
        {
            public GameObject gameObject;
            public Vector3 originalLocalScale;
            public SpriteRenderer spriteRenderer;
            public CanvasRenderer canvasRenderer;
        }

        class ParticleInfo
        {
            public PooledObjectInfo pooledObject;
            public Vector2 velocity;
            public float emissionTime, lifetime;
        }

        IServiceProvider<GameObjectPool> gameObjectPool;
        RecyclingList<ParticleInfo> particles;        
        float internalTime, emissionPeriod, lastEmittedTime;
        bool emitting, lifetimeWarningRaised;

        const float AlphaMin = 0f;

        void Awake()
        {
            gameObjectPool = gameObjectPoolProvider.AsServiceProvider<GameObjectPool>();
            particles = new RecyclingList<ParticleInfo>();

            if (!particlePrefab.GetComponentInChildren<SpriteRenderer>() && !particlePrefab.GetComponentInChildren<CanvasRenderer>())
                Debug.LogWarning("Linked particle prefab does not contain any supported renderer, thus the lifetimeTransparency calculation will be omitted");

            if (!effectDefinition)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, typeof(FeaturedEffectDefinition)));
                effectDefinition = FeaturedEffectDefinition.FallbackInstance();
            }

            emissionPeriod = effectDefinition.emission.EmissionPeriod();
            internalTime = Time.time;            
        }        

        void OnEnable()
        {
            HideUnusedRenderers();
        }

        void Update()
        {
            UpdateParticles();

            if (forceEmitting)
                Emitting = true;

            if (Emitting)
                UpdateEmission();

            if (AutoDestroy && !Emitting && particles.UsedCount == 0)
            {
                CleanupParticles();
                Destroy(gameObject);
            }

            internalTime += Time.deltaTime;
        }
#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            if (effectDefinition.debugDrawParticleSpan)
                DebugDrawParticleSpan();
        }

        void DebugDrawParticleSpan()
        {
            UnityEditor.Handles.matrix = transform.localToWorldMatrix;
            UnityEditor.Handles.color = Color.magenta;

            Color transparentMagenta = Color.magenta;
            transparentMagenta.a = Constants.Arithmetic.Quarter;

            float maxParticleTravelDistance = effectDefinition.particle.lifetimeSpan.Max * effectDefinition.particle.speedSpan.Max;
            float maxParticleDistance = effectDefinition.emission.shapeSpan.Max + maxParticleTravelDistance;

            if (effectDefinition.emission.shape == FeaturedEffectEmitShape.Circle)
            {
                float circleRadius = effectDefinition.emission.shapeDimensions.Max();
                UnityEditor.Handles.DrawWireDisc(Vector3.zero, Vector3.forward, circleRadius);
                UnityEditor.Handles.DrawWireDisc(Vector3.zero, Vector3.forward, circleRadius + maxParticleDistance);
                
                UnityEditor.Handles.color = transparentMagenta;
                UnityEditor.Handles.DrawWireDisc(Vector3.zero, Vector3.forward, circleRadius + effectDefinition.emission.shapeSpan.Min);
            }
            else if (effectDefinition.emission.shape == FeaturedEffectEmitShape.Rectangle)
            {                
                Area2 shapeDimensionsArea = new Area2(effectDefinition.emission.shapeDimensions);
                UnityEditor.Handles.DrawWireCube(Vector3.zero, effectDefinition.emission.shapeDimensions);
                UnityEditor.Handles.DrawWireCube(Vector3.zero, Area2.ApplyBorder(shapeDimensionsArea, maxParticleDistance).Size);
                
                UnityEditor.Handles.color = transparentMagenta;
                UnityEditor.Handles.DrawWireCube(Vector3.zero, Area2.ApplyBorder(shapeDimensionsArea, effectDefinition.emission.shapeSpan.Min).Size);
            }
        }
#endif
        /// <summary>
        /// Pushes pooled particles back to the GameObjectPool.
        /// Note: Since the GameObjectPool reparents pushed GameObjects, this cannot be called from:
        ///         - OnDisable (changing GameObject hierarchy while activating or deactivating the parent is not allowed)
        ///         - OnDestroy (reparenting during a scene change will succeed but the pooled GameObject will be destroyed anyway) </summary>
        void CleanupParticles()
        {
            foreach (var particle in particles)
            {
                gameObjectPool.Instance.PushItem(particle.pooledObject.gameObject, particlePrefab);
            }

            particles.Clear();
        }        

        void UpdateEmission()
        {
            float emissionCount = (internalTime - lastEmittedTime) / emissionPeriod;
            if (!emissionCount.IsFinite())
            {
                const float fallbackEmissionCount = 0f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "emission count", emissionCount, fallbackEmissionCount));
                emissionCount = fallbackEmissionCount;
            }

            int emitCount = (int)emissionCount;
            for (int i = 1; i <= emitCount; i++)
            {
                EmitParticle(lastEmittedTime + emissionPeriod * i);
            }

            lastEmittedTime += emissionPeriod * emitCount;
        }

        void EmitParticle(float emissionTime)
        {
            ParticleInfo particleInfo;

            if (particles.AvailableCount == 0)
            {
                GameObject pooledObject = gameObjectPool.Instance.PopItem(particlePrefab);
                if (!pooledObject)
                {
                    Debug.LogWarning("No object retrieved from the GameObjectPool");
                    return;
                }

                if (pooledObject.transform.parent != transform)
                    pooledObject.transform.SetParent(transform);

                particleInfo = CreateParticle(pooledObject);

                if (!particles.Add(particleInfo, asUsed: true))
                {
                    Debug.LogError("Failed to add the created particle to the particles list");
                    return;
                }
            }
            else
            {
                if((particleInfo = particles.Recycle()) == default(ParticleInfo))
                {
                    Debug.LogError("Recycling of an available particle failed");
                    return;
                }
            }

            InitializeParticle(particleInfo, emissionTime);
        }

        void InitializeParticle(ParticleInfo particleInfo, float emissionTime)
        {
            Vector2 particleOrigin = effectDefinition.emission.RandomOrigin();

            particleInfo.velocity = particleOrigin.normalized * effectDefinition.particle.speedSpan.RandomContained();
            particleInfo.emissionTime = emissionTime;
            particleInfo.lifetime = effectDefinition.particle.lifetimeSpan.RandomContained();

            Transform particleTransform = particleInfo.pooledObject.gameObject.transform;
            particleTransform.localPosition = particleOrigin;
            particleTransform.localRotation = Quaternion.LookRotation(Vector3.forward, particleInfo.velocity);
            particleTransform.localScale = particleInfo.pooledObject.originalLocalScale * effectDefinition.particle.scaleSpan.RandomContained();

            UpdateParticle(particleInfo, internalTime - emissionTime);
        }
 
        void UpdateParticles()
        {
            foreach (var particle in particles.UsedItemsModifiable())
            {
                UpdateParticle(particle, Time.deltaTime);
            }
        }

        void UpdateParticle(ParticleInfo particleInfo, float deltaTime)
        {
            if(internalTime >= particleInfo.emissionTime + particleInfo.lifetime)
            {
                SetParticleAlpha(particleInfo, AlphaMin);
                particles.Dismiss(particleInfo);
                return;
            }

            float lifetimeProgress = (internalTime - particleInfo.emissionTime) / particleInfo.lifetime;
            if (!lifetimeProgress.IsFinite())
            {
                const float fallbackLifetimeProgress = 0f;

                if (!lifetimeWarningRaised)
                {
                    lifetimeWarningRaised = true;
                    Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "particle lifetime progress", lifetimeProgress, fallbackLifetimeProgress));
                }
                
                lifetimeProgress = fallbackLifetimeProgress;
            }
            else
                lifetimeWarningRaised = false;

            particleInfo.pooledObject.gameObject.transform.localPosition += (Vector3)particleInfo.velocity * effectDefinition.particle.lifetimeSpeed.Evaluate(lifetimeProgress) * deltaTime;
            SetParticleAlpha(particleInfo, effectDefinition.particle.lifetimeTransparency.Evaluate(lifetimeProgress));
        }

        static ParticleInfo CreateParticle(GameObject pooledObject)
        {
            return new ParticleInfo
            {
                pooledObject = new PooledObjectInfo
                {
                    gameObject = pooledObject,
                    originalLocalScale = pooledObject.transform.localScale,
                    spriteRenderer = pooledObject.GetComponentInChildren<SpriteRenderer>(),
                    canvasRenderer = pooledObject.GetComponentInChildren<CanvasRenderer>()
                }
            };
        }

        static void SetParticleAlpha(ParticleInfo particleInfo, float alpha)
        {
            if (particleInfo.pooledObject.spriteRenderer)
                particleInfo.pooledObject.spriteRenderer.color = particleInfo.pooledObject.spriteRenderer.color.WithAlpha(alpha);

            if (particleInfo.pooledObject.canvasRenderer)
                particleInfo.pooledObject.canvasRenderer.SetAlpha(alpha);
        }

        /// <summary>
        /// Sets a full transparency to the inactive particle Canvas renderers (if any).
        /// Note: FeaturedEffect does not disable the unused particles but makes them fully transparent, in order to avoid performance hit if used in Canvas.
        ///       Disabling and enabling the parent GameObject makes the canvas renderers fully opaque, thus calling this method from OnEnable reverts the action. </summary>
        void HideUnusedRenderers()
        {
            foreach (var particle in particles.AvailableItems())
            {
                if (particle.pooledObject.canvasRenderer)
                    particle.pooledObject.canvasRenderer.SetAlpha(AlphaMin);
            }
        }
    }
}
