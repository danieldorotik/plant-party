﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
using UnityEngine.UI;
using UnityExtras;
#endif

namespace PlantParty
{
    /// <summary>
    /// Utility which drives the fill amount of an assigned UI.Image component, acting as a progress bar.
    /// Intended for development purposes. </summary>
    [ExecuteInEditMode]
    public class ProgressBar : MonoBehaviour
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        [SerializeField, Tooltip("Specifies a UI.Image component for fill amount changing.")]
        Image progressImage;

        [Range(0f, 1f)]
        [SerializeField, Tooltip("Allows to specify the ProgressBar's progress live in the Unity Editor.")]
        float progress;

        /// <summary>
        /// Specifies the current ProgressBar's progress (where minimum is 0 and maximum is 1). </summary>
        public void SetProgress(float desiredProgress)
        {
            progress = desiredProgress;
        }

        void Awake()
        {
            if (!progressImage)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(Image)));
                progressImage = GetComponent<Image>() ?? gameObject.AddComponent<Image>();
            }
        }

        void Update()
        {
            progressImage.fillAmount = progress;
        }
#endif
    }
}
