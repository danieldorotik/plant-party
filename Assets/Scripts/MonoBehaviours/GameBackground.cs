﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Data;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Represents a game scene background which has a StoredGameScene assigned to it. Generates the background material.
    /// Provides a growth mask testing capability for the scene growth points generation if UNITY_EDITOR is defined. </summary>
    [ExecuteInEditMode]
    public class GameBackground : MonoBehaviour
    {
        [SerializeField, Tooltip("Specifies a parent object for game play objects.")]
        GameObject playObjectsParent;

        [SerializeField, Tooltip("Specifies a game background SpriteRenderer.")]
        SpriteRenderer spriteRenderer;

        [SerializeField, Tooltip("Specifies a material based on which the background material is created.")]
        Material baseMaterial;

#pragma warning disable 0649
        [SerializeField, Tooltip("Specifies a texture used during the background material creation.")]
        Texture mainTexture, detailMask;
#pragma warning restore 0649

        [SerializeField, Tooltip("Represents a StoredGameScene object for a current game scene.")]
        StoredGameScene gameScene;
#if UNITY_EDITOR
        [SerializeField, Tooltip("Specifies a texture used for the scene growth points generation.")]
        Texture2D growthMask;
#endif
        /// <summary>
        /// Specifies a game background SpriteRenderer. </summary>
        public SpriteRenderer SpriteRenderer
        {
            get { return spriteRenderer; }
        }

        /// <summary>
        /// Represents a StoredGameScene object for a current game scene. </summary>
        public StoredGameScene GameScene
        {
            get { return gameScene; }
        }

        /// <summary>
        /// Specifies a parent object for game play objects. </summary>
        public GameObject PlayObjectsParent
        {
            get { return playObjectsParent; }
        }

        public const string Tag = "Background";

#if UNITY_EDITOR
        int backgroundMask;
        Ray ray;
#endif

#if UNITY_EDITOR
        /// <summary>
        /// Returns true if a specified world-point projects to a background growth mask pixel
        /// which has a grayscale value greater than GROWTH_MASK_POSITIVE_THRESHOLD. </summary>
        public bool TestGrowthMaskPoint(Vector2 point)
        {
            if (!spriteRenderer)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinked, "SpriteRenderer"));
                return false;
            }

            ray.origin = (Vector3)point + Vector3.forward * transform.position.z - Vector3.forward;
            ray.direction = Vector3.forward;

            const float castDistance = 50f;
            RaycastHit2D hitInfo = Physics2D.GetRayIntersection(ray, castDistance, backgroundMask);
            if (!hitInfo || hitInfo.transform != transform)
                return false;

            hitInfo.point += (Vector2)spriteRenderer.bounds.extents - (Vector2)transform.position;

            float x = (hitInfo.point.x * growthMask.width) / spriteRenderer.bounds.size.x;
            float y = (hitInfo.point.y * growthMask.height) / spriteRenderer.bounds.size.y;
            
            if (x.IsFinite() && y.IsFinite())
            {
                const float growthMaskPositiveThreshold = .5f;
                return (growthMask.GetPixel(Mathf.FloorToInt(x), Mathf.FloorToInt(y)).grayscale > growthMaskPositiveThreshold);
            }

            Debug.LogError(string.Format("Cannot test growth mask for point [{0},{1}]. Most probably the SpriteRenderer has invalid bounds.", x, y));
            return false;
        }
#endif
        void Awake()
        {
            if (!spriteRenderer)
            {                
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(SpriteRenderer)));
                spriteRenderer = GetComponent<SpriteRenderer>() ?? gameObject.AddComponent<SpriteRenderer>();
            }
            transform.CheckChildComponent(spriteRenderer);

#if UNITY_EDITOR
            const string layer = Tag;
            backgroundMask = LayerMask.GetMask(layer);
            ray = new Ray();
#endif
            const float detailMapTilingMultiplier = .47f;
            spriteRenderer.material = CreateBackgroundMaterial(baseMaterial, mainTexture, detailMask, spriteRenderer.bounds.extents * detailMapTilingMultiplier);
        }

        static Material CreateBackgroundMaterial(Material baseMaterial, Texture texture, Texture detailMask, Vector2 detailTilingScale)
        {
            Material material = new Material(baseMaterial)
            {
                name = baseMaterial.name + " (Generated)",
                mainTexture = texture
            };
            
            material.SetTexture(Constants.UnityProperties.DetailMask, detailMask);
            material.SetTextureScale(Constants.UnityProperties.DetailAlbedoMap, detailTilingScale);

            return material;
        }        
    }
}