﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Definitions;
using System;
using System.Collections;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;
using UnityExtras.Services;
using Random = UnityEngine.Random;

namespace PlantParty
{
    /// <summary>
    /// Represents an initial PlantParty.BeatSway direction. </summary>
    public enum SwayDirection { Left, Right }

    /// <summary>
    /// Represents a PlantParty.Flower sway animation behaviour synchronized with music.
    /// Supports MonoBehaviour enable / disable. </summary>
    public class BeatSway : MonoBehaviour
    {
        [SerializeField, Tooltip("SoundManager provider used for a sway animation music synchronization.")]
        UnityEngine.Object soundManagerProvider;

        [SerializeField, Tooltip("Configuration for the BeatSway behaviour.")]
        BeatSwayDefinition beatSwayDefinition;

        [SerializeField, Tooltip("GameObject to be animated by the BeatSway behaviour.")]
        GameObject drivenObject;

        /// <summary>
        /// Allows specifying a custom event handler used to scale the animated sway angle (optional).
        /// Note: Resulting value of a custom event handler is clamped by the SWAY_AMOUNT_MIN and SWAY_AMOUNT_MAX limits. </summary>
        public event Func<float> SwayAmount;

        /// <summary>
        /// Specifies a default euler angle for the animated driven object rotation. </summary>
        public float DefaultRotation { get; set; }

        /// <summary>
        /// Specifies a deviation euler angle used for the beat sway animation.
        /// Note: The deviation is applied in both the positive and negative direction originating from the DefaultRotation angle. </summary>
        public float RotationSpan { get; set; }

        /// <summary>
        /// Affects the sway animation speed in relation to a currently playing music's BPM. </summary>
        public float BeatMultiplier { get; set; }

        /// <summary>
        /// Specifies a default initial direction of the beat sway animation. </summary>
        public SwayDirection FirstBeatDirection
        {
            get { return firstBeatDirection; }
            set
            {
                firstBeatDirection = value;

                // First animation curve cycle results in left-to-right sway. Let's skip the 1st cycle if we want right-to-left sway.
                swayDirectionShift = (firstBeatDirection == SwayDirection.Left) ? 1d : 0d;
            }
        }

        IServiceProvider<SoundManager> soundManager;
        SwayDirection firstBeatDirection;
        double swayDirectionShift;
        double firstBeatTime, beatDurationMultiplier;
        float trackChangeMultiplier;
        IEnumerator trackChangeCoroutine, trackChangeAnimationCoroutine;

        const float TrackChangeMultiplierMax = 1f;

        /// <summary>
        /// Arbitrarily sets the default animation properties (DefaultRotation, RotationSpan, BeatMultiplier, FirstBeatDirection) based on the linked BeatSwayDefinition. </summary>
        public void SetRandomSway()
        {            
            DefaultRotation = Random.Range(-beatSwayDefinition.maxDefaultRotation, beatSwayDefinition.maxDefaultRotation);
            RotationSpan = beatSwayDefinition.minMaxRotationSpan.RandomContained();

            if (beatSwayDefinition.beatMultipliers.Count > 0)
            {
                int randomIndex = Random.Range(0, beatSwayDefinition.beatMultipliers.Count);
                BeatMultiplier = beatSwayDefinition.beatMultipliers[randomIndex];
            }
            else
            {
                const float defaultBeatMultiplier = 1f;
                BeatMultiplier = defaultBeatMultiplier;
            }

            var directions = Enum.GetValues(typeof(SwayDirection));
            FirstBeatDirection = (SwayDirection)directions.GetValue(Random.Range(0, directions.Length));
        }

        /// <summary>
        /// Evaluates the beat sway animation accordingly to the currently playing music and updates the animated driven object rotation. </summary>
        public void UpdateSway()
        {
            double beatProgression = swayDirectionShift + (soundManager.Instance.SmoothDSPTime - firstBeatTime) * beatDurationMultiplier;
            float curveResult = beatSwayDefinition.animationCurve.Evaluate((float)beatProgression);

            // Note that Quaternion.Euler is counter-clockwise and we want curveResult of 0.0f to be the left sway extreme.
            float currentRotation = Mathf.Lerp(RotationSpan, -RotationSpan, curveResult);

            currentRotation *= trackChangeMultiplier;

            if (SwayAmount != null)
            {
                const float swayAmountMin = 0f, swayAmountMax = 1f;
                currentRotation *= Mathf.Clamp(SwayAmount(), swayAmountMin, swayAmountMax);
            }
                        
            drivenObject.transform.localRotation = Quaternion.Euler(0f, 0f, DefaultRotation + currentRotation);
        }

        void Awake()
        {
            soundManager = soundManagerProvider.AsServiceProvider<SoundManager>();

            if (!beatSwayDefinition)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, typeof(BeatSwayDefinition)));
                beatSwayDefinition = BeatSwayDefinition.FallbackInstance();
            }

            if (!drivenObject)
            {
                Debug.LogWarning(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, "DrivenObject"));
                drivenObject = new GameObject();
                drivenObject.transform.parent = transform;
            }

            trackChangeMultiplier = TrackChangeMultiplierMax;
        }

        void OnEnable()
        {
            EnableInstance();
        }
        
        void Update()
        {
            UpdateSway();
        }

        void OnDisable()
        {
            DisableInstance();
        }

        void EnableInstance()
        {
            soundManager.Instance.PlayingTrackChanged += OnPlayingTrackChanged;

            if (soundManager.Instance.PlayingTrack != null)
                OnPlayingTrackChanged(soundManager.Instance.PlayingTrack, true);
        }

        void DisableInstance()
        {
            StopTrackChangeCoroutines();

            if (soundManager.InstanceExists)
                soundManager.Instance.PlayingTrackChanged -= OnPlayingTrackChanged;
        }

        void OnPlayingTrackChanged(PlayingTrack playingTrack)
        {
            OnPlayingTrackChanged(playingTrack, false);
        }

        void OnPlayingTrackChanged(PlayingTrack playingTrack, bool immediateChange)
        {
            const double invalidBpm = 0d;

            var startTime = playingTrack.StartTime + playingTrack.BPMOffset;

            var bpm = playingTrack.BPM * BeatMultiplier;
            if (bpm.Approximately(invalidBpm))
            {
                if (!playingTrack.BPM.Approximately(invalidBpm))
                {
                    Debug.LogWarning(string.Format("BeatMultiplier {0} invalid. Using the original track BPM {1}.", BeatMultiplier, playingTrack.BPM));
                    bpm = playingTrack.BPM;
                }
                else
                {
                    const double fallbackBpm = 30d;
                    Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "track BPM", playingTrack.BPM, fallbackBpm));
                    bpm = fallbackBpm;
                }
            }

            double beatDuration = Constants.Arithmetic.Minute / bpm;
            double beatMultiplier = 1 / beatDuration;

            if(!beatMultiplier.IsFinite())
            {
                const double fallbackBeatDurationMultiplier = .5d;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "beatMultiplier", beatMultiplier, fallbackBeatDurationMultiplier));
                beatMultiplier = fallbackBeatDurationMultiplier;
            }

            StopTrackChangeCoroutines();

            if (immediateChange)
            {
                firstBeatTime = startTime;
                beatDurationMultiplier = beatMultiplier;
                trackChangeMultiplier = TrackChangeMultiplierMax;
                UpdateSway();
            }
            else
            {
                trackChangeCoroutine = SmoothTrackChange(startTime, beatMultiplier);
                StartCoroutine(trackChangeCoroutine);
            }
        }

        IEnumerator SmoothTrackChange(double startTime, double beatMultiplier)
        {
            if (Mathf.Approximately(beatSwayDefinition.trackChangeDuration, 0f))
            {
                firstBeatTime = startTime;
                beatDurationMultiplier = beatMultiplier;
                trackChangeMultiplier = TrackChangeMultiplierMax;
                trackChangeCoroutine = null;
                yield break;
            }

            const float trackChangeMultiplierMin = 0f;
            trackChangeAnimationCoroutine = TrackChangeAnimation(trackChangeMultiplierMin);
            yield return StartCoroutine(trackChangeAnimationCoroutine);

            firstBeatTime = startTime;
            beatDurationMultiplier = beatMultiplier;

            trackChangeAnimationCoroutine = TrackChangeAnimation(TrackChangeMultiplierMax);
            yield return StartCoroutine(trackChangeAnimationCoroutine);

            trackChangeCoroutine = null;
        }

        IEnumerator TrackChangeAnimation(float finalMultiplierValue)
        {
            float trackChangeDuration = beatSwayDefinition.trackChangeDuration;

            float changeDurationMultiplier = 1 / trackChangeDuration;
            if (!changeDurationMultiplier.IsFinite())
            {
                const float fallbackTrackChangeDuration = 5f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "track change duration", trackChangeDuration, fallbackTrackChangeDuration));
                trackChangeDuration = fallbackTrackChangeDuration;
                changeDurationMultiplier = 1 / trackChangeDuration;
            }

            float beginningTrackChangeMultiplier = trackChangeMultiplier;

            float timePassed = 0f;
            while (timePassed <= trackChangeDuration)
            {
                yield return null;
                timePassed += Time.deltaTime;
                float curveValue = beatSwayDefinition.trackChangeCurve.Evaluate(timePassed * changeDurationMultiplier);
                trackChangeMultiplier = Mathf.Lerp(beginningTrackChangeMultiplier, finalMultiplierValue, curveValue);
            }

            trackChangeAnimationCoroutine = null;
        }

        void StopTrackChangeCoroutines()
        {
            StopCoroutine(ref trackChangeAnimationCoroutine);
            StopCoroutine(ref trackChangeCoroutine);
        }

        void StopCoroutine(ref IEnumerator coroutine)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }
    }
}