﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using PlantParty.Definitions;
using UnityExtras.Services;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Represents a discharge particle effect PlantParty.Flower behaviour.
    /// Particles are emitted from the EmitOrigin position and travel to the Target position.
    /// Automatically enables itself when Emitting and disables itself when no longer Emitting and out of active particles.
    /// Supports MonoBehaviour enable / disable. </summary>
    public class DischargeEffect : MonoBehaviour
    {
        [SerializeField, Tooltip("GameObjectPool provider used for the effect particle pooling.")]
        UnityEngine.Object gameObjectPoolProvider;

        [SerializeField, Tooltip("Configuration for the DischargeEffect behavour.")]
        DischargeEffectDefinition dischargeEffectDefinition;

        /// <summary>
        /// Allows specifying a custom event handler used to scale the particle emission frequency (optional). </summary>
        public event Func<float> EmitFrequencyMultiplier;

        /// <summary>
        /// Represents an event invoked each time a particle reaches its destination (optional). </summary>
        public event Action ParticleArrived;

        /// <summary>
        /// Allows specifying a custom event handler used to define a minimum proximity of a particle to the Target which can be randomly chosen as a particle destination. </summary>
        public event Func<float> MinimumArriveProximity;

        /// <summary>
        /// Enables / disables the particle emission. Enables the MonoBehaviour if needed.
        /// Default: false </summary>
        public bool Emitting
        {
            get { return emitting; }
            set { SetEmission(value); }
        }

        /// <summary>
        /// Returns / sets the particle emission origin position (world-space).
        /// Note: Emitted particle origin position is calculated by applying a deviation to this position accordingly to the linked DischargeEffectDefinition. </summary>
        public Vector3 EmitOrigin { get; set; }

        /// <summary>
        /// Returns / sets the particle emission target position (world-space).
        /// Note: Emitted particle target position is affected by a random minimum arrive proximity bounded by a result from the MinimumArriveProximity event. </summary>
        public Vector3 Target { get; set; }

        struct ParticleInfo
        {
            public GameObject pooledObject;
            public Vector3 target;
        }

        IServiceProvider<GameObjectPool> gameObjectPool;
        LinkedList<ParticleInfo> particles;
        float emitFrequency, internalTime, emitStartAfter, lastEmittedTime;
        bool emitting;

        void Awake()
        {
            gameObjectPool = gameObjectPoolProvider.AsServiceProvider<GameObjectPool>();

            emitFrequency = dischargeEffectDefinition.emitFrequency;

            const float invalidEmitFrequency = 0f;
            if (Mathf.Approximately(emitFrequency, invalidEmitFrequency))
            {
                const float fallbackEmitFrequency = 1f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "emit frequency", emitFrequency, fallbackEmitFrequency));
                emitFrequency = fallbackEmitFrequency;
            }

            particles = new LinkedList<ParticleInfo>();
            internalTime = Time.time;
        }

        void OnEnable()
        {
            EnableParticleObjects(true);
        }

        void Update()
        {
            if(Emitting)
                UpdateEmission();

            UpdateParticles();

            internalTime += Time.deltaTime;

            if (!Emitting && particles.Count == 0)
                enabled = false;
        }

        void OnDisable()
        {
            EnableParticleObjects(false);
        }

        void OnDestroy()
        {
            if (gameObjectPool.InstanceExists)
                ReleasePoolObjects();
        }

        void SetEmission(bool enableEmission)
        {
            if (!emitting && enableEmission)
            {
                emitStartAfter = internalTime + dischargeEffectDefinition.emitStartAfter;
                lastEmittedTime = float.MinValue;
                enabled = true;
            }

            emitting = enableEmission;
        }

        void ReleasePoolObjects()
        {
            foreach (var particle in particles)
            {
                gameObjectPool.Instance.PushItem(particle.pooledObject, dischargeEffectDefinition.effectPrefab);
            }

            particles.Clear();
        }

        void EnableParticleObjects(bool enable)
        {
            foreach (var particle in particles)
            {
                if(particle.pooledObject != null)
                    particle.pooledObject.SetActive(enable);
            }
        }

        /// <summary>
        /// Emits a new particle if needed, accordingly to the current emission frequency reading.
        /// Note: Current implementation limits the emission frequency to the framerate. This is sufficient given the use within the game.
        ///       For a framerate-independent particle effect implementation see the FeaturedEffect MonoBehaviour. </summary>
        void UpdateEmission()
        {
            if (emitStartAfter > internalTime)
                return;

            float emitPeriod = 1 / CurrentEmitFrequency();
            if (!emitPeriod.IsFinite())
            {
                const float fallbackEmitPeriod = 5f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "emit period", emitPeriod, fallbackEmitPeriod));
                emitPeriod = fallbackEmitPeriod;
            }

            if(internalTime < lastEmittedTime + emitPeriod)
                return;

            GameObject pooledObject = gameObjectPool.Instance.PopItem(dischargeEffectDefinition.effectPrefab);
            if (pooledObject)
                RegisterParticle(pooledObject);
            else
                Debug.LogWarning("No object retrieved from the GameObjectPool");

            lastEmittedTime = internalTime;
        }

        float CurrentEmitFrequency()
        {
            return (EmitFrequencyMultiplier != null ? emitFrequency * EmitFrequencyMultiplier() : emitFrequency);
        }

        void UpdateParticles()
        {
            float moveStep = dischargeEffectDefinition.speed * Time.deltaTime;
            float rotateStep = dischargeEffectDefinition.rotationSpeed * Time.deltaTime;

            var particleNode = particles.First;
            while (particleNode != null)
            {
                var nextParticleNode = particleNode.Next;

                var particleInfo = particleNode.Value;
                GameObject particleObject = particleInfo.pooledObject;
                particleObject.transform.position = Vector3.MoveTowards(particleObject.transform.position, particleInfo.target, moveStep);
                particleObject.transform.Rotate(Vector3.forward * rotateStep);

                // Note that the Vector3 equality operator checks if vectors are approximately equal.
                if (particleObject.transform.position == particleInfo.target)
                {
                    gameObjectPool.Instance.PushItem(particleObject, dischargeEffectDefinition.effectPrefab);
                    particles.Remove(particleNode);

                    if (ParticleArrived != null)
                        ParticleArrived();
                }

                particleNode = nextParticleNode;
            }
        }

        void RegisterParticle(GameObject particleObject)
        {
            particleObject.transform.position = EmitOrigin + (Vector3)(Random.insideUnitCircle * dischargeEffectDefinition.emitStartPositionDeviation);
            particleObject.transform.localScale = Vector3.one * dischargeEffectDefinition.scaleSpan.RandomContained();

            Vector3 targetOffset = Vector3.zero;
            if (MinimumArriveProximity != null)
            {
                float arriveDistance = Random.Range(0f, MinimumArriveProximity());
                targetOffset = (EmitOrigin - Target).normalized * arriveDistance;
            }

            particles.AddLast(new ParticleInfo
            {
                pooledObject = particleObject,
                target = Target + targetOffset
            });
        }
    }
}
