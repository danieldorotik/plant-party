﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace PlantParty
{
    /// <summary>
    /// Represents a StandaloneInputModule with a FakeTouch support.
    /// Assigned FakeTouches will invoke Unity Events identically to the real input touches. </summary>
    public class GameInputModule : StandaloneInputModule
    {
        [SerializeField, Tooltip("Contains assigned FakeTouches to be processed.")]
        List<FakeTouch> fakeTouches;

        Dictionary<FakeTouch, Touch> touchesLookup;

        /// <summary>
        /// Activates the module on standard circumstances or if there is any non-cancelled FakeTouch assigned. </summary>
        public override bool ShouldActivateModule()
        {
            if (base.ShouldActivateModule())
                return true;

            foreach (var touch in touchesLookup)
            {
                if (touch.Value.phase != TouchPhase.Canceled)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Processes input in a StandaloneInputModule fashion and then processes the FakeTouch input. </summary>
        public override void Process()
        {
            base.Process();

            if(fakeTouches != null)
                ProcessFakeTouchEvents();
        }

        protected override void Awake()
        {
            base.Awake();
            touchesLookup = new Dictionary<FakeTouch, Touch>();
        }

        void ProcessFakeTouchEvents()
        {
            foreach(var fakeTouch in fakeTouches)
            {
                Touch touch;
                if(!touchesLookup.TryGetValue(fakeTouch, out touch))
                {
                    touch = new Touch
                    {
                        fingerId = fakeTouch.FingerId,
                        phase = TouchPhase.Canceled,
                        position = fakeTouch.Position,
                        type = TouchType.Direct
                    };
                    touchesLookup.Add(fakeTouch, touch);
                }
                                
                if(fakeTouch.Active && fakeTouch.Pressed)
                {
                    if (touch.phase != TouchPhase.Moved)
                        touch.phase = (touch.phase != TouchPhase.Began) ? TouchPhase.Began : TouchPhase.Moved;

                    touch.position = fakeTouch.Position;
                    touchesLookup[fakeTouch] = touch;
                }
                else
                {
                    if(touch.phase == TouchPhase.Canceled)
                        continue;

                    if (touch.phase == TouchPhase.Ended)
                    {
                        touch.phase = TouchPhase.Canceled;
                        touchesLookup[fakeTouch] = touch;
                        continue;
                    }

                    touch.phase = TouchPhase.Ended;
                    touch.position = fakeTouch.Position;
                    touchesLookup[fakeTouch] = touch;
                }

                bool released;
                bool pressed;
                var pointer = GetTouchPointerEventData(touch, out pressed, out released);
                
                ProcessTouchPress(pointer, pressed, released);

                if (!released)
                {
                    ProcessMove(pointer);
                    ProcessDrag(pointer);
                }
                else
                    RemovePointerData(pointer);
            }
        }
    }
}
