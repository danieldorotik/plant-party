﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
using UnityEngine.UI;
using UnityExtras;
using UnityExtras.Services;
#endif

namespace PlantParty
{
    /// <summary>
    /// Monitors utilization of a specified AudioSourcePool and updates a specified Text component accordingly.
    /// Intended for development diagnostic purposes. </summary>
    public class AudioSourcePoolUtilization : MonoBehaviour
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        [SerializeField, Tooltip("AudioSourcePool provider through which the specified AudioSourcePool is interfaced.")]
        Object audioSourcePoolProvider;

        [SerializeField, Tooltip("Text component used to visualize the monitored AudioSourcePool utilization.")]
        Text text;

        IServiceProvider<AudioSourcePool> audioSourcePool;
        int lastUtilization;

        void Awake()
        {
            audioSourcePool = audioSourcePoolProvider.AsServiceProvider<AudioSourcePool>();

            if (!text)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(Text)));
                text = GetComponent<Text>() ?? gameObject.AddComponent<Text>();
            }
        }

        void Update()
        {
            if (audioSourcePool.Instance.Utilization != lastUtilization)
            {
                lastUtilization = audioSourcePool.Instance.Utilization;
                text.text = string.Format("Used audio sources: {0}", lastUtilization);
            }
        }
#endif
    }
}
