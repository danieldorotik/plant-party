﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityExtras.Services;

namespace PlantParty
{
    /// <summary>
    /// Provides general information regarding user activity.
    /// Supports monitoring activity of a simulated user input (FakeTouch). </summary>
    public class InputManager : MonoBehaviour
    {
        [SerializeField, Tooltip("A CanvasManager service provider. Used for ignoring activity of pointers over UI (see the ignoreActivityOverUI member).")]
        UnityEngine.Object canvasManagerProvider;

        /// <summary>
        /// Activity of pointers located over UI elements is ignored if this member is set to true.
        /// Has effect only on the activity monitoring status properties (OngoingActivity, LastActivePosition, LastActiveTime).
        /// Note: The canvasManagerProvider member must be set in order for this functionality to take effect. </summary>
        [SerializeField, Tooltip("Activity of pointers located over UI elements is ignored if this member is set to true.")]
        bool ignoreActivityOverUI;

        [SerializeField, Tooltip("List referencing the FakeTouch instances for activity monitoring (optional).")]
        List<FakeTouch> fakeTouches;        

        /// <summary>
        /// An event invoked at the beginning of a new activity (from a previous inactivity). </summary>
        public event Action ActivityBegan;

        /// <summary>
        /// An event invoked during a frame the TESTED_MOUSE_BUTTON is released. </summary>
        public event Action MouseReleased;

        /// <summary>
        /// Position of the last active pointer. </summary>
        public Vector3? LastActivePosition { get; private set; }

        /// <summary>
        /// Time of the last activity. </summary>
        public float? LastActiveTime { get; private set; }

        /// <summary>
        /// Returns true if the last activity test was positive. </summary>
        public bool OngoingActivity { get; private set; }

        IServiceProvider<CanvasManager> canvasManager;
        
        const int MouseLMB = 0;
        const int TestedMouseButton = MouseLMB;

        /// <summary>
        /// Returns true if a specified pointer is pressed.
        /// Optionally it possible to omit pointers located over UI elements from this test. </summary>
        public bool IsPointerPressed(int pointerId, bool ignorePointersOverUI = false)
        {
            const int mousePointerId = -1;

            return (FindActiveTouch(ignorePointersOverUI, pointerId).HasValue ||
                    FindActiveFakeTouch(ignorePointersOverUI, pointerId) != null ||
                    pointerId == mousePointerId && FindActiveMousePress(ignorePointersOverUI, TestedMouseButton).HasValue);
        }

        /// <summary>
        /// Overrides values returned by the LastActivePosition and the LastActiveTime properties. </summary>
        public void OverrideLastActivity(Vector3 position, float time)
        {
            LastActivePosition = position;
            LastActiveTime = time;
        }

        void Awake()
        {
            canvasManager = canvasManagerProvider.AsServiceProvider<CanvasManager>();
            if (canvasManager == null && ignoreActivityOverUI)
                Debug.LogWarning("CanvasManager provider not linked, thus the UI activity ignoring will be omitted.");
        }

        void Update()
        {
            UpdateActivity();
            TestMouseReleased();
        }

        void UpdateActivity()
        {
            Vector2? pointerPosition = FindActivePointer();

            if(pointerPosition.HasValue)
            {
                LastActivePosition = pointerPosition;
                LastActiveTime = Time.time;
            }

            if (!OngoingActivity && pointerPosition.HasValue && (ActivityBegan != null))
                ActivityBegan();

            OngoingActivity = pointerPosition.HasValue;
        }

        void TestMouseReleased()
        {
            if (Input.GetMouseButtonUp(TestedMouseButton) && (MouseReleased != null))
                MouseReleased();
        }

        Vector2? FindActivePointer()
        {
            Touch? touch = FindActiveTouch(ignoreActivityOverUI);
            if (touch.HasValue)
                return touch.Value.position;

            FakeTouch fakeTouch = FindActiveFakeTouch(ignoreActivityOverUI);
            return (fakeTouch ? fakeTouch.Position : FindActiveMousePress(ignoreActivityOverUI, TestedMouseButton));
        }

        Touch? FindActiveTouch(bool ignoreTouchesOverUI, int? desiredPointerID = null)
        {
            for (int i = 0; i < Input.touchCount; ++i)
            {
                Touch touch = Input.GetTouch(i);

                if (!IsActiveTouchPhase(touch.phase))
                    continue;

                if (ignoreTouchesOverUI && IsOverUI(touch.position))
                    continue;

                if (desiredPointerID != null && touch.fingerId != desiredPointerID.Value)
                    continue;

                return touch;
            }

            return null;
        }

        FakeTouch FindActiveFakeTouch(bool ignoreTouchesOverUI, int? desiredPointerID = null)
        {
            if (fakeTouches != null)
            {
                foreach (var fakeTouch in fakeTouches)
                {
                    if (!fakeTouch.Active || !fakeTouch.Pressed)
                        continue;

                    if (ignoreTouchesOverUI && IsOverUI(fakeTouch.Position))
                        continue;

                    if (desiredPointerID != null && fakeTouch.FingerId != desiredPointerID.Value)
                        continue;

                    return fakeTouch;
                }
            }

            return null;
        }

        Vector2? FindActiveMousePress(bool ignoreMouseOverUI, int mouseButton)
        {
            if (Input.GetMouseButton(mouseButton) && !(ignoreMouseOverUI && IsOverUI(Input.mousePosition)))
                return Input.mousePosition;

            return null;
        }

        bool IsOverUI(Vector2 screenPosition)
        {
            return (canvasManager != null && canvasManager.Instance.IsOverUI(screenPosition));
        }

        static bool IsActiveTouchPhase(TouchPhase phase)
        {
            return (phase != TouchPhase.Ended && phase != TouchPhase.Canceled);
        }
    }
}