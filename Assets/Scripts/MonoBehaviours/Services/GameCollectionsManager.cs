﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Data;
using System.Collections.Generic;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty.GameCollections
{
    /// <summary>
    /// Represents a collection manager which initializes and handles assigned game collections and the game collection screen.
    /// Provides overall collection operations.
    /// By default the game collection screen is made invisible. </summary>
    public class GameCollectionsManager : MonoBehaviour
    {
        [SerializeField, Tooltip("Specifies a list of collections to be handled by the manager.")]
        List<StoredGameCollection> storedCollections;

        [SerializeField, Tooltip("Specifies a collection to be retrieved by the MusicCollection property.\nNote: Has to be included to the storedCollections list.")]
        StoredGameCollection musicCollection;

        [SerializeField, Tooltip("Specifies a collection to be retrieved by the PlayObjectCollection property.\nNote: Has to be included to the storedCollections list.")]
        StoredGameCollection playObjectCollection;

        [SerializeField, Tooltip("Specifies a collection to be retrieved by the SceneCollection property.\nNote: Has to be included to the storedCollections list.")]
        StoredGameCollection sceneCollection;

        [SerializeField, Tooltip("Specifies the original game collection screen prefab.")]
        GameObject collectionScreenPrefab;

        [SerializeField, Tooltip("Specifies a game camera builder used for the game collection screen camera creation.")]
        GameCameraBuilder cameraBuilder;

        /// <summary>
        /// Retrieves a game collection assigned for music collectibles. </summary>
        public GameCollection MusicCollection
        {
            get { return musicCollection.Data; }
        }

        /// <summary>
        /// Retrieves a game collection assigned for play object collectibles. </summary>
        public GameCollection PlayObjectCollection
        {
            get { return playObjectCollection.Data; }
        }

        /// <summary>
        /// Retrieves a game collection assigned for scene collectibles. </summary>
        public GameCollection SceneCollection
        {
            get { return sceneCollection.Data; }
        }

        /// <summary>
        /// Retrieves a camera of the game collection screen. </summary>
        public GameCamera CollectionCamera { get; private set; }

        /// <summary>
        /// Returns / sets the game collection screen visibility.
        /// Note: Game object hierarchy of the game collection screen is disabled when set to invisible. </summary>
        public bool CollectionVisible
        {
            get { return collectionVisible; }
            set { ShowCollection(value); }
        }

        GameObject collectionScreen;
        SpriteRenderer collectionScreenRenderer;
        bool collectionVisible;
        
        /// <summary>
        /// Returns the sum of locked items of all the assigned collections. </summary>
        public int LockedItemsCount()
        {
            int count = 0;

            foreach (var collection in storedCollections)
            {
                count += collection.Data.LockedItemsCount;
            }

            return count;
        }

        /// <summary>
        /// Unlocks and returns a collectible randomly chosen from all the assigned collections.
        /// Returns null if there is no collectible to unlock.
        /// Regarding the pick probability - it's the same as if all the locked items were in the same array from which they're randomly picked. </summary>
        public GameCollectible UnlockRandomCollectible()
        {
            int lockedItemsCount = LockedItemsCount();
            if (lockedItemsCount == 0)
                return null;

            int randomIndex = Random.Range(0, lockedItemsCount);
            int top = 0;

            foreach (var collection in storedCollections)
            {
                top += collection.Data.LockedItemsCount;
                if (randomIndex < top)
                    return collection.Data.UnlockRandomItem();
            }

            return null;
        }     

        /// <summary>
        /// Stores the assigned collections. </summary>
        public void SaveCollections()
        {
            foreach (var collection in storedCollections)
            {
                collection.Save();
            }
        }

        void Awake()
        {
            LoadCollections();

            AssureCollection(ref musicCollection, "MusicCollection");
            AssureCollection(ref playObjectCollection, "PlayObjectCollection");
            AssureCollection(ref sceneCollection, "SceneCollection");

            CreateCollectionScreen(collectionScreenPrefab, out collectionScreen, out collectionScreenRenderer);

            if (!cameraBuilder)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, typeof(GameCameraBuilder)));
                cameraBuilder = GameCameraBuilder.FallbackInstance();
            }

            CollectionCamera = cameraBuilder.Build(collectionScreen.transform);
            CollectionCamera.ScreeningArea = collectionScreenRenderer.Area();

            CollectionVisible = false;
        }

        void ShowCollection(bool show)
        {
            collectionVisible = show;
            collectionScreen.SetActive(collectionVisible);
        }

        void LoadCollections()
        {
            for(int i = storedCollections.Count - 1; i >= 0; --i)
            {
                if(!storedCollections[i])
                {
                    storedCollections.RemoveAt(i);
                    continue;
                }

                storedCollections[i].Load();
            }
        }

        void AssureCollection(ref StoredGameCollection collection, string logName)
        {
            if (!collection || !collection.Data)
            {
                Debug.LogError(string.Format("{0} is not properly linked, creating a default instance...", logName));
                collection = StoredGameCollection.FallbackInstance();
                collection.Load();
            }

            if (!CollectionRegistered(collection))
                Debug.LogError(string.Format("The '{0}' collection is not properly registered to the collections manager and will not be handled by it.", logName));
        }

        bool CollectionRegistered(StoredGameCollection testedCollection)
        {
            foreach (var collection in storedCollections)
            {
                if (testedCollection == collection)
                    return true;
            }

            return false;
        }

        static void CreateCollectionScreen(GameObject screenPrefab, out GameObject collectionScreen, out SpriteRenderer collectionRenderer)
        {
            if (!screenPrefab)
            {
                const string collectionScreenName = "CollectionScreen";
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, collectionScreenName));
                collectionScreen = new GameObject(collectionScreenName);
            }
            else
                collectionScreen = Instantiate(screenPrefab);

            DontDestroyOnLoad(collectionScreen);

            GameBackground gameBackground = collectionScreen.GetComponentInChildren<GameBackground>();
            if(gameBackground && gameBackground.SpriteRenderer)
            {
                collectionRenderer = gameBackground.SpriteRenderer;
                return;
            }

            if ((collectionRenderer = collectionScreen.GetComponentInChildren<SpriteRenderer>()) != null)
                return;

            Debug.LogError("Cannot find a SpriteRenderer neither in the GameBackground nor object hierarchy of the game collection screen. Adding a default one...");
            collectionRenderer = collectionScreen.AddComponent<SpriteRenderer>();
        }
    }
}