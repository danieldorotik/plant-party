﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Data;
using PlantParty.GameCollections;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PlantParty
{
    /// <summary>
    /// Custom handler of a change scene operation. </summary>
    public delegate void ChangeSceneResultHandler(bool success);

    /// <summary>
    /// Changes game scene safely with a fallback and tests that it's valid (contains a StoredGameScene reference). </summary>
    public class GameSceneManager : MonoBehaviour
    {
        [SerializeField, Tooltip("The default game scene used during a scene change fallback and by the ChangeToDefaultScene method.")]
        SceneCollectible defaultScene;

        /// <summary>
        /// The StoredGameScene retrieved from the currently active scene. </summary>
        public StoredGameScene CurrentScene { get; private set; }

        /// <summary>
        /// Name of the currently active scene. </summary>
        public string CurrentSceneName
        {
            get { return SceneManager.GetActiveScene().name; }
        }

        /// <summary>
        /// Name of the configured default scene (if any) or an empty string. </summary>
        public string DefaultSceneName
        {
            get { return defaultScene ? defaultScene.sceneName : string.Empty; }
        }

        /// <summary>
        /// Invoked each time a scene is about to be changed (can get invoked twice in case of a fallback). </summary>
        public event Action SceneChanging;

        /// <summary>
        /// Attempts to load a specified scene and retrieve its StoredGameScene reference which is then accessible through the CurrentScene property.
        ///   - result of the operation is known the following frame, hence its optionally possible to specify a custom result handler
        ///   - on a failure the method automatically falls back by changing to a default scene (can be optionally disabled using the fallbackToDefault parameter). </summary>
        public void ChangeScene(string sceneName, ChangeSceneResultHandler resultHandler = null, bool fallbackToDefault = true)
        {
            if (SceneChanging != null)
                SceneChanging();

            if (string.IsNullOrEmpty(sceneName))
            {
                Debug.LogError("Cannot change to a scene with an empty name");
                if (resultHandler != null)
                    resultHandler(success: false);

                return;
            }

            StartCoroutine(ChangeSceneCoroutine(sceneName, resultHandler, fallbackToDefault));
        }

        /// <summary>
        /// Attempts to load a default scene and retrieve its StoredGameScene reference which is then accessible through the CurrentScene property.
        ///   - result of the operation is known the following frame, hence its optionally possible to specify a custom result handler </summary>       
        public void ChangeToDefaultScene(ChangeSceneResultHandler resultHandler = null)
        {
            ChangeScene(DefaultSceneName, resultHandler, fallbackToDefault: false);
        }

        IEnumerator ChangeSceneCoroutine(string sceneName, ChangeSceneResultHandler resultHandler, bool fallbackToDefault)
        {
            // Free the current scene data
            CurrentScene = null;

            SceneManager.LoadScene(sceneName);

            // Scene is effectively loaded the next frame, so let's wait to check
            yield return null;

            bool result = (TestActiveScene(sceneName) && (CurrentScene = LoadStoredGameScene()));

            if (resultHandler != null)
                resultHandler(result);

            if (!result && fallbackToDefault)
            {
                Debug.Log(string.Format("Falling back to a default scene '{0}'.", DefaultSceneName));
                ChangeToDefaultScene(resultHandler);
            }
        }

        static bool TestActiveScene(string desiredSceneName)
        {
            Scene activeScene = SceneManager.GetActiveScene();
            bool sceneMatches = (activeScene.name == desiredSceneName);

            if(!sceneMatches)
                Debug.LogError(string.Format("Scene '{0}' expected, but the current scene is '{1}'.", desiredSceneName, activeScene.name));

            return sceneMatches;
        }

        static StoredGameScene LoadStoredGameScene()
        {
            try
            {
                GameObject backgroundObject = GameObject.FindWithTag(GameBackground.Tag);
                if (!backgroundObject)
                    throw new UnityException(string.Format("Cannot find a game object tagged '{0}' in the loaded scene", GameBackground.Tag));

                GameBackground backgroundScript = backgroundObject.GetComponent<GameBackground>();
                if (!backgroundScript)
                    throw new UnityException(string.Format("The '{0}' object does not have the GameBackground script", backgroundObject.name));

                if(!backgroundScript.GameScene)
                    throw new UnityException(string.Format("The '{0}' object does not have a game scene set to its GameBackground script", backgroundObject.name));

                backgroundScript.GameScene.Load();

                return backgroundScript.GameScene;
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
                return null;
            }
        }
    }
}