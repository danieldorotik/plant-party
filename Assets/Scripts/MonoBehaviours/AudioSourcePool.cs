﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras;

namespace PlantParty
{
    /// <summary>
    /// Represents a pool of AudioSource components.
    /// Note: AutoExpand is turned off by default. </summary>
    public class AudioSourcePool : Pool<AudioSource>
    {
        protected override void Awake()
        {
            base.Awake();
            AutoExpand = false;
        }

        protected override AudioSource CreateItem()
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.bypassEffects = true;
            audioSource.bypassReverbZones = true;
            return audioSource;
        }

        protected override void PrepareItem(AudioSource item)
        {
        }

        protected override void CleanupItem(AudioSource item)
        {
            if(!item)
            {
                Debug.LogWarning("Cannot cleanup a null item");
                return;
            }

            if (item.isPlaying)
                item.Stop();

            item.outputAudioMixerGroup = null;
            item.clip = null;
        }

        protected override void DestroyItem(AudioSource item)
        {
            if (!item)
            {
                Debug.LogWarning("Cannot destroy a null item");
                return;
            }

            Destroy(item);
        }
    }
}
