﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections.Generic;
using System;
using UnityExtras;
using UnityExtras.ExtensionMethods;
using PlantParty.Definitions;
using PlantParty.GameCollections;
using UnityExtras.Services;

namespace PlantParty
{
    /// <summary>
    /// Represents the main logic of a PlantParty.Flower. 
    /// Controls its states and drives the associated behaviours.
    /// Supports MonoBehaviour enable / disable. </summary>
    public class Flower : MonoBehaviour
    {
        [SerializeField, Tooltip("GameManager provider used by a Flower instance in state FlowerState.Grown to get a currently growing flower for charging.")]
        UnityEngine.Object gameManagerProvider;

        [SerializeField, Tooltip("SoundManager provider used to play special sound effects (e.g. the growth success sound).")]
        UnityEngine.Object soundManagerProvider;

        [SerializeField, Tooltip("Configuration for the Flower behaviour.")]
        FlowerDefinition flowerDefinition;

        /// <summary>
        /// The associated behaviour which acts as a multi-zone button and drives the BeatPlayer and Animator components of a PlantParty.Flower object.
        /// Used by a Flower in FlowerState.Grown to find out if the Button is pressed (for charging of a growing flower), 
        /// to animate the object in special cases and to set a custom event handler to the Button's Interact event. </summary>
        [SerializeField, Tooltip("The associated behaviour which acts as a multi-zone button and drives the BeatPlayer and Animator components of a PlantParty.Flower object.")]
        PlayButton playButton;

        [SerializeField, Tooltip("The associated behaviour which animates part of a Flower GameObject synchronously to a currently playing music track.")]
        BeatSway beatSway;

        [SerializeField, Tooltip("The associated custom particle effect behaviour which visualizes the energy transfer from a grown flower into a growing flower.")]
        DischargeEffect dischargeEffect;

        /// <summary>
        /// Contains the visibility events invoked by a child GameObject due to its Renderer component. 
        /// The Flower behaviour assigns own event handlers to these events for activation / deactivation of given associated behaviours. </summary>
        [SerializeField, Tooltip("Contains the visibility events invoked by a child GameObject due to its Renderer component. ")]
        VisibilityCallback visibilityCallback;

        [SerializeField, Tooltip("Represents the main Flower collider which serves as a basis for its radius calculation.")]
        CircleCollider2D mainCollider;

        /// <summary>
        /// Represents a child GameObject which is rotated by the animation.
        /// Driven by the Flower behaviour not in state FlowerState.Grown, otherwise driven by the associated BeatSway behaviour. </summary>
        [SerializeField, Tooltip("Represents a child GameObject which is rotated by the animation.")]
        GameObject movingPart;

        [SerializeField, Tooltip("Represents a SpriteRenderer used for the charge amount visualization of a Flower in state FlowerState.Grown.")]
        SpriteRenderer grownChargeVisual;

        [SerializeField, Tooltip("Contains SpriteRenderers which have their transparency altered during the Flower's growth process (accordingly to the assigned FlowerDefinition).")]
        SpriteRenderer[] growthTransparencySprites;

        [SerializeField, Tooltip("Contains associated Behaviours which are enabled / disabled by this behaviour accordingly to its performance optimization logic.")]
        Behaviour[] activatedBehaviours;
#if UNITY_EDITOR
#pragma warning disable 0649
        [SerializeField, Tooltip("Controls the display of the corresponding debug shape in the Unity Editor.")]
        bool debugDrawCircleColliders, debugDrawRadius;
#pragma warning restore 0649
#endif
        /// <summary>
        /// Allows to specify a custom event handler indicating whether the Flower instance shall consider itself as interactive.
        /// The Flower uses this information for a performance optimization of disabling associated behaviours which are needed only for an interactive Flower. </summary>
        public event Func<bool> Interactive;

        /// <summary>
        /// Represents an event invoked when a Flower changes its state to a FlowerState.Grown. </summary>
        public event Action<Flower> GrownStateStarted;

        /// <summary>
        /// Allows to specify a custom event handler for multiplying the transparency of the grownChargeVisual SpriteRenderer.
        /// Note: The custom event handler result is clamped by the OPAQUE_MIN and OPAQUE_MAX constants before applied. 
        ///       Bringing the transparency down to zero disables the SpriteRenderer, otherwise it's enabled. </summary>
        public event Func<float> GrowthChargeVisualMultiplier;

        /// <summary>
        /// Represents a collectible containing a prefab from which the Flower instance was cloned.
        /// Used during the game scene load / save operations. </summary>
        public PlayObjectCollectible OriginCollectible { get; set; }

        /// <summary>
        /// Returns / sets the current state of a Flower instance. </summary>
        public FlowerState State
        {
            get { return stateMachine.CurrentState; }
            set { stateMachine.CurrentState = value; }
        }

        /// <summary>
        /// Returns true if a Flower instance is in a state of ongoing growth (expanding or shrinking). </summary>
        public bool InGrowth
        {
            get { return State.In(FlowerState.Emerging, FlowerState.Growing, FlowerState.Dying); }
        }

        /// <summary>
        /// Returns a world-space radius measurement of a Flower instance. </summary>
        public float Radius
        {
            get { return MainColliderRadius(); }
        }

        /// <summary>
        /// Returns / sets the charge amount of a Flower instance (effective on a Flower in state FlowerState.Grown). </summary>
        public float GrownCharge
        {
            get { return grownCharge; }
            set { grownCharge = Mathf.Clamp(value, GrownChargeMin, GrownChargeMax); }
        }

        /// <summary>
        /// Returns the maximum possible GrownCharge value. </summary>
        public float GrownChargeMax
        {
            get { return flowerDefinition.flowerStates.grown.capacity; }
        }

        float GrowthProgress
        {
            get { return growthProgress; }
            set { growthProgress = Mathf.Clamp(value, GrowthProgressMin, GrowthProgressMax); }
        }

        struct SpriteInfo
        {
            public SpriteRenderer spriteRenderer;
            public Color originalColor;
        }

        IServiceProvider<GameManager> gameManager;
        IServiceProvider<SoundManager> soundManager;
        List<SpriteInfo> growthTransparencySpritesInfo;
        bool isVisible, behavioursActive;
        Vector3 grownRadiusLocalScale;
        float growthProgress, frameGrowthSpeed, manualShrinkDuration, grownCharge, grownCapacityMultiplier;
        Color grownChargeVisualOriginalColor;
        ExponentialMovingAverage growthSpeed;
        LiteStateMachine<FlowerState> stateMachine;
        
        const float DefaultGrowthSpeed = 0f;
        const float DefaultFrameGrowthSpeed = 0f;
        const float GrowthProgressMin = 0f, GrowthProgressMax = 100f;
        const float GrowthProgressMaxMultiplier = 1 / GrowthProgressMax;
        const float GrownChargeMin = 0f;
        const float OpaqueMin = 0f, OpaqueMax = 1f;
        const float DurationZero = 0f;

        /// <summary>
        /// Adds a specified speed into the accumulative speed, which is scaled at the end of each frame by the Time.deltaTime, applied to the Flower growth speed and zeroed. </summary>        
        public void AddGrowthSpeed(float speed)
        {
            frameGrowthSpeed += speed;
        }

        /// <summary>
        /// Runs a standard beat animation clip of the associated PlayButton component. </summary>
        public void AnimateGrowthChargeReceipt()
        {
            playButton.AnimateBeat();
        }

        /// <summary>
        /// Sets a radius which the Flower instance will reach when switched to a state FlowerState.Grown.
        /// Note: Can be set regardless the current state of a Flower instance. </summary>
        public void SetGrownRadius(float grownRadius)
        {
            grownRadiusLocalScale = CalculateRadiusLocalScale(grownRadius);
        }

        void Awake()
        {
            gameManager = gameManagerProvider.AsServiceProvider<GameManager>();
            soundManager = soundManagerProvider.AsServiceProvider<SoundManager>();

            if (!playButton)
            {                
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(PlayButton)));
                playButton = GetComponent<PlayButton>() ?? gameObject.AddComponent<PlayButton>();
            }
            transform.CheckChildComponent(playButton);
            playButton.Interact += ResetManualShrinkDuration;

            if (!beatSway)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(BeatSway)));
                beatSway = GetComponent<BeatSway>() ?? gameObject.AddComponent<BeatSway>();
            }
            transform.CheckChildComponent(beatSway);
            beatSway.SwayAmount += GrownChargeNormalized;

            if (!dischargeEffect)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(DischargeEffect)));
                dischargeEffect = GetComponent<DischargeEffect>() ?? gameObject.AddComponent<DischargeEffect>();
            }
            transform.CheckChildComponent(dischargeEffect);
            HookupDischargeEffect(dischargeEffect);

            if (!flowerDefinition)
            {                
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, typeof(FlowerDefinition)));
                flowerDefinition = FlowerDefinition.FallbackInstance();
            }

            if (visibilityCallback)
            {
                transform.CheckChildComponent(visibilityCallback);
                visibilityCallback.BecameVisible += FlowerBecameVisible;
                visibilityCallback.BecameInvisible += FlowerBecameInvisible;
            }
            else
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinked, typeof(VisibilityCallback)));

            // The mainCollider has to be a direct or indirect child, since the Flower's radius scaling is based on this assumption.
            if (!mainCollider || mainCollider.transform.GetComponentInParent<Flower>() != this)
            {
                Debug.LogError("MainCollider not linked or is not a direct/indirect child. Adding a default one...");
                mainCollider = gameObject.AddComponent<CircleCollider2D>();
                mainCollider.isTrigger = true;
            }
            transform.CheckChildComponent(mainCollider);

            const float mainColliderOffsetTolerance = .01f;
            if (Vector2.Distance(mainCollider.WorldCenter(), transform.position) > mainColliderOffsetTolerance)
                Debug.LogError(string.Format("{0}'s mainCollider is not at its center. Flower's logic expects it to be aligned.", gameObject.name));

            if (movingPart)
                transform.CheckChild(movingPart.transform);
            else
                Debug.LogWarning(string.Format(Constants.DebugMessages.ObjectNotLinked, "MovingPart"));

            if (grownChargeVisual)
            {
                transform.CheckChildComponent(grownChargeVisual);
                grownChargeVisualOriginalColor = grownChargeVisual.color;
            }
            else
                Debug.LogWarning(string.Format(Constants.DebugMessages.ObjectNotLinked, "GrownChargeVisual"));

            growthSpeed = new ExponentialMovingAverage(flowerDefinition.flowerGrowth.speedEMATimeConstant, DefaultGrowthSpeed);            

            CheckChildComponents(growthTransparencySprites);
            CheckChildComponents(activatedBehaviours);

            InitializeSpritesInfo();            
            ActivateBehaviours(false);

            SetGrownRadius(MainColliderRadius());
            SetGrownCapacity();

            InitializeStateMachine();
            State = FlowerState.Grown;
        }

        void Update()
        {
            bool interactive = (Interactive == null || Interactive());
            bool activateBehaviours = isVisible && interactive && State != FlowerState.Dying;

            if (activateBehaviours != behavioursActive)
                ActivateBehaviours(activateBehaviours);

            stateMachine.Update();
        }

        void LateUpdate()
        {
            stateMachine.LateUpdate();
            frameGrowthSpeed = DefaultFrameGrowthSpeed;
        }
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (debugDrawCircleColliders)                
                DebugDrawCircleColliders(flowerDefinition.debugDrawCollidersColor);

            if(debugDrawRadius)
                DebugDrawRadius(flowerDefinition.flowerRadius.debugDrawColor);
        }

        void DebugDrawCircleColliders(Color color)
        {
            UnityEditor.Handles.color = color;

            CircleCollider2D[] colliders = transform.GetComponentsInChildren<CircleCollider2D>();
            foreach (var currentCollider in colliders)
            {
                UnityEditor.Handles.DrawWireDisc(currentCollider.WorldCenter(), Vector3.forward, currentCollider.WorldRadius());
            }
        }

        void DebugDrawRadius(Color color)
        {
            UnityEditor.Handles.color = color;
            UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, Radius);
        }
#endif
        void FlowerBecameVisible()
        {
            VisibilityChanged(true);
        }

        void FlowerBecameInvisible()
        {
            VisibilityChanged(false);
        }

        void VisibilityChanged(bool visible)
        {
            isVisible = visible;
            beatSway.enabled = (visible && State == FlowerState.Grown);
        }

        void ApplyGrowthSpeed()
        {
            GrowthProgress += (float)growthSpeed.Apply(frameGrowthSpeed, Time.deltaTime) * Time.deltaTime;
        }

        void DepictGrowth()
        {
            float visualGrowthProgress = flowerDefinition.flowerGrowth.progressOutcome.Evaluate(GrowthProgress * GrowthProgressMaxMultiplier);
            transform.localScale = Vector3.Lerp(Vector3.zero, grownRadiusLocalScale, visualGrowthProgress);

            if(!beatSway.enabled)
                RotateMovingPart(Mathf.LerpAngle(-beatSway.DefaultRotation, beatSway.DefaultRotation, visualGrowthProgress));            

            UpdateSpritesGrowthTransparency();
        }

        void DepictGrownCharge()
        {
            if (!grownChargeVisual)
                return;

            float alphaMultiplier = OpaqueMax;
            if (GrowthChargeVisualMultiplier != null)
                alphaMultiplier = Mathf.Clamp(GrowthChargeVisualMultiplier(), OpaqueMin, OpaqueMax);

            Color color = grownChargeVisual.color;
            color.a = alphaMultiplier * Mathf.Lerp(OpaqueMin, grownChargeVisualOriginalColor.a, GrownChargeNormalized());
            grownChargeVisual.color = color;
            grownChargeVisual.enabled = !Mathf.Approximately(color.a, OpaqueMin);            
        }

        float GrownChargeNormalized()
        {
            return GrownCharge * grownCapacityMultiplier;
        }

        /// <summary>
        /// Returns a local scale which if used, would affect the MainColliderRadius() method result to be equal to a specified value. </summary>
        Vector3 CalculateRadiusLocalScale(float radius)
        {
            float scaleMultiplier = radius / MainColliderRadius();
            if (!scaleMultiplier.IsFinite())
            {
                const float fallbackScaleMultiplier = 1f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "radius scale multiplier", scaleMultiplier, fallbackScaleMultiplier));
                scaleMultiplier = fallbackScaleMultiplier;
            }

            return transform.localScale * scaleMultiplier;
        }

        float MainColliderRadius()
        {
            const float mainColliderRadiusMultiplier = 1.075f;
            return mainCollider.WorldRadius() * mainColliderRadiusMultiplier;
        }

        void ActivateBehaviours(bool activate)
        {
            foreach (var behaviour in activatedBehaviours)
            {
                behaviour.enabled = activate;
            }

            behavioursActive = activate;
        }

        void CheckChildComponents(IEnumerable<Component> components)
        {
            foreach(var component in components)
            {
                transform.CheckChildComponent(component);
            }
        }
    
        void InitializeSpritesInfo()
        {
            growthTransparencySpritesInfo = new List<SpriteInfo>();            
            foreach (var spriteRenderer in growthTransparencySprites)
            {
                SpriteInfo spriteInfo;
                spriteInfo.spriteRenderer = spriteRenderer;
                spriteInfo.originalColor = spriteRenderer.color;
                growthTransparencySpritesInfo.Add(spriteInfo);
            }
        }

        void UpdateSpritesGrowthTransparency()
        {
            float interpolant = flowerDefinition.flowerGrowth.progressTransparency.Evaluate(GrowthProgress * GrowthProgressMaxMultiplier);
            foreach(var spriteInfo in growthTransparencySpritesInfo)
            {
                spriteInfo.spriteRenderer.color = spriteInfo.spriteRenderer.color.WithAlpha(Mathf.Lerp(OpaqueMin, spriteInfo.originalColor.a, interpolant));
            }
        }
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        void SetSpritesDebugColor(Color color)
        {
            foreach (var spriteInfo in growthTransparencySpritesInfo)
            {
                color.a = spriteInfo.spriteRenderer.color.a;
                spriteInfo.spriteRenderer.color = color;
            }
        }

        void DebugSetSpritesOriginalColor()
        {
            foreach (var spriteInfo in growthTransparencySpritesInfo)
            {
                spriteInfo.spriteRenderer.color = spriteInfo.originalColor;
            }
        }
#endif
        void RotateMovingPart(float angle)
        {
            if (!movingPart)
                return;

            movingPart.transform.rotation = Quaternion.Euler(Constants.Arithmetic.EulerAngleZero, Constants.Arithmetic.EulerAngleZero, angle);
        }

        void ResetManualShrinkDuration()
        {
            manualShrinkDuration = flowerDefinition.flowerStates.growing.manualShrinkDuration;
        }

        void SetGrownCapacity()
        {
            FlowerStateGrown grownState = flowerDefinition.flowerStates.grown;

            grownCapacityMultiplier = 1 / grownState.capacity;
            if (grownCapacityMultiplier.IsFinite())
                return;

            const float fallbackGrownCapacity = 1000f;
            Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "grown state capacity", grownState.capacity, fallbackGrownCapacity));
            grownCapacityMultiplier = 1 / fallbackGrownCapacity;
        }

        void GrowthChargeReceiptHandler()
        {
            if (gameManager.Instance.CurrentFlower)
                gameManager.Instance.CurrentFlower.AnimateGrowthChargeReceipt();
        }

        float GrowingFlowerRadius()
        {
            const float fallbackGrowingFlowerRadius = 0f;
            return gameManager.Instance.CurrentFlower ? gameManager.Instance.CurrentFlower.Radius : fallbackGrowingFlowerRadius;
        }

        void HookupDischargeEffect(DischargeEffect effect)
        {
            effect.EmitOrigin = transform.position;
            effect.EmitFrequencyMultiplier += GrownChargeNormalized;
            effect.ParticleArrived += GrowthChargeReceiptHandler;
            effect.MinimumArriveProximity += GrowingFlowerRadius;
        }

        void InitializeStateMachine()
        {
            stateMachine = new LiteStateMachine<FlowerState>(FlowerState.Invalid, new FlowerStateComparer())
            {
                { FlowerState.Emerging, EmergingStateEvents() },
                { FlowerState.Growing, GrowingStateEvents() },
                { FlowerState.Dying, DyingStateEvents() },
                { FlowerState.Grown, GrownStateEvents() }
            };            
        }

        LiteStateEvents EmergingStateEvents()
        {
            Definitions.FlowerState stateDefinition = flowerDefinition.flowerStates.emerging;

            return new LiteStateEvents
            {
                initialize = () =>
                {
                    beatSway.SetRandomSway();
                    GrowthProgress = stateDefinition.startGrowthProgress;
                    growthSpeed.Value = DefaultGrowthSpeed;
                    GrownCharge = GrownChargeMin;                    

#if DEVELOPMENT_BUILD || UNITY_EDITOR
                    if (stateDefinition.useDebugColor)
                        SetSpritesDebugColor(stateDefinition.debugColor);
                    else
                        DebugSetSpritesOriginalColor();
#endif
                    // Keep in mind that a gameObject instantiated during the MonoBehaviour.Update doesn't have its Update called during the same frame.
                    DepictGrowth();
                    DepictGrownCharge();
                },
                lateUpdate = () =>
                {
                    AddGrowthSpeed(stateDefinition.growthSpeed);
                    ApplyGrowthSpeed();
                    DepictGrowth();

                    if (GrowthProgress >= flowerDefinition.flowerStates.growing.startGrowthProgress)
                        State = FlowerState.Growing;
                }
            };
        }

        LiteStateEvents GrowingStateEvents()
        {
            FlowerStateGrowing stateDefinition = flowerDefinition.flowerStates.growing;

            return new LiteStateEvents
            {
                initialize = () =>
                {
                    manualShrinkDuration = DurationZero;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
                    if (stateDefinition.useDebugColor)
                    {
                        SetSpritesDebugColor(stateDefinition.debugColor);
                    }
                    else
                    {
                        DebugSetSpritesOriginalColor();
                        UpdateSpritesGrowthTransparency();
                    }
#endif
                },
                lateUpdate = () =>
                {
                    if (manualShrinkDuration > DurationZero)
                    {
                        manualShrinkDuration -= Time.deltaTime;
                        AddGrowthSpeed(stateDefinition.manualShrinkSpeed);
                    }

                    if (Mathf.Approximately(frameGrowthSpeed, DefaultFrameGrowthSpeed))
                        AddGrowthSpeed(stateDefinition.growthSpeed);

                    ApplyGrowthSpeed();
                    DepictGrowth();

                    if (GrowthProgress <= flowerDefinition.flowerStates.dying.startGrowthProgress)
                    {
                        State = FlowerState.Dying;
                    }
                    else if (GrowthProgress >= GrowthProgressMax)
                    {
                        soundManager.Instance.PlayEffect(stateDefinition.successEffectClip);
                        playButton.AnimateBeat();
                        State = FlowerState.Grown;
                    }
                }
            };
        }

        LiteStateEvents DyingStateEvents()
        {
            FlowerStateDying stateDefinition = flowerDefinition.flowerStates.dying;

            return new LiteStateEvents
            {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
                initialize = () =>
                {
                    if (stateDefinition.useDebugColor)
                    {
                        SetSpritesDebugColor(stateDefinition.debugColor);
                    }
                    else
                    {
                        DebugSetSpritesOriginalColor();
                        UpdateSpritesGrowthTransparency();
                    }
                },
#endif
                lateUpdate = () =>
                {
                    AddGrowthSpeed(stateDefinition.growthSpeed);
                    ApplyGrowthSpeed();
                    DepictGrowth();

                    if (GrowthProgress <= stateDefinition.dieProgress)
                        Destroy(gameObject);
                }
            };
        }

        LiteStateEvents GrownStateEvents()
        {
            FlowerStateGrown stateDefinition = flowerDefinition.flowerStates.grown;
            const float fallbackChargeAccelerationDuration = 10f;
            float chargeAccelerationDurationMultiplier = 1 / fallbackChargeAccelerationDuration;
            float chargingTime = DurationZero;
            bool lastCharging = false;
            Vector3 lastRadiusLocalScale = Vector3.zero;
#if DEVELOPMENT_BUILD || UNITY_EDITOR
            ProgressBar debugCapacityProgressbar = null;            
#endif
            return new LiteStateEvents
            {
                initialize = () =>
                {                    
#if DEVELOPMENT_BUILD || UNITY_EDITOR
                    DebugSetSpritesOriginalColor();

                    if(stateDefinition.showDebugCapacityProgresbar && stateDefinition.debugCapacityProgressbar)
                    {
                        const float debugProgressOffsetMultiplier = 1.1f;
                        GameObject progressbarObject = Instantiate(stateDefinition.debugCapacityProgressbar, transform.position + Vector3.left * Radius * debugProgressOffsetMultiplier, Quaternion.identity, transform);
                        debugCapacityProgressbar = progressbarObject.GetComponent<ProgressBar>();
                        if(!debugCapacityProgressbar)
                        {
                            Debug.LogWarning("Debug Progress Bar prefab has no ProgressBar");
                            Destroy(progressbarObject);
                        }
                    }
#endif
                    GrowthProgress = GrowthProgressMax;
                    growthSpeed.Value = DefaultGrowthSpeed;
                    lastRadiusLocalScale = grownRadiusLocalScale;
                    DepictGrowth();

                    chargeAccelerationDurationMultiplier = 1 / stateDefinition.chargeAccelerationDuration;
                    if (!chargeAccelerationDurationMultiplier.IsFinite())
                    {
                        Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "charge acceleration duration", stateDefinition.chargeAccelerationDuration, fallbackChargeAccelerationDuration));
                        chargeAccelerationDurationMultiplier = 1 / fallbackChargeAccelerationDuration;
                    }

                    chargingTime = DurationZero;

                    GrownCharge = GrownChargeMin;
                    DepictGrownCharge();

                    if (GrownStateStarted != null)
                        GrownStateStarted(this);

                    beatSway.enabled = isVisible;
                },
                deinitialize = () =>
                {
                    dischargeEffect.Emitting = false;
#if DEVELOPMENT_BUILD || UNITY_EDITOR
                    if (debugCapacityProgressbar)
                        Destroy(debugCapacityProgressbar.gameObject);
#endif
                },
                update = () =>
                {
                    Flower growingFlower = gameManager.Instance.CurrentFlower;
                    if (growingFlower && growingFlower.State != FlowerState.Growing)
                        growingFlower = null;

                    bool charging = !(playButton.Pressed && growingFlower);
                    chargingTime = (lastCharging == charging) ? chargingTime + Time.deltaTime : DurationZero;
                    float chargeSpeed = charging ? stateDefinition.chargeAcceleration.Evaluate(chargingTime * chargeAccelerationDurationMultiplier) * stateDefinition.chargeSpeedMax : stateDefinition.dischargeSpeed;

                    GrownCharge += chargeSpeed * Time.deltaTime;                    
#if DEVELOPMENT_BUILD || UNITY_EDITOR
                    if (debugCapacityProgressbar)
                        debugCapacityProgressbar.SetProgress(GrownChargeNormalized());
#endif
                    if (!charging && !Mathf.Approximately(GrownCharge, GrownChargeMin))
                    {
                        growingFlower.AddGrowthSpeed(stateDefinition.addGrowthSpeed);
                        dischargeEffect.Target = growingFlower.transform.position;
                        dischargeEffect.Emitting = true;
                    }
                    else
                        dischargeEffect.Emitting = false;                    

                    lastCharging = charging;
                },
                lateUpdate = () =>
                {
                    if (lastRadiusLocalScale != grownRadiusLocalScale)
                    {
                        transform.localScale = grownRadiusLocalScale;
                        lastRadiusLocalScale = grownRadiusLocalScale;
                        DepictGrowth();
                    }

                    DepictGrownCharge();
                }
            };
        }
    }
}
