﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using PlantParty.Definitions;
using System;
using System.Collections;
using UnityEngine;
using UnityExtras;
using UnityExtras.ExtensionMethods;

namespace PlantParty
{
    /// <summary>
    /// Animates zooming and 2D panning of an orthogonal camera. 
    /// When not zoomed the GameCamera automatically starts an animated 2D pan within its ScreeningArea.
    /// Automatically adjusts to a screen resize / orientation change.
    /// Supports MonoBehaviour enable / disable. </summary>
    public class GameCamera : MonoBehaviour
    {
        [SerializeField, Tooltip("Configuration for the GameCamera behaviour.")]
        CameraDefinition cameraDefinition;

        /// <summary>
        /// Specifies a Camera component which shall serve as a basis for the GameCamera calculations.
        /// The component is expected to be located on the same GameObject or on any of its child GameObjects. </summary>
        [SerializeField, Tooltip("Specifies a Camera component which shall serve as a basis for the GameCamera calculations.")]
        Camera cam;

        /// <summary>
        /// Represents an event invoked after a GameCamera animation-unrelated reconfiguration takes place.
        /// Occurs when the assigned Camera component changes its aspect ratio, when the GameCamera component gets enabled or when its ResetCamera method is called. </summary>
        public event Action Recalculated;

        /// <summary>
        /// Specifies a GameCamera Camera component. </summary>
        public Camera Camera
        {
            get { return cam; }
        }

        /// <summary>
        /// Returns / sets a bounding area for the GameCamera. The GameCamera makes sure its viewport never leaves this bounding area. </summary>
        public Area2 ScreeningArea { get; set; }

        /// <summary>
        /// Returns true if the GameCamera is in its zoomed state.
        /// Note: The GameCamera is considered zoomed since the start of its zoom animation. Use the FullyZoomed property to check whether it is completely zoomed. </summary>
        public bool IsZoomed { get; private set; }

        /// <summary>
        /// Returns true if the GameCamera is completely zoomed. </summary>
        public bool FullyZoomed
        {
            get { return IsZoomed && !IsAnimating; }
        }

        /// <summary>
        /// Returns an immediate amount of zoom of the GameCamera.
        /// Note: This value ranges between the ZOOM_AMOUNT_MIN and ZOOM_AMOUNT_MAX constants. </summary>
        public float ZoomAmount { get; private set; }

        /// <summary>
        /// Returns true if the GameCamera is currently animating itself (either for zooming or panning). </summary>
        public bool IsAnimating
        {
            get { return scrollingAnimation != null || zoomingAnimation != null; }
        }

        Vector2 targetPoint;
        float? lastScreenRatio;
        IEnumerator scrollingAnimation, zoomingAnimation;
        
        /// <summary>
        /// Resets the GameCamera accordingly to its current settings while stopping all its animations. </summary>
        public void ResetCamera()
        {
            targetPoint = new Vector2(0f, ScreeningArea.BottomLeft.y);
            IsZoomed = false;            

            StopAnimations();
            RecalculateCamera();
        }

        /// <summary>
        /// Returns a world-space point calculated from a specified screen-space point using the associated Camera component. </summary>
        public Vector3 ScreenToWorldPoint(Vector3 position)
        {
            return cam.ScreenToWorldPoint(position);
        }

        /// <summary>
        /// Returns a world-space Area2 which represents a portion of 2D space currently visible by the associated Camera component. </summary>
        public Area2 ScreenToWorldArea()
        {
            return new Area2(
                    cam.ScreenToWorldPoint(new Vector3(0f, 0f, cam.nearClipPlane)),
                    cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth, cam.pixelHeight, cam.nearClipPlane)));
        }

        /// <summary>
        /// Performs an animated centered zoom of the GameCamera into a specified world-point.
        /// Optionally the zoom operation can be carried out immediately without being animated.
        /// Note: The specified world-point may not become the camera's center, as the GameCamera always stays within its ScreeningArea bounds. </summary>
        public void ZoomTo(Vector2 point, bool animate = true)
        {
            targetPoint = point;
            IsZoomed = true;
            Zoom(animate);
        }
        
        /// <summary>
        /// Performs an animated zoom-out of the GameCamera into its ScreeningArea.
        /// Optionally the zoom-out operation can be carried out immediately without being animated. </summary>
        public void ZoomOut(bool animate = true)
        {
            IsZoomed = false;
            Zoom(animate);
        }

        void Awake()
        {            
            if (!cam)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(Camera)));                
                cam = GetComponent<Camera>() ?? gameObject.AddComponent<Camera>();
            }
            transform.CheckChildComponent(cam);

            if (!cameraDefinition)
            {                
                Debug.LogError(string.Format(Constants.DebugMessages.ObjectNotLinkedDefault, typeof(CameraDefinition)));
                cameraDefinition = CameraDefinition.FallbackInstance();
            }

            ScreeningArea = FallbackScreeningArea();
        }

        void OnEnable()
        {
            lastScreenRatio = null;
            AssureScreeningArea();
        }

        void Update()
        {
            AssureScreeningArea();

            if (!lastScreenRatio.HasValue || !Mathf.Approximately(cam.aspect, lastScreenRatio.Value))
            {
                StopAnimations();
                RecalculateCamera();
            }

            if (!IsZoomed && !IsAnimating)
                StartCameraScrolling();

            lastScreenRatio = cam.aspect;
        }

        void OnDisable()
        {
            StopAnimations();
        }

        void AssureScreeningArea()
        {
            if (!Mathf.Approximately(ScreeningArea.Min, 0f))
                return;

            Debug.LogWarning("Screening area invalid. Using the fallback screening area");
            ScreeningArea = FallbackScreeningArea();
        }

        void Zoom(bool animate)
        {
            StopAnimations();

            if (animate)
                StartCameraZoom();
            else
                RecalculateCamera();
        }

        void CalculateNewCameraSettings(out float orthoSize, out Vector3 position)
        {
            float zoomedSize = IsZoomed ? cameraDefinition.zooming.desiredZoomedSize : 0f;

            orthoSize = CalculateCameraOrthoSize(ScreeningArea, cam.aspect, zoomedSize);
            position = CalculateCameraPosition(ScreeningArea, targetPoint, orthoSize, cam.aspect);
            position += Vector3.forward * transform.position.z;
        }

        void RecalculateCamera()
        {
            float orthoSize;
            Vector3 position;

            CalculateNewCameraSettings(out orthoSize, out position);

            cam.orthographicSize = orthoSize;
            transform.position = position;

            const float zoomAmountMin = 0f, zoomAmountMax = 1f;
            ZoomAmount = IsZoomed ? zoomAmountMax : zoomAmountMin;

            if (Recalculated != null)
                Recalculated();
        }

        void StartCameraScrolling()
        {
            scrollingAnimation = AnimateCameraScrolling();
            StartCoroutine(scrollingAnimation);
        }

        IEnumerator AnimateCameraScrolling()
        {
            yield return new WaitForSeconds(cameraDefinition.autoScrolling.startAfterSecs);

            Vector3 closeWaypoint, distantWaypoint;
            CalculateScrollingWaypoints(out closeWaypoint, out distantWaypoint);

            IEnumerator animEnumerator = AnimateAutoScrolling(transform.position, distantWaypoint, false);
            while (animEnumerator.MoveNext())
                yield return null;

            animEnumerator = AnimateAutoScrolling(distantWaypoint, closeWaypoint);
            while (animEnumerator.MoveNext())
                yield return null;
        }

        IEnumerator AnimateAutoScrolling(Vector3 startWaypoint, Vector3 endWaypoint, bool loopScrolling = true)
        {
            const float fallbackTravelDuration = 30f;
            float startTime = Time.time;

            float travelDuration = Vector3.Distance(startWaypoint, endWaypoint) / cameraDefinition.autoScrolling.speed;
            if(!travelDuration.IsFinite())
            {
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "travel duration", travelDuration, fallbackTravelDuration));
                travelDuration = fallbackTravelDuration;
            }

            if (travelDuration < cameraDefinition.autoScrolling.minimumTravelDuration)
                travelDuration = cameraDefinition.autoScrolling.minimumTravelDuration;

            while (true)
            {
                float curveTime = (Time.time - startTime) / travelDuration;
                if (!curveTime.IsFinite())
                {
                    Debug.LogWarning("Curve time invalid, switching to a fallback travel duration value");
                    travelDuration = fallbackTravelDuration;
                    continue;
                }

                const float endCurveTime = 1f;

                if (!loopScrolling)
                    curveTime = Mathf.Clamp(curveTime, 0f, endCurveTime);

                float interpolant = cameraDefinition.autoScrolling.animationCurve.Evaluate(curveTime);
                Vector3 currentPosition = Vector3.Lerp(startWaypoint, endWaypoint, interpolant);
                transform.position = currentPosition;
                targetPoint = currentPosition;

                if (!loopScrolling && Mathf.Approximately(curveTime, endCurveTime))
                    break;

                yield return null;
            }
        }

        void CalculateScrollingWaypoints(out Vector3 closeWaypoint, out Vector3 distantWaypoint)
        {
            AssureScreeningArea();

            float screeningAspect = ScreeningArea.Size.x / ScreeningArea.Size.y;
            if(!screeningAspect.IsFinite())
                Debug.LogWarning("Screening area aspect ratio is not a finite number, scrolling direction may be invalid");
            
            bool horizontalScroll = (screeningAspect > cam.aspect);

            Vector2 cameraHalfSize;
            cameraHalfSize.y =  cam.orthographicSize;
            cameraHalfSize.x = cameraHalfSize.y * cam.aspect;

            Vector3 currentPosition = transform.position;
            Vector3 offset = Vector3.zero;
            bool positiveOffsetCloser;

            if (horizontalScroll)
            {
                offset.x = ScreeningArea.Size.x * Constants.Arithmetic.Half - cameraHalfSize.x;
                positiveOffsetCloser = (currentPosition.x > ScreeningArea.Center.x);
            }
            else
            {
                offset.y = ScreeningArea.Size.y * Constants.Arithmetic.Half - cameraHalfSize.y;
                positiveOffsetCloser = (currentPosition.y >= ScreeningArea.Center.y);
            }

            Vector3 center = (Vector3)ScreeningArea.Center + Vector3.forward * currentPosition.z;

            if (positiveOffsetCloser)
            {
                closeWaypoint = center + offset;
                distantWaypoint = center - offset;
            }
            else
            {
                closeWaypoint = center - offset;
                distantWaypoint = center + offset;
            }
        }

        void StartCameraZoom()
        {
            float orthoSize;
            Vector3 position;
            CalculateNewCameraSettings(out orthoSize, out position);

            zoomingAnimation = AnimateCameraZoom(orthoSize, position);
            StartCoroutine(zoomingAnimation);
        }

        IEnumerator AnimateCameraZoom(float finalOrthoSize, Vector3 destination)
        {
            float startOrthoSize = cam.orthographicSize;

            float startZoomAmount = IsZoomed ? startOrthoSize : finalOrthoSize;
            float finalZoomAmount = IsZoomed ? finalOrthoSize : startOrthoSize;

            const float fallbackZoomDuration = 10f;
            float zoomDuration = cameraDefinition.zooming.animationLengthSecs;
            if (zoomDuration <= 0)
            {
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "zooming animation length", zoomDuration, fallbackZoomDuration));
                zoomDuration = fallbackZoomDuration;
            }

            Vector3 startPosition = transform.position;
            float startTime = Time.time;

            float progress = 0f;
            while (progress < 1f)
            {
                progress = (Time.time - startTime) / zoomDuration;
                if (!progress.IsFinite())
                {
                    Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "zooming animation length", zoomDuration, fallbackZoomDuration));
                    zoomDuration = fallbackZoomDuration;
                    progress = 0f;
                    continue;
                }

                float interpolant = cameraDefinition.zooming.animationCurve.Evaluate(progress);
                cam.orthographicSize = Mathf.Lerp(startOrthoSize, finalOrthoSize, interpolant);
                transform.position = Vector3.Lerp(startPosition, destination, interpolant);
                ZoomAmount = Mathf.InverseLerp(startZoomAmount, finalZoomAmount, cam.orthographicSize);

                yield return null;
            }

            zoomingAnimation = null;
        }

        void StopAnimations()
        {
            StopAnimation(ref scrollingAnimation);
            StopAnimation(ref zoomingAnimation);
        }

        void StopAnimation(ref IEnumerator coroutine)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        static Area2 FallbackScreeningArea()
        {
            const float fallbackScreeningAreaSize = 1f;
            return new Area2(new Vector2(fallbackScreeningAreaSize, fallbackScreeningAreaSize));
        }

        /// <summary>
        /// Returns an orthogonal size that the associated Camera component would need to have in order to fit into a specified screeningArea.
        /// If specified, takes the desiredZoomedSize into consideration. </summary>
        static float CalculateCameraOrthoSize(Area2 screeningArea, float cameraAspectRatio, float desiredZoomedSize = 0)
        {
            Vector2 areaBounds = screeningArea.Size;

            const float invalidAspect = 0f;
            if (Mathf.Approximately(cameraAspectRatio, invalidAspect))
            {
                const float fallbackCameraAspect = 1f;
                Debug.LogWarning(string.Format("Invalid camera aspect ratio. Using the fallback value {0}.", fallbackCameraAspect));
                cameraAspectRatio = fallbackCameraAspect;
            }

            float? orthoSize = null;
            if (desiredZoomedSize > 0f)
            {
                Vector2 cameraSize;
                cameraSize.y = (cameraAspectRatio > 1f ? desiredZoomedSize : desiredZoomedSize / cameraAspectRatio);
                cameraSize.x = cameraSize.y * cameraAspectRatio;

                if (areaBounds.x >= cameraSize.x && areaBounds.y >= cameraSize.y)
                    orthoSize = Constants.Arithmetic.Half * cameraSize.y;
            }

            if(!orthoSize.HasValue)
                orthoSize = Constants.Arithmetic.Half * ((areaBounds.x / areaBounds.y > cameraAspectRatio) ? areaBounds.y : areaBounds.x / cameraAspectRatio);

            if (orthoSize.Value.IsFinite())
                return orthoSize.Value;

            const float fallbackOrthoSize = 50f;
            Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "resulting ortho size", orthoSize.Value, fallbackOrthoSize));
            return fallbackOrthoSize;
        }

        /// <summary>
        /// Calculates a described camera position nearest to a specified targetPosition, while respecting a specified screeningArea bounds. </summary>
        static Vector2 CalculateCameraPosition(Area2 screeningArea, Vector2 targetPosition, float cameraOrthographicSize, float cameraAspectRatio)
        {
            Vector2 cameraHalfSize;
            cameraHalfSize.y = cameraOrthographicSize;
            cameraHalfSize.x = cameraHalfSize.y * cameraAspectRatio;

            Vector2 cameraOffsetLimit = screeningArea.Size * Constants.Arithmetic.Half - cameraHalfSize;

            Vector2 resultPosition = targetPosition - screeningArea.Center;
            resultPosition.x = Mathf.Clamp(resultPosition.x, -cameraOffsetLimit.x, cameraOffsetLimit.x);
            resultPosition.y = Mathf.Clamp(resultPosition.y, -cameraOffsetLimit.y, cameraOffsetLimit.y);

            return resultPosition + screeningArea.Center;
        }
    }
}