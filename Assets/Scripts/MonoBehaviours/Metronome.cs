﻿/*
-----------------------------------------------------------------------------
This source file is part of Plant Party
    (Mobile music game)
For the latest info, see http://www.plantparty.cz/

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

#if DEVELOPMENT_BUILD || UNITY_EDITOR
using UnityExtras;
using UnityExtras.Services;
#endif

namespace PlantParty
{
    /// <summary>
    /// Utility which drives an assigned BeatPlayer accordingly to a specified SoundManager provider.
    /// Can be configured to remain after a scene change.
    /// Supports MonoBehaviour enable / disable.
    /// Intended for development purposes. </summary>
    public class Metronome : MonoBehaviour
    {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
        [SerializeField, Tooltip("SoundManager provider used for synchronization of an assigned BeatPlayer with music.")]
        Object soundManagerProvider;

        [SerializeField, Tooltip("Specifies a BeatPlayer component which shall be synchronized with a SoundManager's current music track.")]
        BeatPlayer beatPlayer;

        [SerializeField, Tooltip("Sets whether the Metronome's GameObject shall be marked as DontDestroyOnLoad.")]
        bool keepBetweenScenes;

        IServiceProvider<SoundManager> soundManager;

        void Awake()
        {
            if (keepBetweenScenes)
                DontDestroyOnLoad(gameObject);

            soundManager = soundManagerProvider.AsServiceProvider<SoundManager>();

            if (!beatPlayer)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(BeatPlayer)));
                beatPlayer = GetComponent<BeatPlayer>() ?? gameObject.AddComponent<BeatPlayer>();
            }
        }

        void OnEnable()
        {
            soundManager.Instance.PlayingTrackChanged += OnPlayingTrackChanged;

            if (soundManager.Instance.PlayingTrack != null)
                OnPlayingTrackChanged(soundManager.Instance.PlayingTrack);
        }

        void OnDisable()
        {
            if(soundManager.InstanceExists)
                soundManager.Instance.PlayingTrackChanged -= OnPlayingTrackChanged;
        }        

        void OnPlayingTrackChanged(PlayingTrack playingTrack)
        {
            beatPlayer.BPM = playingTrack.BPM;
            beatPlayer.FirstBeatTime = playingTrack.StartTime + playingTrack.BPMOffset;
        }
#endif
    }
}
