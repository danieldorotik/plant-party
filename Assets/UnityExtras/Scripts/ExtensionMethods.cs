﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityExtras.ExtensionMethods
{
    /// <summary>
    /// Extension methods for a Behaviour List type. </summary>
    public static class BehaviourList
    {
        /// <summary>
        /// Places found occurrence of T type from the supplied 'transform' into the list (if any). </summary>        
        public static void AddFrom<T>(this List<Behaviour> list, Transform transform) where T : Behaviour
        {
            Behaviour behaviour;
            if ((behaviour = transform.GetComponent<T>()) != null)
                list.Add(behaviour);
            else
                Debug.LogWarning(string.Format("Cannot find a {0} behaviour", typeof(T).FullName));
        }

        /// <summary>
        /// Places found occurrences of T type from the supplied 'transform' or from any of its active children into the list (if any). </summary>
        public static void AddAllFrom<T>(this List<Behaviour> list, Transform transform) where T : Behaviour
        {
            T[] components = transform.GetComponentsInChildren<T>();
            if (components == null || components.Length == 0)
            {
                Debug.LogWarning(string.Format("Cannot find any {0} behaviour", typeof(T).FullName));
                return;
            }

            foreach (var component in components)
            {
                list.Add(component);
            }
        }        
    }

    /// <summary>
    /// Extension methods for Collider2D. </summary>
    public static class Collider_2D
    {
        /// <summary>
        /// Returns center of the Collider2D in world space coordinates. </summary>
        public static Vector2 WorldCenter(this Collider2D collider)
        {
            // Affected by collider.transform scale (by the complete transform hierarchy)
            return collider.transform.TransformPoint(collider.offset);
        }
    }

    /// <summary>
    /// Extension methods for CircleCollider_2D. </summary>
    public static class CircleCollider_2D
    {
        /// <summary>
        /// Returns radius of the CircleCollider2D in world space coordinates. </summary>
        public static float WorldRadius(this CircleCollider2D collider)
        {
            // Affected by collider.transform scale (by the complete transform hierarchy)
            return collider.transform.TransformVector(collider.radius, 0f, 0f).magnitude;
        }
    }

    /// <summary>
    /// Extension methods for SpriteRenderer. </summary>
    public static class SpriteRendererType
    {
        /// <summary>
        /// Returns sprite's world area as an Area2 type. </summary>
        public static Area2 Area(this SpriteRenderer spriteRenderer)
        {
            Vector3 rendererPosition = spriteRenderer.transform.position;
            return new Area2(rendererPosition - spriteRenderer.bounds.extents, rendererPosition + spriteRenderer.bounds.extents);
        }
    }

    /// <summary>
    /// Extension methods for Transform. </summary>
    public static class TransformComponent
    {
        /// <summary>
        /// Copies position, rotation and scale from the 'source' Transform. </summary>
        public static void Copy(this Transform transform, Transform source)
        {
            transform.position = source.position;
            transform.rotation = source.rotation;
            transform.localScale = source.localScale;
        }

        /// <summary>
        /// Logs warning if 'checkedTransform' is neither a direct or indirect child, nor the same transform. </summary>
        public static void CheckChild(this Transform transform, Transform checkedTransform)
        {
            if(!checkedTransform)
            {
                Debug.LogError("Checked transform invalid");
                return;
            }

            if (!checkedTransform.IsChildOf(transform))
                Debug.LogWarning(string.Format("{0} is not a child of {1}", checkedTransform.gameObject, transform.gameObject));
        }

        /// <summary>
        /// Logs warning if 'component' is not a direct or indirect child component. </summary>
        public static void CheckChildComponent(this Transform transform, Component component)
        {
            if (!component || !component.transform.IsChildOf(transform))
                Debug.LogWarning(string.Format("{0} is not a child component of {1}", component, transform.gameObject));
        }
    }

    /// <summary>
    /// Extension methods for Vector2. </summary>
    public static class Vector2Type
    {
        /// <summary>
        /// Returns the smaller vector component. </summary>
        public static float Min(this Vector2 vector)
        {
            return Mathf.Min(vector.x, vector.y);
        }

        /// <summary>
        /// Returns the greater vector component. </summary>
        public static float Max(this Vector2 vector)
        {
            return Mathf.Max(vector.x, vector.y);
        }
    }

    /// <summary>
    /// Extension methods for UnityEngine.Color. </summary>
    public static class UnityColor
    {
        /// <summary>
        /// Returns a version of the color with the specified alpha. </summary>
        public static Color WithAlpha(this Color color, float alpha)
        {
            Color alphaColor = color;
            alphaColor.a = alpha;
            return alphaColor;
        }
    }

    /// <summary>
    /// Extension methods for IEnumerable of type GameObject. </summary>
    public static class GameObjectList
    {
        /// <summary>
        /// Returns average transform.position of elements in the IEnumerable collection.
        /// Note: Consider nature of your data, as this is fast but prone to an overflow. </summary>
        public static Vector3 AveragePosition(this IEnumerable<GameObject> source)
        {
            Vector3 sum = Vector3.zero;
            int count = 0;

            foreach (var item in source)
            {
                sum += item.transform.position;
                count++;
            }

            return (count > 0 ? sum / count : sum);
        }
    }

    /// <summary>
    /// General extension methods for a generic type. </summary>
    public static class GenericType
    {
        /// <summary>
        /// Without boxing, check if the type is equal to the default value of its type. </summary>
        public static bool IsDefault<T>(this T value)
        {
            return EqualityComparer<T>.Default.Equals(value, default(T));
        }
    }

    /// <summary>
    /// General extension methods for a value type. </summary>
    public static class ValueType
    {
        /// <summary>
        /// Returns true if the value occurs in the specified set of values. </summary>
        public static bool In<T>(this T value, params T[] valueList) where T : struct
        {
            return valueList.Contains(value);
        }
    }

    /// <summary>
    /// Extension methods for a float value type. </summary>
    public static class FloatType
    {
        /// <summary>
        /// Returns true in case of a finite number (no infinity or NaN). </summary>
        public static bool IsFinite(this float value)
        {
            return (!float.IsNaN(value) && !float.IsInfinity(value));
        }
    }

    /// <summary>
    /// Extension methods for a double value type. </summary>
    public static class DoubleType
    {
        /// <summary>
        /// Returns true in case of a finite number (no infinity or NaN). </summary>
        public static bool IsFinite(this double value)
        {
            return (!double.IsNaN(value) && !double.IsInfinity(value));
        }

        /// <summary>
        /// Returns true if proximity of a specified value is within a specified tolerance. </summary>
        public static bool Approximately(this double value, double compared, double tolerance = 1E-06D)
        {
            return (Math.Abs(compared - value) <= tolerance);
        }
    }

    /// <summary>
    /// Extension methods for a generic IList. </summary>
    public static class ListInterface
    {
        /// <summary>
        /// Shuffles the list using the Fisher–Yates shuffle algorithm (has O(n) complexity). </summary>
        public static void Shuffle<T>(this IList<T> list)
        {
            for (int i = 0; i < list.Count - 1; ++i)
            {
                Swap(list, i, UnityEngine.Random.Range(i, list.Count));
            }
        }

        /// <summary>
        /// Swaps list elements specified by a first and second index. Does not check indices to be in range. </summary>
        public static void Swap<T>(this IList<T> list, int firstIndex, int secondIndex)
        {
            T element = list[firstIndex];
            list[firstIndex] = list[secondIndex];
            list[secondIndex] = element;
        }
    }

    /// <summary>
    /// Extension methods for a generic IDictionary. </summary>
    public static class DictionaryInterface
    {
        /// <summary>
        /// Sets value to the specified key if the key exists. Returns true if value was set. </summary>
        public static bool TrySetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            bool contains = dictionary.ContainsKey(key);
            if (contains)
                dictionary[key] = value;

            return contains;
        }
    }
}