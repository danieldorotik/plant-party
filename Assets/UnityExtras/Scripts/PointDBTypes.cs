﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras.PointDB
{
    /// <summary>
    /// Generic type record which represents a point in 2D space. </summary>
    public class PointRecord<T>
    {
        /// <summary>
        /// Position of the record in 2D space. </summary>
        public Vector2 Position { get; private set; }

        /// <summary>
        /// Value of the record. </summary>
        public T Value { get; set; }

        /// <summary>
        /// Creates a generic type record which has a position in 2D space. </summary>
        public PointRecord(Vector2 position, T value)
        {
            Position = position;
            Value = value;
        }
    }

    /// <summary>
    /// Discrete 2D space position.
    /// Note: Used only internally by the PointDB. </summary>
    struct Address
    {
        /// <summary>
        /// Horizontal discrete position. </summary>
        public int X
        {
            get { return x; }
        }

        /// <summary>
        /// Vertical discrete position. </summary>
        public int Y
        {
            get { return y; }
        }

        readonly int x, y;

        /// <summary>
        /// Creates a discrete 2D space address from the supplied coordinates. </summary>
        public Address(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Creates a discrete 2D space address by quantizing the supplied Vector2 'position' to a grid of cell size 'partitionSize' and origin at [0,0]. </summary>
        public Address(Vector2 position, float partitionSize)
        {
            x = Quantize(position.x, partitionSize);
            y = Quantize(position.y, partitionSize);
        }

        /// <summary>
        /// Quantizes the continuous 'value' into a discrete one where 1 unit = partitionSize. </summary>
        public static int Quantize(float value, float partitionSize)
        {
            float quantized = value / partitionSize;
            if(!quantized.IsFinite())
                Debug.LogWarning(string.Format("Invalid quantization of the value {0} into the partition size {1}.", value, partitionSize));

            return (int)Mathf.Floor(quantized);
        }
    }

    /// <summary>
    /// A range in 1D discrete space. 
    /// Note: Used only internally by the PointDB. </summary>
    struct Range
    {
        /// <summary>
        /// Beginning of the range (inclusive). </summary>
        public int Start
        {
            get { return start; }
        }

        /// <summary>
        /// End of the range (inclusive). </summary>
        public float End
        {
            get { return end; }
        }

        readonly int start, end;

        /// <summary>
        /// Creates a range in 1D discrete space from the supplied arbitrarily ordered values. </summary>
        public Range(int valueA, int valueB)
        {
            if (valueA > valueB)
            {
                int swap = valueA;
                valueA = valueB;
                valueB = swap;
            }

            start = valueA;
            end = valueB;
        }

        /// <summary>
        /// Returns true if 'value' is within the Range's Start [inclusive] and End [inclusive]. </summary>
        public bool Contains(int value)
        {
            return (value >= Start && value <= End);
        }
    }

#if UNITY_EDITOR    
    /// <summary>
    /// Provides information about Database's partition contained in a debug query.
    /// Note: Used only for development debug purposes. </summary>
    public struct PartitionInfo
    {
        public int x, y;
        public bool fullyContained;
    }
#endif
}