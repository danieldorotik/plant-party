﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// General utility methods of UnityExtras. </summary>    
    public static class Auxiliaries
    {
        /// <summary>
        /// Queries results into an array (up to its Length) and returns the number of records stored in the array. </summary>
        public delegate int QueryDelegate<in T>(T[] resultBuffer);

        /// <summary>        
        /// Helper method for calling NonAlloc style methods while expanding a specified results array if necessary.
        /// A specified delegate is called as many times as it is needed to expand a specified results array
        /// due to its insufficient size to accommodate all the query results. </summary>
        public static int QueryAutoExpand<T>(QueryDelegate<T> queryDelegate, ref T[] resultBuffer)
        {
            if (resultBuffer == null || resultBuffer.Length == 0)
            {
                const int bufferFallbackSize = 10;
                Debug.LogWarning(string.Format("ResultBuffer is uninitialized or has a zero length. Initializing it with size {0}...", bufferFallbackSize));
                resultBuffer = new T[bufferFallbackSize];
            }

            int resultCount;
            while ((resultCount = queryDelegate(resultBuffer)) == resultBuffer.Length)
            {
                const int bufferExpandRatio = 2;
                resultBuffer = new T[resultBuffer.Length * bufferExpandRatio];
            }

            return resultCount;
        }

        /// <summary>
        /// Calculates a normalized direction vector from a specified degree angle. </summary>
        public static Vector2 DirectionNormalized(float degreeAngle)
        {
            return new Vector2(Mathf.Cos(degreeAngle * Mathf.Deg2Rad), Mathf.Sin(degreeAngle * Mathf.Deg2Rad));
        }
    }
}