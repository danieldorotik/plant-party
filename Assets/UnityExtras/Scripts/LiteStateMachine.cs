﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityExtras
{
    /// <summary>
    /// Events supported by a LiteStateMachine state.
    /// Note: A LiteStateMachine state is not required to define any of the supported events. </summary>
    public struct LiteStateEvents
    {
        public Action initialize, update, lateUpdate, deinitialize;
    }

    /// <summary>
    /// A lightweight state machine where state is identified by a generic type, most notably a custom enum.
    /// Does not deal with state transitions. These can be implicitly implemented in events of custom states. 
    /// Note: Can be initialized using a collection initializer. </summary>
    public class LiteStateMachine<T> : IEnumerable<LiteStateEvents> where T : struct
    {
        /// <summary>
        /// Retrieves / sets the currently active state. 
        /// Retrieves the invalid state specified during the LiteStateMachine initialization in case the current state wasn't yet set.
        /// Attempt to set an unsupported state would raise the KeyNotFoundException exception.
        /// Note: Successful state change calls a Deinitialize action on previous state (if any) and Initialize action on the new state. </summary>
        public T CurrentState
        {
            get { return currentState; }
            set { ChangeState(value); }
        }

        readonly Dictionary<T, LiteStateEvents> states;        
        T currentState;
        LiteStateEvents? currentStateEvents;

        /// <summary>
        /// Registers the specified state in the LiteStateMachine.
        /// Attempt to register an already registered state would raise the ArgumentException exception. </summary>
        public void Add(T stateType, LiteStateEvents stateEvents)
        {
            states.Add(stateType, stateEvents);
        }

        /// <summary>
        /// Calls the CurrentState Update action (if the state is set and defines this action). </summary>
        public void Update()
        {
            if (currentStateEvents != null && currentStateEvents.Value.update != null)
                currentStateEvents.Value.update();
        }

        /// <summary>
        /// Calls the CurrentState LateUpdate action (if the state is set and defines this action). </summary>
        public void LateUpdate()
        {
            if (currentStateEvents != null && currentStateEvents.Value.lateUpdate != null)
                currentStateEvents.Value.lateUpdate();
        }

        /// <summary>
        /// Initializes a LiteStateMachine instance with a specified invalid state identifier to be set as a CurrentState.
        /// Optionally it is possible to pass an equality comparer for the LiteStateMachine generic type, for example to prevent boxing. </summary>        
        public LiteStateMachine(T invalidState, IEqualityComparer<T> stateComparer = null)
        {
            states = new Dictionary<T, LiteStateEvents>(stateComparer);
            currentState = invalidState;
        }

        /// <summary>
        /// Returns an enumerator for iterating through the LiteStateEvents values of the registered states. </summary>        
        public IEnumerator<LiteStateEvents> GetEnumerator()
        {
            return states.Values.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator for iterating through the LiteStateEvents values of the registered states. </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        void ChangeState(T newState)
        {
            if (currentStateEvents != null && currentStateEvents.Value.deinitialize != null)
                currentStateEvents.Value.deinitialize();

            currentState = newState;
            currentStateEvents = states[currentState];

            if (currentStateEvents.Value.initialize != null)
                currentStateEvents.Value.initialize();
        }
    }
}