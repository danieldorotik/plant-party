﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;

namespace UnityExtras
{
    /// <summary>
    /// A custom attribute for the Span type, supported by the SpanDrawer (a custom PropertyDrawer). </summary>
    public class SpanRangeAttribute : Attribute
    {
        /// <summary>
        /// The Min value used by the SpanDrawer to represent the Span type in the EditorGUI.MinMaxSlider. </summary>
        public float Min
        {
            get { return min; }
        }

        /// <summary>
        /// The Max value used by the SpanDrawer to represent the Span type in the EditorGUI.MinMaxSlider. </summary>
        public float Max
        {
            get { return max; }
        }
        
        readonly float min, max;

        /// <summary>
        /// Specify values for a convenient representation of the Span as a MinMaxSlider through the SpanDrawer. </summary>
        public SpanRangeAttribute(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }
}