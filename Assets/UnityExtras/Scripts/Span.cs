﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// Describes a continuous interval specified by two arbitrary float values.
    /// Note: There is a custom drawer for this struct called SpanDrawer, based on EditorGUI.MinMaxSlider.
    ///       The SpanDrawer also supports a custom attribute for this struct called SpanRange. </summary>
    [System.Serializable]
    public struct Span
    {
        /// <summary>
        /// Returns the smallest value defining the span. </summary>
        public float Min
        {
            get { return Mathf.Min(ValueA, ValueB); }
        }

        /// <summary>
        /// Returns the largest value defining the span. </summary>
        public float Max
        {
            get { return Mathf.Max(ValueA, ValueB); }
        }

        /// <summary>
        /// Returns the span length (largest minus smallest value defining the span). </summary>
        public float Length
        {
            get { return Max - Min; }
        }

        /// <summary>
        /// Returns the span-defining value A. </summary>
        public float ValueA
        {
            get { return valueA; }
            set { valueA = value; }
        }

        /// <summary>
        /// Returns the span-defining value B. </summary>
        public float ValueB
        {
            get { return valueB; }
            set { valueB = value; }
        }

        [SerializeField]
        private float valueA, valueB;

        /// <summary>
        /// Creates the span out of two provided arbitrary values. </summary>
        public Span(float valueA, float valueB)
        {
            this.valueA = valueA;
            this.valueB = valueB;
        }

        /// <summary>
        /// Returns a random value from the span interval (Min[Inclusive] to Max[Inclusive]). </summary>        
        public float RandomContained()
        {
            return Random.Range(Min, Max);
        }

        /// <summary>
        /// Shifts the span by the supplied shiftValue.
        /// Note: This modifies the span. For a static version see the Shifted method. </summary>        
        public void Shift(float shiftValue)
        {
            ValueA += shiftValue;
            ValueB += shiftValue;
        }

        /// <summary>
        /// Returns true if the supplied 'value' is within the span interval (Min[Inclusive] to Max[Inclusive]). </summary>
        public bool Contains(float value)
        {
            return (value >= Min && value <= Max);
        }

        /// <summary>
        /// Returns a Span which represents an intersection of the provided spans.
        /// Returns null if there is no intersection. </summary>
        public static Span? Intersection(Span span, Span otherSpan)
        {
            float largestMinimum = Mathf.Max(span.Min, otherSpan.Min);
            float smallestMaximum = Mathf.Min(span.Max, otherSpan.Max);

            if (largestMinimum <= smallestMaximum)
                return new Span(largestMinimum, smallestMaximum);
            
            return null;
        }
        
        /// <summary>
        /// Returns a Span which represents a continuous union of the provided spans (made from the smallest Min and the largest Max). </summary>
        public static Span Union(Span span, Span otherSpan)
        {
            return new Span(Mathf.Min(span.Min, otherSpan.Min), Mathf.Max(span.Max, otherSpan.Max));
        }

        /// <summary>
        /// Returns the longer Span out of the two optionally provided spans.
        /// Returns null if there are no provided spans. </summary>
        public static Span? GetLonger(Span? firstSpan, Span? secondSpan)
        {
            if (firstSpan != null && secondSpan != null)
                return (firstSpan.Value.Length > secondSpan.Value.Length) ? firstSpan.Value : secondSpan.Value;

            return firstSpan ?? secondSpan;
        }

        /// <summary>
        /// Returns a version of the provided span shifted by a shiftValue. </summary>
        public static Span Shifted(Span span, float shiftValue)
        {
            return new Span(span.ValueA + shiftValue, span.ValueB + shiftValue);
        }
    }
}