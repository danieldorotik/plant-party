﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
namespace UnityExtras
{
    public static partial class Constants
    { 
        /// <summary>
        /// Defines common messages used for debug logging. </summary>
        public static class DebugMessages
        {
            public const string ComponentNotFound = "Component {0} not found";
            public const string ComponentNotFoundDefault = ComponentNotFound + ". Creating a default one...";
            public const string ComponentNotLinked = "Component {0} not linked";
            public const string ComponentNotLinkedDefault = ComponentNotLinked + ". Using existing or adding a default one...";
            public const string ObjectNotLinked = "{0} object not linked";
            public const string ObjectNotLinkedDefault = ObjectNotLinked + ". Creating a default one...";
            public const string ObjectNotLinkedInvalid = ObjectNotLinked + " or is invalid";
            public const string ParameterNull = "The {0} parameter is null";
            public const string ParameterNullDefault = ParameterNull + ". Creating default instance...";
            public const string InvalidValueUsingFallback = "Invalid {0} '{1}'. Using the fallback value '{2}'.";
        }
    }
}