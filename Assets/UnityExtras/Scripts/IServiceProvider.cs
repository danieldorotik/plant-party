﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras.Services
{
    /// <summary>
    /// Represents a provider which maintains and returns a UnityEngine.Object descendant instance (i.e. a service). </summary>
    public interface IServiceProvider<out T> : IInitializable where T : Object
    {
        /// <summary>
        /// Returns the internally maintained instance of type T. </summary>
        T Instance { get; }

        /// <summary>
        /// Returns true if the internally maintained instance of type T exists. </summary>
        bool InstanceExists { get; }

        /// <summary>
        /// Returns true if this provider is valid (i.e. properly configured). </summary>
        bool ProviderValid { get; }
    }
}
