﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Collections.Generic;

namespace UnityExtras
{
    /// <summary>
    /// An optimized queue with automatic dequeuing on exceeding its size and with a lookup complexity of O(1). </summary>
    public class GenericHistory<T>
    {
        /// <summary>
        /// Gets the number of elements located in the GenericHistory. </summary>
        public int Count
        {
            get { return history.Count; }
        }

        /// <summary>
        /// Maximum history capacity. Adding a new item will automatically remove the oldest item if the capacity would have been exceeded.
        /// Decreasing the depth will result in removal of the oldest items in order to meet the new history capacity. </summary>
        public int Depth
        {
            get { return depth; }
            set
            {
                depth = (value < 0) ? 0 : value;

                while (history.Count > depth)
                {
                    RemoveHistoryFirst();
                }
            }
        }

        readonly LinkedList<T> history;
        readonly HashSet<T> searchHistory;
        int depth;

        /// <summary>
        /// Creates a GenericHistory instance and optionally specifies its depth. </summary>
        public GenericHistory(int depth = 0)
        {
            history = new LinkedList<T>();
            searchHistory = new HashSet<T>();
            Depth = depth;
        }

        /// <summary>
        /// Adds a specified element to the GenericHistory.
        /// This can result in removing the exceeding historical element if necessary.
        /// This is an O(1) operation. </summary>        
        public void Add(T historyItem)
        {
            history.AddLast(historyItem);
            searchHistory.Add(historyItem);

            if (history.Count > depth)
                RemoveHistoryFirst();
        }

        /// <summary>
        /// Removes the first occurrence of a specified element from the GenericHistory.
        /// This is an O(n) operation. </summary>
        public void Remove(T historyItem)
        {
            history.Remove(historyItem);
            searchHistory.Remove(historyItem);
        }

        /// <summary>
        /// Returns true if a specified element is contained within the GenericHistory.
        /// This is an O(n) operation. </summary>
        public bool Contains(T historyItem)
        {
            return searchHistory.Contains(historyItem);
        }

        /// <summary>
        /// Gives descendants efficient access to the underlying history (shall be used only for reading values). </summary>
        protected LinkedList<T> History()
        {
            return history;
        }

        void RemoveHistoryFirst()
        {
            if (history.Count <= 0)
                return;

            searchHistory.Remove(history.First.Value);
            history.RemoveFirst();
        }
    }
}