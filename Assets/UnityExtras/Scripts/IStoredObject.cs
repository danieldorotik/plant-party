﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
namespace UnityExtras
{
    /// <summary>
    /// Provides an instance of a specified generic type and methods to maintain its storage. </summary>
    public interface IStoredObject<out T> where T : class
    {
        /// <summary>
        /// Returns the internally maintained instance. </summary>
        T Data { get; }

        /// <summary>
        /// Stores the internally maintained instance (if any). </summary>
        void Save();

        /// <summary>
        /// Restores the internally maintained instance, making it available through the Data property. </summary>
        void Load();

        /// <summary>
        /// Clears stored data regarding the internally maintained instance (if any) and restores (loads) it from its defaults. </summary>
        void ResetToDefault();
    }
}
