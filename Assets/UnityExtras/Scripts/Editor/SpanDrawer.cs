﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

using System;
using UnityEngine;
using UnityEditor;

namespace UnityExtras
{
    /// <summary>
    /// Represents the Span type as an editable MinMaxSlider in the Unity editor. </summary>
    [CustomPropertyDrawer(typeof(Span))]
    public class SpanDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            GUIStyle labelStyle = new GUIStyle(GUI.skin.label);

            SerializedProperty serializedValueA = property.FindPropertyRelative("valueA");
            SerializedProperty serializedValueB = property.FindPropertyRelative("valueB");

            var spanRanges = (SpanRangeAttribute[])fieldInfo.GetCustomAttributes(typeof(SpanRangeAttribute), false);
            Span? limitSpan = (spanRanges.Length > 0) ? new Span(spanRanges[0].Min, spanRanges[0].Max) : (Span?)null;            
            if(limitSpan != null)
            {
                if (!limitSpan.Value.Contains(serializedValueA.floatValue) || !limitSpan.Value.Contains(serializedValueB.floatValue))
                {
                    label.tooltip = string.Format("Span value(s) violate SpanRange({0}f, {1}f)", limitSpan.Value.Min, limitSpan.Value.Max);
                    labelStyle.normal.textColor = Color.red;
                }
            }

            position = EditorGUI.PrefixLabel(position, label, labelStyle);

            const float floatFieldWidth = 50f;
            Rect fieldRect = new Rect(position) {width = floatFieldWidth};
            float valueA = EditorGUI.FloatField(fieldRect, serializedValueA.floatValue);
            if (!Mathf.Approximately(valueA, serializedValueA.floatValue))
                serializedValueA.floatValue = Clamp(valueA, limitSpan);

            fieldRect.x += position.width - floatFieldWidth;
            float valueB = EditorGUI.FloatField(fieldRect, serializedValueB.floatValue);
            if (!Mathf.Approximately(valueB, serializedValueB.floatValue))
                serializedValueB.floatValue = Clamp(valueB, limitSpan);

            Span valueSpan = new Span(Clamp(serializedValueA.floatValue, limitSpan), Clamp(serializedValueB.floatValue, limitSpan));

            const float sliderOffset = 5f;
            Rect sliderRect = new Rect(position);
            sliderRect.xMin += floatFieldWidth + sliderOffset;
            sliderRect.xMax -= floatFieldWidth + sliderOffset;
            float min = valueSpan.Min;
            float max = valueSpan.Max;
            if (DrawMinMaxSlider(sliderRect, ref min, ref max, limitSpan))
            {
                // Removes possible focus from FloatField so its value will visually change immediately
                GUI.FocusControl(null);

                serializedValueA.floatValue = Clamp(Round(min), limitSpan);
                serializedValueB.floatValue = Clamp(Round(max), limitSpan);
            }

            EditorGUI.EndProperty();
        }

        /// <summary>
        /// Rounds float to a 1 decimal place. </summary>
        static float Round(float value)
        {
            return (float)Math.Round(value, 1, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Returns the supplied value, optionally clamped by the supplied clampSpan. </summary>
        static float Clamp(float value, Span? clampSpan)
        {
            return (clampSpan != null) ? Mathf.Clamp(value, clampSpan.Value.Min, clampSpan.Value.Max) : value;
        }

        /// <summary>
        /// Shows the EditorGUI.MinMaxSlider and returns true if the referenced values were changed by it. </summary>
        static bool DrawMinMaxSlider(Rect sliderRect, ref float min, ref float max, Span? limitSpan)
        {
            float limitMin = (limitSpan != null) ? limitSpan.Value.Min : min;
            float limitMax = (limitSpan != null) ? limitSpan.Value.Max : max;

            EditorGUI.BeginChangeCheck();
            EditorGUI.MinMaxSlider(sliderRect, ref min, ref max, limitMin, limitMax);
            return EditorGUI.EndChangeCheck();
        }
    }
}