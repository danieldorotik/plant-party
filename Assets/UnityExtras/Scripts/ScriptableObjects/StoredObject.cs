﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// Provides an instance of a specified generic type and methods to maintain its storage.
    /// The maintained instance is loaded / saved using a specified data source (object that implements the IDataSource interface).
    /// In case the instance cannot be loaded, it is cloned from a specified 'originalObject' instance.
    /// Note:
    ///   - The StoredObject class is designed to be fail-safe, meaning it handles its failures and falls back to the 'originalObject' instance if necessary. 
    ///     Descendants shall adhere to this approach.
    ///   - StoredObject throws only if it has to load the 'originalObject' and the operation fails. Otherwise it logs the issue
    ///     and invokes the ErrorOccurred event. </summary>
    public class StoredObject<T> : ScriptableObject, IStoredObject<T> where T : UnityEngine.Object
    {
        [SerializeField, Tooltip("The original object from which the internal instance is cloned.")]
        T originalObject;

        [SerializeField, Tooltip("The Data source from which the internal instance is loaded / saved.")]
        UnityEngine.Object dataSource;

        /// <summary>
        /// Returns the internally maintained instance. </summary>
        public T Data { get; protected set; }

        /// <summary>
        /// The original object from which the internal instance is cloned. </summary>
        public T OriginalObject
        {
            get { return originalObject; }
            protected set { originalObject = value; }
        }

        /// <summary>
        /// The Data source from which the internal instance is loaded / saved. </summary>
        public UnityEngine.Object DataSource
        {
            get { return dataSource; }
        }

        /// <summary>
        /// Event invoked when an internally handled error occurs. </summary>
        public event Action<string> ErrorOccurred;

        protected const string DatasourceMissing = "DataSource missing";

        /// <summary>
        /// Attempts to store the internal instance using the configured data source. </summary>
        public virtual void Save()
        {
            Action save = () =>
            {
                if (Data == null)
                    throw new UnityException("Data has not yet been initialized");

                if (!dataSource)
                    throw new UnityException(DatasourceMissing);

                SaveData(Data, (IDataSource)dataSource);
            };

            SafeInvoke(save);
        }

        /// <summary>
        /// Creates an internal instance using the configured data source.
        /// In case of any loading issue the internal instance is created by cloning the configured 'originalObject'. </summary>
        public virtual void Load()
        {
            bool loaded = false;

            Action load = () =>
            {
                if (!dataSource)
                    throw new UnityException(DatasourceMissing);

                IDataSource source = (IDataSource)dataSource;

                if (!source.Exists())
                    return;

                Data = LoadData((IDataSource)dataSource);

                loaded = true;
            };

            SafeInvoke(load);

            if (loaded)
                return;

            LoadDefault();
        }

        /// <summary>
        /// Rolls back to the original state (clean data source and the internal instance cloned from the configured 'originalObject'). </summary>
        public virtual void ResetToDefault()
        {
            Action clearData = () =>
            {
                if (!dataSource)
                    throw new UnityException(DatasourceMissing);

                ((IDataSource)dataSource).Clear();
            };

            SafeInvoke(clearData);

            LoadDefault();
        }

        /// <summary>
        /// Stores provided data into a specified data source. 
        /// Optionally reimplement in a descendant for a custom data preparation. </summary>
        protected virtual void SaveData(T data, IDataSource source)
        {
            source.Save(data);
        }

        /// <summary>
        /// Returns data loaded from a specified data source. Loads the default data version if specified.
        /// Optionally reimplement in a descendant for a custom data loading. </summary>
        protected virtual T LoadData(IDataSource source, bool loadDefault = false)
        {
            return (loadDefault ? Instantiate(originalObject) : (T)source.Load(typeof(T)));
        }

        /// <summary>
        /// Executes a specified action safely, meaning potential exception is catched, logged and notified (see the ErrorOccurred event). </summary>
        protected void SafeInvoke(Action action)
        {
            try
            {
                if (action != null)
                    action();
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);

                if (ErrorOccurred != null)
                    ErrorOccurred(e.Message);
            }
        }

        void LoadDefault()
        {
            Debug.Log(string.Format("Creating a {0} instance by cloning the default object '{1}'.", typeof(T).Name, originalObject));
            Data = LoadData(null, loadDefault: true);
        }
    }
        
}
