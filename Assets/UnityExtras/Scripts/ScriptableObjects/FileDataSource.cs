﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.IO;
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// File data source.
    /// Using assigned 'serializer', stores / restores data object into file located in [Application.persistentDataPath + 'path']. </summary>
    [CreateAssetMenu(menuName = Constants.AssetMenu.DataSource + "FileDataSource")]
    public class FileDataSource : ScriptableObject, IDataSource
    {
        [SerializeField, Tooltip("File location of the File Data Source.")]
        string path;

        [SerializeField, Tooltip("Serializer employed by the File Data Source.")]
        UnityEngine.Object serializer;

        public string BasePath
        {
            get { return Application.persistentDataPath + PathSeparator; }
        }

        public string FinalPath
        {
            get { return BasePath + path; }
        }

        const string PathSeparator = "/";
        const string SerializerMissing = "Serializer missing";

        void IDataSource.Clear()
        {
            if(File.Exists(FinalPath))
                File.Delete(FinalPath);
        }

        bool IDataSource.Exists()
        {
            return File.Exists(FinalPath);
        }

        object IDataSource.Load(Type type)
        {
            if (!serializer)
                throw new UnityException(SerializerMissing);

            using (FileStream stream = new FileStream(FinalPath, FileMode.Open, FileAccess.Read))
            {
                return ((ISerializer)serializer).Deserialize(type, stream);
            }
        }

        void IDataSource.Save(object data)
        {
            if (data == null)
                throw new UnityException("Data invalid");

            if (!serializer)
                throw new UnityException(SerializerMissing);

            using (FileStream stream = new FileStream(FinalPath, FileMode.Create))
            {
                ((ISerializer)serializer).Serialize(data, stream);
            }
        }
    }
}