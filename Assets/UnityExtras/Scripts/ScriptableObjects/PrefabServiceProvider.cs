﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras.Services
{
    /// <summary>
    /// An IServiceProvider implementation for providing services made out of prefabs.
    /// A service GameObject is expected to have a component of type T. </summary>
    public class PrefabServiceProvider<T> : ScriptableObject, IServiceProvider<T> where T : Component
    {
        [SerializeField, Tooltip("The prefab service GameObject from which the maintained service instance will be cloned.")]
        GameObject servicePrefab;

        [SerializeField, Tooltip("Makes the maintained service instance permanent upon its creation (using the PermanentService functionality).")]
        bool permanent = true;

        /// <summary>
        /// Returns the service instance (lazy initialization). </summary>
        public T Instance
        {
            get { return GetInstance(); }
        }

        /// <summary>
        /// Returns true if the maintained service instance exists. </summary>
        public bool InstanceExists
        {
            get { return (instance != null); }
        }

        /// <summary>
        /// Returns true if the provider is properly configured. </summary>
        public bool ProviderValid
        {
            get { return (servicePrefab && servicePrefab.GetComponentInChildren<T>()); }
        }

        T instance;

        /// <summary>
        /// Initializes the provider by creation of its maintained service instance. </summary>
        public void Initialize()
        {
            GetInstance();
        }

        T GetInstance()
        {
            if (instance != null)
                return instance;            

            if (servicePrefab == null)
            {
                Debug.LogError("Missing servicePrefab");
                return null;
            }

            Transform parentTransform = permanent ? PermanentService.ParentObject : null;
            GameObject instanceObject = Instantiate(servicePrefab, parentTransform);
            instance = instanceObject.GetComponentInChildren<T>();

            if (instance == null)
            {                
                Destroy(instanceObject);                
                Debug.LogError(string.Format("Cannot find {0} component on servicePrefab. Instance destroyed.", typeof(T)));
                return null;
            }

            if(permanent)
                PermanentService.Register(this);

            return instance;
        }
    }
}
