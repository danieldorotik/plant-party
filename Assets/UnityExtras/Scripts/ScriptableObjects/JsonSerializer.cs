﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.IO;
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// Employs Unity's JsonUtility to serialize / deserialize classes derived from UnityEngine.Object or [System.Serializable] classes / structs. </summary>
    [CreateAssetMenu(menuName = Constants.AssetMenu.Serializer + "JsonSerializer")]    
    public class JsonSerializer : ScriptableObject, ISerializer
    {
        /// <summary>
        /// Serializes passed 'obj' object into 'output' stream as a json format. </summary>
        void ISerializer.Serialize(object obj, Stream output)
        {
            using (StreamWriter writer = new StreamWriter(output))
            {
                writer.Write(JsonUtility.ToJson(obj));
            }
        }

        /// <summary>
        /// Deserializes object of given 'type' from the json 'input' stream and returns it.
        /// Note that a default(type) instance is created first (without using field initializers and calling constructor), 
        /// then its fields are overwritten as much as available from the input.
        /// In case of UnityEngine.Object, the field initializers are used and constructor is called. </summary>
        object ISerializer.Deserialize(Type type, Stream input)
        {
            using (StreamReader reader = new StreamReader(input))
            {
                string data = reader.ReadToEnd();
                if (string.IsNullOrEmpty(data))
                    throw new UnityException("Deserialization stream input data invalid");

                if (!type.IsSubclassOf(typeof(UnityEngine.Object)))
                    return JsonUtility.FromJson(data, type);

                UnityEngine.Object instance = CreateInstance(type);
                JsonUtility.FromJsonOverwrite(data, instance);
                return instance;
            }
        }
    }
}
