﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras.PointDB
{
    /// <summary>
    /// Spatial database for efficient storage / lookup of generic type records, each representing a point in 2D space.
    /// To achieve efficient record storage / lookup, database internally divides continuous space division into discrete intervals.
    /// Fine-tuning of database performance can be achieved by setting the discrete spatial interval size during the database's creation. </summary>
    public class PointDB<T>
    {
        /// <summary>
        /// Discrete interval by which the database divides continuous space to achieve efficient record storage / lookup. </summary>
        public float PartitionSize
        {
            get { return partitionSize; }
        }

        PointDBTable<T> dbTable;
        readonly float partitionSize;
        protected const float DefaultPartitionSize = 1f;

        delegate void VerticalExtremesDelegate(Span horizontalBounds, out Span? verticalMinima, out Span? verticalMaxima);        
        delegate void PartitionDelegate(Address address, bool fullyContained);
        delegate void ScanDelegate(PartitionDelegate partitionDelegate);

        /// <summary>
        /// Creates the spatial database.
        /// To achieve efficient record storage / lookup, the continuous 2D space is divided into squares of size 'partitionSize'.
        /// Set this value to best fit your use case. </summary>
        public PointDB(float partitionSize = DefaultPartitionSize)
        {
            this.partitionSize = partitionSize;
            InitDBTable();
        }

        /// <summary>
        /// Adds a generic value T located in 2D space as a PointRecord into the database.
        /// Returns the created PointRecord (reference type). </summary>
        public PointRecord<T> Add(Vector2 position, T value)
        {
            PointRecord<T> record = new PointRecord<T>(position, value);
            dbTable.AddRecord(CreateAddress(position), record);
            return record;
        }

        /// <summary>
        /// Removes specified PointRecord record from the database. </summary>
        public bool Remove(PointRecord<T> record)
        {
            return dbTable.RemoveRecord(CreateAddress(record.Position), record);
        }

        /// <summary>
        /// Removes all records from the database. More efficient than calling Remove individually for each record. </summary>
        public void RemoveAll()
        {
            InitDBTable();
        }

        /// <summary>
        /// Retrieves all database records which overlap the 'queryArea' into the pre-allocated 'results' array.
        /// Returns the number of records stored in the 'results' array (only as many as the pre-allocated array size). </summary>
        public int QueryOverlapAreaNonAlloc(Area2 queryArea, PointRecord<T>[] results)
        {
            if (results.Length == 0)
                return 0;

            int resultsMaxIndex = results.Length - 1;
            int resultsIndex = 0;

            PartitionDelegate partitionDelegate = (address, fullyContained) =>
            {
                if (resultsIndex > resultsMaxIndex)
                    return;

                var field = dbTable.GetField(address);
                if (field == null)
                    return;

                foreach (var record in field)
                {
                    if (!fullyContained && !queryArea.Contains(record.Position))
                        continue;

                    results[resultsIndex] = record;
                    resultsIndex++;

                    if (resultsIndex > resultsMaxIndex)
                        break;
                }
            };

            ScanArea(queryArea, partitionDelegate);

            return resultsIndex;        
        }
#if UNITY_EDITOR
        /// <summary>
        /// Development diagnostics method of a rectangular area space scanning.        
        /// Resulting array contains addresses of partitions which overlap the 'queryArea' and respective information about full / partial overlap. </summary>
        public int QueryOverlapAreaNonAllocDebug(Area2 queryArea, PartitionInfo[] results)
        {
            return QueryPartitionDebugInfo(partitionDelegate => ScanArea(queryArea, partitionDelegate), results);
        }
#endif
        /// <summary>
        /// Retrieves all database records which overlap the specified circle area into the pre-allocated 'results' array.
        /// Returns the number of records stored in the 'results' array (only as many as the pre-allocated array size). </summary>
        public int QueryOverlapCircleNonAlloc(Vector2 circlePosition, float circleRadius, PointRecord<T>[] results)
        {
            float radiusSqr = circleRadius * circleRadius;

            if (results.Length == 0)
                return 0;

            int resultsMaxIndex = results.Length - 1;
            int resultsIndex = 0;

            PartitionDelegate partitionDelegate = (address, fullyContained) =>
            {
                if (resultsIndex > resultsMaxIndex)
                    return;

                var field = dbTable.GetField(address);
                if (field == null)
                    return;

                foreach (var record in field)
                {
                    if (!fullyContained && (circlePosition - record.Position).sqrMagnitude > radiusSqr)
                        continue;

                    results[resultsIndex] = record;
                    resultsIndex++;

                    if (resultsIndex > resultsMaxIndex)
                        break;
                }
            };

            ScanCircle(circlePosition, circleRadius, partitionDelegate);

            return resultsIndex;
        }
#if UNITY_EDITOR
        /// <summary>
        /// Development diagnostics method of a circle area space scanning.        
        /// Resulting array contains addresses of partitions which overlap the specified circle area and respective information about full / partial overlap. </summary>
        public int QueryOverlapCircleNonAllocDebug(Vector2 circlePosition, float circleRadius, PartitionInfo[] results)
        {
            return QueryPartitionDebugInfo(partitionDelegate => ScanCircle(circlePosition, circleRadius, partitionDelegate), results);
        }
#endif
        /// <summary>
        /// Retrieves all database records into the pre-allocated 'results' array.
        /// Returns the number of records stored in the 'results' array (only as many as the pre-allocated array size). </summary>
        public int QueryAllNonAlloc(PointRecord<T>[] results)
        {
            if (results.Length == 0)
                return 0;

            int resultsIndex = -1;
            return dbTable.CallForEachRecord(record => results[++resultsIndex] = record, results.Length);
        }

        void InitDBTable()
        {
            dbTable = new PointDBTable<T>();            
        }

        Address CreateAddress(Vector2 position)
        {
            return new Address(position, partitionSize);
        }

        int Quantize(float value)
        {
            return Address.Quantize(value, partitionSize);
        }

        /// <summary>
        /// Calls partitionDelegate for each db partition which belong to space marked by horizontalBounds and corresponding result from getVerticalExtremes. </summary>
        void ScanSpace(Span horizontalBounds, VerticalExtremesDelegate getVerticalExtremes, PartitionDelegate partitionDelegate)
        {
            int firstScanColumn = Quantize(horizontalBounds.Min);
            int lastScanColumn = Quantize(horizontalBounds.Max);

            Span columnBounds = new Span(firstScanColumn * partitionSize, (firstScanColumn + 1) * partitionSize);

            for (int scannedColumn = firstScanColumn; scannedColumn <= lastScanColumn; scannedColumn++)
            {
                Span? verticalMinima, verticalMaxima;
                getVerticalExtremes(columnBounds, out verticalMinima, out verticalMaxima);

                ScanColumn(scannedColumn, verticalMinima, verticalMaxima, partitionDelegate);

                columnBounds.Shift(partitionSize);
            }
        }

        void ScanColumn(int columnIndex, Span? verticalMinima, Span? verticalMaxima, PartitionDelegate partitionDelegate)
        {
            if (verticalMaxima == null)
                return;

            int firstScanField = Quantize(verticalMaxima.Value.Min);
            int lastScanField = Quantize(verticalMaxima.Value.Max);

            Range? fullyContainedRange = null;
            if(verticalMinima != null)
            {
                int firstFullyContainedField = Quantize(verticalMinima.Value.Min) + 1;
                int lastFullyContainedField = Quantize(verticalMinima.Value.Max) - 1;

                if (firstFullyContainedField <= lastFullyContainedField)
                    fullyContainedRange = new Range(firstFullyContainedField, lastFullyContainedField);
            }

            for (int scannedField = firstScanField; scannedField <= lastScanField; scannedField++)
            {
                bool fullyContained = (fullyContainedRange.HasValue && fullyContainedRange.Value.Contains(scannedField));
                partitionDelegate(new Address(columnIndex, scannedField), fullyContained);
            }    
        }

        void ScanArea(Area2 area, PartitionDelegate partitionDelegate)
        {
            Span areaHorizontalBounds = new Span(area.BottomLeft.x, area.TopRight.x);
            Span areaVerticalBounds = new Span(area.BottomLeft.y, area.TopRight.y);

            VerticalExtremesDelegate getAreaVerticalExtremes = delegate (Span columnBounds, out Span? verticalMinima, out Span? verticalMaxima)
            {
                Span? columnIntersection = Span.Intersection(areaHorizontalBounds, columnBounds);
                if (columnIntersection == null)
                {
                    // columnBounds are completely off the areaHorizontalBounds
                    verticalMinima = null;
                    verticalMaxima = null;
                }
                else if(!Mathf.Approximately(columnIntersection.Value.Length, columnBounds.Length))
                {
                    // columnBounds are not completely within the areaHorizontalBounds
                    verticalMinima = null;
                    verticalMaxima = areaVerticalBounds;
                }
                else
                {
                    // columnBounds are completely within the areaHorizontalBounds
                    verticalMinima = areaVerticalBounds;
                    verticalMaxima = areaVerticalBounds;
                }
            };
                        
            ScanSpace(areaHorizontalBounds, getAreaVerticalExtremes, partitionDelegate);
        }

        void ScanCircle(Vector2 position, float radius, PartitionDelegate partitionDelegate)
        {
            Span circleVerticalBounds = new Span(position.y - radius, position.y + radius);

            VerticalExtremesDelegate getCircleVerticalExtremes = delegate (Span columnBounds, out Span? verticalMinima, out Span? verticalMaxima)
            {
                Span? columnIntersectionLeft = GetCircleIntersections(position, radius, columnBounds.Min);
                Span? columnIntersectionRight = GetCircleIntersections(position, radius, columnBounds.Max);

                // find out verticalMinima
                if (columnIntersectionLeft != null && columnIntersectionRight != null)                
                    verticalMinima = (columnIntersectionLeft.Value.Length < columnIntersectionRight.Value.Length) ? columnIntersectionLeft.Value : columnIntersectionRight.Value;                
                else
                    verticalMinima = null;

                // find out verticalMaxima
                verticalMaxima = columnBounds.Contains(position.x) ? circleVerticalBounds : Span.GetLonger(columnIntersectionLeft, columnIntersectionRight);
            };

            Span circleHorizontalBounds = new Span(position.x - radius, position.x + radius);
            ScanSpace(circleHorizontalBounds, getCircleVerticalExtremes, partitionDelegate);
        }

        static Span? GetCircleIntersections(Vector2 circlePosition, float circleRadius, float intersectionX)
        {
            float horizontalDistance = Mathf.Abs(intersectionX - circlePosition.x);
            if (horizontalDistance > circleRadius)
                return null;

            float verticalIntersection = Mathf.Sqrt((circleRadius * circleRadius) - (horizontalDistance * horizontalDistance));
            if (float.IsNaN(verticalIntersection))
                verticalIntersection = 0f;

            return new Span(circlePosition.y - verticalIntersection, circlePosition.y + verticalIntersection);
        }
#if UNITY_EDITOR
        static int QueryPartitionDebugInfo(ScanDelegate scanDelegate, PartitionInfo[] results)
        {
            if (results.Length == 0)
                return 0;

            int resultsMaxIndex = results.Length - 1;
            int resultsIndex = 0;

            PartitionInfo info;
            PartitionDelegate partitionDelegate = (address, fullyContained) =>
            {
                if (resultsIndex > resultsMaxIndex)
                    return;

                info.x = address.X;
                info.y = address.Y;
                info.fullyContained = fullyContained;

                results[resultsIndex] = info;
                resultsIndex++;
            };

            scanDelegate(partitionDelegate);

            return resultsIndex;
        }
#endif
    }
}