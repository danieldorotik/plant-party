﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    /// <summary>
    /// Area in 2D space specified by two arbitrary Vector2 points. </summary>
    public struct Area2
    {
        /// <summary>
        /// Area's bottom left corner absolute position. </summary>
        public Vector2 BottomLeft
        {
            get { return bottomLeft; }
        }

        /// <summary>
        /// Area's top right corner absolute position. </summary>
        public Vector2 TopRight
        {
            get { return topRight; }
        }

        /// <summary>
        /// Size of the area (calculated value). </summary>
        public Vector2 Size
        {
            get { return TopRight - BottomLeft; }
        }

        /// <summary>
        /// Area's center absolute position (calculated value). </summary>
        public Vector2 Center
        {
            get { return (BottomLeft + TopRight) * Constants.Arithmetic.Half; }
        }

        /// <summary>
        /// The smaller area dimension. </summary>
        public float Min
        {
            get { return Size.Min(); }
        }

        /// <summary>
        /// The greater area dimension. </summary>
        public float Max
        {
            get { return Size.Max(); }
        }

        Vector2 bottomLeft, topRight;

        /// <summary>
        /// Creates Area from the two specified arbitrary Vector2 points. </summary>        
        public Area2(Vector2 pointA, Vector2 pointB)
        {
            bottomLeft = topRight = Vector2.zero;
            SetArea(pointA, pointB);
        }

        /// <summary>
        /// Creates Area with its center at [0,0] and size accordingly to a specified Vector2 type. </summary>  
        public Area2(Vector2 areaSize)
        {
            float halfWidth = areaSize.x * Constants.Arithmetic.Half;
            float halfHeight = areaSize.y * Constants.Arithmetic.Half;

            bottomLeft = new Vector2(-halfWidth, -halfHeight);
            topRight = new Vector2(halfWidth, halfHeight);
        }

        /// <summary>
        /// Redefines the area from the two specified arbitrary Vector2 points. </summary>
        public void SetArea(Vector2 pointA, Vector2 pointB)
        {
            bottomLeft = Vector2.Min(pointA, pointB);
            topRight = Vector2.Max(pointA, pointB);
        }

        /// <summary>
        /// Returns true if a specified Vector2 absolute point is contained within the area (inclusive). </summary>        
        public bool Contains(Vector2 point)
        {
            return (point.x >= BottomLeft.x && point.x <= TopRight.x &&
                    point.y >= BottomLeft.y && point.y <= TopRight.y);
        }

        /// <summary>
        /// Finds the closest distance from a specified Vector2 absolute point to the area border (even if its outside the area). </summary> 
        public float BorderDistance(Vector2 point)
        {
            if (point.x >= BottomLeft.x && point.x <= TopRight.x ||
                point.y >= BottomLeft.y && point.y <= TopRight.y)
            {
                // closest distance is to the area border (inside or outside)
                Vector2 areaOffset = Vector2.Min(point - BottomLeft, TopRight - point);
                return Mathf.Abs(Mathf.Min(areaOffset.x, areaOffset.y));
            }

            // closest distance is to the area corner (point is outside the area)
            Vector2 closestPoint;                
            if (point.x > TopRight.x && point.y > TopRight.y)
            {
                closestPoint = TopRight;
            }
            else if (point.x < BottomLeft.x && point.y > TopRight.y)
            {
                // Top left
                closestPoint.x = BottomLeft.x;
                closestPoint.y = TopRight.y;
            }
            else if (point.x < BottomLeft.x && point.y < BottomLeft.y)
            {
                closestPoint = BottomLeft;
            }
            else if (point.x > TopRight.x && point.y < BottomLeft.y)
            {
                // Bottom right
                closestPoint.x = TopRight.x;
                closestPoint.y = BottomLeft.y;                    
            }
            else
                throw new UnityException("Internal algorithm error");

            return Vector2.Distance(point, closestPoint);
        }

        /// <summary>
        /// Returns a bordered version of a specified area (inner border if a specified thickness is negative, outer border if it's positive).
        /// Note: Exceeding inner border will not cause area sides to flip. Area sides will converge into the area center. </summary>
        public static Area2 ApplyBorder(Area2 area, float borderThickness)
        {
            Vector2 border = new Vector2(borderThickness, borderThickness);

            Vector2 borderedBottomLeft = area.BottomLeft - border;
            Vector2 borderedTopRight = area.TopRight + border;

            ConvergeSwapped(ref borderedBottomLeft.x, ref borderedTopRight.x);
            ConvergeSwapped(ref borderedBottomLeft.y, ref borderedTopRight.y);

            return new Area2(borderedBottomLeft, borderedTopRight);
        }

        /// <summary>
        /// If the supplied values are not in the prescribed order, they are both set to their arithmetic mean (their center). </summary>
        static void ConvergeSwapped(ref float smallerValue, ref float greaterValue)
        {
            if (smallerValue <= greaterValue)
                return;

            float center = (smallerValue + greaterValue) * Constants.Arithmetic.Half;
            smallerValue = center;
            greaterValue = center;
        }
    }
}