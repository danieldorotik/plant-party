﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Collections;
using System.Collections.Generic;

namespace UnityExtras
{
    /// <summary>
    /// A generic collection similar to List with support of item reuse, having an O(1) complexity.
    /// Basically a lean pool, useful for example as an intermediate local pool in cases when a frequent use
    /// of global pooling would result in performance overhead (e.g. GameObject reparenting).
    /// Note: Supports GC-free foreach iteration. </summary>
    public class RecyclingList<T> : ICollection<T>
    {
        /// <summary>
        /// Number of items in the collection (both the used and available).
        /// O(1) operation. </summary>
        public int Count
        {
            get { return items.Count; }
        }

        /// <summary>
        /// Number of used items in the collection.
        /// O(1) operation. </summary>
        public int UsedCount
        {
            get { return usedItems.Count; }
        }

        /// <summary>
        /// Number of available items in the collection.
        /// O(1) operation. </summary>
        public int AvailableCount
        {
            get { return availableItems.Count; }
        }

        /// <summary>
        /// Indicates whether the ICollection is read-only (it's not). </summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// A list-like indexer of all the items in the collection (both the used and available).
        /// O(1) operation. </summary>
        public T this[int index]
        {
            get { return items[index]; }
            set { items[index] = value; }
        }

        readonly List<T> items;
        readonly HashSet<T> usedItems, availableItems;

        /// <summary>
        /// Initializes the collection instance. </summary>
        public RecyclingList()
        {
            items = new List<T>();
            usedItems = new HashSet<T>();
            availableItems = new HashSet<T>();
        }

        /// <summary>
        /// Adds supplied item to the collection.
        /// O(1) operation (see List.Add and HashSet.Add). </summary>
        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        /// <summary>
        /// Adds supplied item to the collection.
        /// O(1) operation (see List.Add and HashSet.Add). </summary>
        public bool Add(T item)
        {
            return Add(item, asUsed: false);
        }

        /// <summary>
        /// Adds supplied item to the collection.
        /// Parameter 'asUsed' specifies whether the item shall be added as an already used or as available.
        /// O(1) operation (see List.Add and HashSet.Add). </summary>
        public bool Add(T item, bool asUsed)
        {
            bool added = asUsed ? usedItems.Add(item) : availableItems.Add(item);
            if (added)
                items.Add(item);

            return added;
        }

        /// <summary>
        /// Removes all items from the collection.
        /// O(n) operation. </summary>
        public void Clear()
        {
            items.Clear();
            usedItems.Clear();
            availableItems.Clear();
        }

        /// <summary>
        /// Returns true if the collection contains the supplied item.
        /// O(1) operation. </summary>
        public bool Contains(T item)
        {
            return (usedItems.Contains(item) || availableItems.Contains(item));
        }

        /// <summary>
        /// Copies all items from the collection to the provided array, starting from the specified arrayIndex.
        /// O(n) operation. </summary>
        public void CopyTo(T[] array, int arrayIndex)
        {
            items.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through all the items of the collection. </summary>
        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through all the items of the collection. </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Removes the first occurrence of a specified item in the collection
        /// Returns true if the item was found and removed.
        /// O(n) operation. </summary>
        public bool Remove(T item)
        {
            bool usedRemoved = (usedItems.Contains(item) && usedItems.Remove(item));
            bool availableRemoved = (availableItems.Contains(item) && availableItems.Remove(item));
            bool removed = (usedRemoved || availableRemoved);

            if (removed)
                removed &= items.Remove(item);

            return removed;
        }

        /// <summary>
        /// Returns available item from the collection (if any), otherwise returns a default type value.
        /// Note: Returned available item is internally marked as used and will not get returned anymore, 
        ///       until it's made available again through a Dismiss call.
        /// O(1) operation (see HashSet.Add). </summary>        
        public T Recycle()
        {
            using (var iterator = availableItems.GetEnumerator())
            {
                if (!iterator.MoveNext())
                    return default(T);

                T availableItem = iterator.Current;

                availableItems.Remove(availableItem);
                usedItems.Add(availableItem);

                return availableItem;
            }
        }

        /// <summary>
        /// Supplied item is internally marked from used to available, so it can be returned by a Recycle call again.
        /// Returns true if the supplied item was recognized as an used one and made available again.
        /// O(1) operation (see HashSet.Add). </summary>
        public bool Dismiss(T item)
        {
            if (!usedItems.Contains(item))
                return false;

            usedItems.Remove(item);
            availableItems.Add(item);

            return true;
        }

        /// <summary>
        /// Returns an IEnumerable for iteration over the used items (GC-free). </summary>
        public IEnumerable<T> UsedItems()
        {
            return usedItems;
        }

        /// <summary>
        /// Returns an IEnumerable for iteration over the available items (GC-free). </summary>
        public IEnumerable<T> AvailableItems()
        {
            return availableItems;
        }

        /// <summary>
        /// Returns a GC-free enumerable iterator of used collection items.
        /// Allows for modification of the collection with Recycle and Dismiss calls during the iteration.
        /// Slightly less efficient than UsedItems(), as all the collection items are walked over internally. </summary>
        public RecyclingListIterator<T> UsedItemsModifiable()
        {
            return new RecyclingListIterator<T>(items, usedItems);
        }

        /// <summary>
        /// Returns a GC-free enumerable iterator of available collection items.
        /// Allows for modification of the collection with Recycle and Dismiss calls during the iteration.
        /// Slightly less efficient than AvailableItems(), as all the collection items are walked over internally. </summary>
        public RecyclingListIterator<T> AvailableItemsModifiable()
        {
            return new RecyclingListIterator<T>(items, availableItems);
        }
    }

    /// <summary>
    /// A GC-free iterator for the RecyclingList.
    /// Since the Unity 5.5, standard collections no longer produce garbage when used with foreach,
    /// but as the RecyclingList provides a custom read-write iteration, it has to handle this issue on its own.
    /// More info:
    ///   https://unity3d.com/learn/tutorials/topics/performance-optimization/optimizing-garbage-collection-unity-games
    ///   https://answers.unity.com/questions/849524/using-foreach-still-bad-in-46.html </summary>
    public struct RecyclingListIterator<T>
    {
        readonly List<T> items;
        readonly HashSet<T> allowedItems;
        int index;

        public RecyclingListIterator(List<T> items, HashSet<T> allowedItems)
        {
            this.items = items;
            this.allowedItems = allowedItems;
            index = -1;
        }

        public RecyclingListIterator<T> GetEnumerator()
        {
            return this;
        }

        public T Current
        {
            get { return items[index]; }
        }

        public bool MoveNext()
        {
            while(++index < items.Count && !allowedItems.Contains(items[index])) { }            
            return (index < items.Count);
        }
    }
}