﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityExtras.PointDB
{    
    /// <summary>
    /// A 2D discrete space table for O(1) addition/removal/retrieval of PointRecord records. 
    /// Note: Used only internally by the PointDB. </summary>
    class PointDBTable<T>
    {
        readonly Dictionary<int, Dictionary<int, HashSet<PointRecord<T>>>> db;

        /// <summary>
        /// Creates the table. </summary>
        public PointDBTable()
        {
            db = new Dictionary<int, Dictionary<int, HashSet<PointRecord<T>>>>();
        }

        /// <summary>
        /// Adds 'records' into a table field specified by 'address'. </summary>
        public void AddRecord(Address address, PointRecord<T> record)
        {
            if (!db.ContainsKey(address.X))
                db.Add(address.X, new Dictionary<int, HashSet<PointRecord<T>>>());

            var column = db[address.X];
            if (!column.ContainsKey(address.Y))
                column.Add(address.Y, new HashSet<PointRecord<T>>());

            var field = column[address.Y];
            field.Add(record);
        }

        /// <summary>
        /// Removes 'record' from a table field specified by 'address'.
        /// Returns true if record was successfully located and removed. </summary>
        public bool RemoveRecord(Address address, PointRecord<T> record)
        {
            var field = GetField(address);
            if (!field.Contains(record))
                return false;

            bool removalResult = field.Remove(record);
            if (removalResult)
            {
                var column = db[address.X];

                // Remove column's field if it has no records
                if (field.Count == 0)
                    column.Remove(address.Y);

                // Remove column if it has no fields
                if (column.Count == 0)
                    db.Remove(address.X);
            }

            return removalResult;
        }

        /// <summary>
        /// Returns table field specified by 'address'.
        /// Returns null if such field does not exist. </summary>
        public HashSet<PointRecord<T>> GetField(Address address)
        {
            // Note that TryGetValue is more efficient than ContainsKey + indexer.
            Dictionary<int, HashSet<PointRecord<T>>> column;
            if (!db.TryGetValue(address.X, out column))
                return null;

            HashSet<PointRecord<T>> field;
            column.TryGetValue(address.Y, out field);

            return field;
        }

        /// <summary>
        /// Calls passed 'recordAction' for each record in the table and returns count of the processed records.
        /// Optionally it's possible to limit the amount of processed records using the 'maxResults' parameter. </summary>
        public int CallForEachRecord(Action<PointRecord<T>> recordAction, int? maxResults = null)
        {
            var query = QueryAll();

            if (maxResults != null)
                query = query.Take(maxResults.Value);

            int processedRecords = 0;
            foreach (var record in query)
            {
                recordAction(record);
                ++processedRecords;
            }

            return processedRecords;
        }

        IQueryable<PointRecord<T>> QueryAll()
        {
            return db.SelectMany(column => column.Value)
                     .SelectMany(record => record.Value)
                     .AsQueryable();
        }
    }
}