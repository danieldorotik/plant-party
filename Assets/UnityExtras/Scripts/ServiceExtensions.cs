﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras.Services
{
    /// <summary>
    /// Extension methods of UnityEngine.Object related to the UnityExtras.Services. </summary>
    public static class UnityObjectExtensions
    {
        /// <summary>
        /// Tries to convert the instance into an IServiceProvider interface and return it.
        /// Returns null on failure (and logs the failure details). 
        /// Check the IServiceProvider for being a valid provider and logs a potential issue. </summary>
        public static IServiceProvider<T> AsServiceProvider<T>(this Object providerObject) where T : Object
        {
            if (!providerObject)
            {
                Debug.LogError("Supposed Service Provider object is null");
                return null;
            }

            IServiceProvider<T> serviceProvider = providerObject as IServiceProvider<T>;
            if (serviceProvider == null)
            {
                Debug.LogError(string.Format("The {0} does not implement interface {1}", providerObject, typeof(IServiceProvider<T>)));
                return null;
            }

            if (!serviceProvider.ProviderValid)
                Debug.LogError(string.Format("The {0} provider is not valid", serviceProvider));

            return serviceProvider;
        }
    }
}