﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    /// <summary>
    /// Calculates the basic exponential moving average of a repeatedly applied input value.
    /// Input value doesn't have to be sampled in discrete time intervals (supports variable deltaTime).
    /// Basically works as a low-pass filter. </summary>
    public class ExponentialMovingAverage
    {
        /// <summary>
        /// Current exponential moving average value.
        /// Changing it sets it to the specified value, which also effectively makes it the new initialValue. </summary>
        public double Value { get; set; }

        /// <summary>
        /// Amount of time (seconds) for which an input value needs to be applied so that a relative exponential average response
        /// progresses by approximately 63.2% (1-1/e) towards the input value.
        /// For example with a TimeConstant of 5, initial value of 0 and continuously applied input value of 10, it will take 5 seconds
        /// till the exponential average value reaches 6.32. </summary>
        public double TimeConstant
        {
            get { return timeConstant; }
            set
            {
                timeConstant = value;
                timeConstantMultiplier = 1 / timeConstant;

                if (timeConstant > 0d && timeConstantMultiplier.IsFinite())
                    return;

                const double fallbackTimeConstant = 10f;
                Debug.LogWarning(string.Format(Constants.DebugMessages.InvalidValueUsingFallback, "time constant", timeConstant, fallbackTimeConstant));

                timeConstant = fallbackTimeConstant;
                timeConstantMultiplier = 1 / timeConstant;
            }
        }

        double timeConstant, timeConstantMultiplier;

        /// <summary>
        /// Creates an instance with a specified timeConstant (see the TimeConstant property for more info) and initialValue (see the Value property for more info). </summary>
        public ExponentialMovingAverage(double timeConstant, double initialValue)
        {
            TimeConstant = timeConstant;
            Value = initialValue;
        }

        /// <summary>
        /// Exponentially averages a specified value with the current exponential average value.
        /// Current exponential moving average value is updated accordingly (Value property) and also it is returned.
        /// Parameter 'deltaTime' specifies the seconds passed since the last current value update (through the Apply or by setting the initialValue). </summary>
        public double Apply(double applyValue, double deltaTime)
        {
            if (deltaTime < 0f)
            {
                Debug.LogWarning("Skipping Apply operation, since the deltaTime cannot be a negative value");
                return Value;
            }

            double smoothingFactor = 1 - Math.Exp(-deltaTime * timeConstantMultiplier);

            // Code below is a mathematic and programmatic simplification of the exponential moving average equation:
            //    Value = smoothingFactor * applyValue + (1 - smoothingFactor) * Value
            Value += smoothingFactor * (applyValue - Value);

            return Value;
        }
    }
}