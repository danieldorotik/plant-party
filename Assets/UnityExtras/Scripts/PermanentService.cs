﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections.Generic;

namespace UnityExtras.Services
{
    /// <summary>
    /// The static PermanentService protects registered services from being garbage collected by keeping a permanent reference to them.
    /// It does so by creating a DontDestroyOnLoad GameObject and adding a PermanentServices MonoBehaviour to it. Then it uses that script for reference keeping.
    /// Note: A service can be any UnityEngine.Object, but the most common use would be to keep IServiceProvider service providers. </summary>
    public static class PermanentService
    {
        /// <summary>
        /// Returns Transform of the DontDestroyOnLoad PermanentServices GameObject.
        /// Suitable for parenting GameObjects which are supposed to remain. </summary>
        public static Transform ParentObject
        {
            get { return ParentObjectScript ? ParentObjectScript.transform : null; }
        }

        static PermanentServices ParentObjectScript
        {
            get { return GetParentObjectScript(); }
        }

        class PermanentServices : MonoBehaviour
        {
            [SerializeField]
            List<Object> registeredServices;

            public void RegisterService(Object service)
            {
                registeredServices.Add(service);
            }

            void Awake()
            {
                if (registeredServices == null)
                    registeredServices = new List<Object>();
            }

            void OnDestroy()
            {
                ParentObjectDestroyed();
            }
        }

        static volatile PermanentServices parentObjectScript;
        static readonly object ParentObjectLock = new object();
        static bool parentObjectDestroyed;
        
        /// <summary>
        /// Registers passed Object for being permanently referenced by the DontDestroyOnLoad PermanentServices GameObject. </summary>
        public static void Register(Object service)
        {
            if (!ParentObjectScript)
            {
                Debug.LogError(string.Format("Cannot register service '{0}', since the permanent services parent is missing", service.name));
                return;
            }

            ParentObjectScript.RegisterService(service);
        }

        static void ParentObjectDestroyed()
        {
            parentObjectDestroyed = true;
        }

        static PermanentServices GetParentObjectScript()
        {
            if (parentObjectDestroyed)
                return null;

            if (parentObjectScript == null)
            {
                lock (ParentObjectLock)
                {
                    if (parentObjectScript == null)
                        parentObjectScript = CreateParentGameObject();
                }
            }

            return parentObjectScript;
        }

        static PermanentServices CreateParentGameObject()
        {
            string objectName = typeof(PermanentServices).Name;
            const string root = "/";

            GameObject parentGameObject = GameObject.Find(root + objectName) ?? new GameObject(objectName);
            Object.DontDestroyOnLoad(parentGameObject);

            return parentGameObject.GetComponent<PermanentServices>() ?? parentGameObject.AddComponent<PermanentServices>();
        }
    }
}
