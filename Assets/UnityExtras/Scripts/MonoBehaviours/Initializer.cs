﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections.Generic;

namespace UnityExtras
{
    /// <summary>
    /// Initializer for IInitializable UnityEngine.Object instances. </summary>
    public class Initializer : MonoBehaviour, IInitializable
    {
        [SerializeField, Tooltip("List of Objects to be initialized as IInitializable instances.")]
        List<Object> initializables;

        [SerializeField, Tooltip("If set, the Initializer automatically initializes assigned initializables on Start.")]
        bool autoInitialize;

        [SerializeField, Tooltip("If set, the Initializer marks itself as a DontDestroyOnLoad object upon its Start.")]
        bool dontDestroyOnLoad;

        /// <summary>
        /// Initializes the assigned 'initializables' as IInitializable instances.
        /// If instance cannot be converted to IInitializable, logs it and continues to another. </summary>
        public void Initialize()
        {
            if (initializables == null)
            {
                Debug.LogError("The list of initializables is not initialized");
                return;
            }

            foreach (var initializable in initializables)
            {
                var item = initializable as IInitializable;
                if (item == null)
                {
                    Debug.LogError(string.Format("Cannot initialize object '{0}', since it is not IInitializable", initializable.name));
                    continue;
                }

                item.Initialize();
            }
        }

        void Start()
        {
            if(autoInitialize)
                Initialize();

            if (dontDestroyOnLoad)
                DontDestroyOnLoad(this);
        }
    }
}
