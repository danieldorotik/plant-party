﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using System;

namespace UnityExtras
{
    /// <summary>
    /// Allows registering handlers which will be invoked when a renderer on the same game object changes its visibility status.
    /// Useful for propagating visibility change messages from a child which has a renderer to a parent which processes logic. </summary>
    public class VisibilityCallback : MonoBehaviour
    {
        /// <summary>
        /// Event invoked when a renderer attached on the same game object (if any) changes its visibility status. </summary>
        public event Action BecameVisible, BecameInvisible;

        void OnBecameVisible()
        {
            if (BecameVisible != null)
                BecameVisible();
        }

        void OnBecameInvisible()
        {
            if (BecameInvisible != null)
                BecameInvisible();
        }
    }
}