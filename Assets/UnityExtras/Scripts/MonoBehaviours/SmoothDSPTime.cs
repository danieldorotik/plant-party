﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System;
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    using TimeFunction = Func<double>;

    /// <summary>
    /// Provides an interpolated version of the AudioSettings.dspTime, useful for driving animations that need to stay in sync with the Unity Audio.
    /// 
    /// Rationale:
    ///   The AudioSettings.dspTime doesn't have to change as frequently as Unity renders new frames, depending on factors like sample rate and platform.
    ///   Thus driving animations based on its value could result in a choppy movement. This MonoBehaviour solves that issue.
    /// 
    /// The exposed value increments every frame by an interval that approaches the Time.deltaTime (the SmoothDSPTime has a mechanism
    /// to prevent drifting of the calculated Value from the Unity's DSP time - any amount of deviation is smoothly equalized within seconds).
    /// 
    /// Note: The Value is updated on demand or every frame, thus there is no need for a specific script execution order. </summary>
    public class SmoothDSPTime : MonoBehaviour
    {
        [SerializeField, Tooltip("Specifies the used exponential moving average time constant.")]
        double smoothingTimeConstant;

        /// <summary>
        /// Returns an interpolated version of the AudioSettings.dspTime that changes every frame by a step that approaches the Time.deltaTime. </summary>
        public double Value
        {
            get { return InternalDSPTime - SmoothDeviation; }
        }

        double InternalDSPTime
        {
            get { return refreshInternalDSPTime(); }
        }

        double Deviation
        {
            get { return refreshDeviation(); }
        }

        double SmoothDeviation
        {
            get { return refreshSmoothDeviation(); }
        }

        static double DSPTime
        {
            get { return AudioSettings.dspTime; }
        }

        TimeFunction refreshInternalDSPTime, refreshDeviation, refreshSmoothDeviation;

        void Awake()
        {
            refreshInternalDSPTime = CreateInternalDSPTime();
            refreshDeviation = CreateDeviation();
            refreshSmoothDeviation = CreateSmoothDeviation();
        }

        void Update()
        {
            refreshInternalDSPTime();
            refreshSmoothDeviation();
        }        

        TimeFunction CreateDeviation()
        {
            double lastDSPTime = double.NaN;
            double deviation = 0d;

            return () =>
            {
                // The deviation reading is most accurate when a DSPTime change is detected
                if (!lastDSPTime.Approximately(DSPTime))
                {
                    lastDSPTime = DSPTime;
                    deviation = InternalDSPTime - DSPTime;                    
                }

                return deviation;
            };
        }

        TimeFunction CreateSmoothDeviation()
        {
            const double emaInitialValue = 0d;
            ExponentialMovingAverage smoothDeviation = new ExponentialMovingAverage(smoothingTimeConstant, emaInitialValue);
            int lastUpdateFrame = int.MinValue;

            return () =>
            {
                if (lastUpdateFrame != Time.frameCount)
                {
                    lastUpdateFrame = Time.frameCount;
                    smoothDeviation.Apply(Deviation, Time.deltaTime);
                }

                return smoothDeviation.Value;
            };
        }

        static TimeFunction CreateInternalDSPTime()
        {
            double dspTime = AudioSettings.dspTime;
            int lastUpdateFrame = Time.frameCount;

            return () =>            
            {
                if(lastUpdateFrame != Time.frameCount)
                {
                    lastUpdateFrame = Time.frameCount;
                    dspTime += Time.deltaTime;
                }

                return dspTime;
            };
        }
    }
}
