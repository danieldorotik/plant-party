﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using System.Collections.Generic;
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    /// <summary>
    /// Base class for a generic-type pooling.
    /// Pooled generic types are grouped in generic-type groups (implicitly created).
    ///   - for example, a prefab can be used to identify a group of pooled GameObjects. </summary>
    public abstract class GroupPool<TItem, TGroup> : MonoBehaviour
    {
        /// <summary>
        /// When set to true, pool automatically creates a new pooled type instance if it has no more items to pop.
        /// Default: true </summary>
        public bool AutoExpand { get; set; }

        /// <summary>
        /// Number of popped (and not yet pushed back) pool items.
        /// Informative value suitable for development diagnostic purposes. </summary>
        public int Utilization { get; private set; }

        Dictionary<TGroup, Stack<TItem>> poolItems;

        /// <summary>
        /// Returns a pooled instance from the specified pool group or a default value of the grouped type.
        /// Creates and returns a new instance if the pool has no more items pooled in the group and AutoExpand = true.
        /// Note: The popped item is no longer tracked by the pool, only the diagnostic Utilization counter is incremented. </summary>
        public TItem PopItem(TGroup group)
        {
            AssureGroup(group);

            if (poolItems[group].Count == 0 && AutoExpand)
                Expand(group, 1);

            if (poolItems[group].Count == 0)
            {
                Debug.LogWarning(string.Format("Pool has no free item for group {0}", group.ToString()));
                return default(TItem);
            }

            TItem item = poolItems[group].Pop();
            PrepareItem(item, group);

            Utilization++;

            return item;
        }

        /// <summary>
        /// Puts the instance back to the specified pool group.
        /// Keep in mind that this method is not meant for pool initialization. The pool shall create instances on its own during the PopItem.
        /// Optionally it's possible to notify the pool to destroy the pushed item and replace it with a new instance (recreate = true). </summary>
        public void PushItem(TItem item, TGroup group, bool recreate = false)
        {
            AssureGroup(group);

            if (!recreate)
            {
                CleanupItem(item, group);
                poolItems[group].Push(item);
            }
            else
            {
                DestroyItem(item, group);
                Expand(group, 1);
            }

            Utilization--;
        }    

        protected virtual void Awake()
        {
            AutoExpand = true;
            poolItems = new Dictionary<TGroup, Stack<TItem>>();
        }

        void OnDestroy()
        {
            CleanupItems();
        }

        void AssureGroup(TGroup group)
        {
            if (typeof(TGroup).IsClass && group.IsDefault())
                throw new UnityException("Pool group 'null' is not allowed");

            if (!poolItems.ContainsKey(group))
                poolItems.Add(group, new Stack<TItem>());
        }

        void CleanupItems()
        {
            foreach (var group in poolItems)
            {
                foreach (var item in group.Value)
                {
                    DestroyItem(item, group.Key);
                }
            }

            poolItems.Clear();
            Utilization = 0;
        }

        /// <summary>
        /// Creates 'count' new instance(s) of the pooled type and adds them to the specified pool group. </summary>
        protected void Expand(TGroup group, int count)
        {
            if (count == 0)
                return;

            AssureGroup(group);

            for (int i = 0; i < count; i++)
            {
                TItem item = CreateItem(group);
                if (item == null)
                    throw new UnityException("Created item is null");

                CleanupItem(item, group);
                poolItems[group].Push(item);
            }
        }

        /// <summary>
        /// Returns a new instance of the pooled type to be added to the specified pool group. </summary>
        protected abstract TItem CreateItem(TGroup group);

        /// <summary>
        /// Prepares pooled item before its pop from the pool. </summary>
        protected abstract void PrepareItem(TItem item, TGroup group);

        /// <summary>
        /// Resets the pushed item before its adding to the specified pool group (after creation or after push). </summary>
        protected abstract void CleanupItem(TItem item, TGroup group);

        /// <summary>
        /// Destroys the pool item. </summary>
        protected abstract void DestroyItem(TItem item, TGroup group);
    }
}