﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;

namespace UnityExtras
{
    /// <summary>
    /// Simplifies the GroupPool for uses where grouping is not necessary.
    /// Supports pool pre-allocation. </summary>
    public abstract class Pool<T> : GroupPool<T, int> where T : class
    {
        [SerializeField, Tooltip("The initial number of pooled type instances, created by the Pool upon its creation.")]
        int initialPoolSize;

        /// <summary>
        /// Returns a pooled instance or a default value of the pooled type.
        /// Creates and returns a new instance if the pool has no more items pooled and AutoExpand = true.
        /// Note: The popped item is no longer tracked by the pool, only the diagnostic Utilization counter is incremented. </summary>
        public T PopItem()
        {
            return PopItem(default(int));
        }

        /// <summary>
        /// Puts the instance back to the pool.
        /// Keep in mind that this method is not meant for pool initialization. The pool shall create instances on its own during the PopItem.
        /// Optionally it's possible to notify the pool to destroy the pushed item and replace it with a new instance (recreate = true). </summary>
        public void PushItem(T item, bool recreate = false)
        {
            PushItem(item, default(int), recreate);
        }

        protected override void Awake()
        {
            base.Awake();
            Expand(default(int), initialPoolSize);
        }

        protected override T CreateItem(int group)
        {
            return CreateItem();
        }

        protected override void PrepareItem(T item, int group)
        {
            PrepareItem(item);
        }

        protected override void CleanupItem(T item, int group)
        {
            CleanupItem(item);
        }

        protected override void DestroyItem(T item, int group)
        {
            DestroyItem(item);
        }

        /// <summary>
        /// Returns a new instance of the pooled type. </summary>
        protected abstract T CreateItem();

        /// <summary>
        /// Prepares pooled item before its pop from the pool. </summary>
        protected abstract void PrepareItem(T item);

        /// <summary>
        /// Resets the pushed item before its adding to the pool (after creation or after push). </summary>
        protected abstract void CleanupItem(T item);

        /// <summary>
        /// Destroys the pool item. </summary>
        protected abstract void DestroyItem(T item);
    }  
}