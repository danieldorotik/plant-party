﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityEngine.UI;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    /// <summary>
    /// Basic FPS counter MonoBehaviour which periodically updates a specified Text component in a specified time interval. </summary>
    public class FPSCounter : MonoBehaviour
    {        
        [SerializeField, Tooltip("UI Component to be updated with a calculated FPS counter value.")]
        Text text;

        /// <summary>
        /// Interval (in seconds) for a periodic FPS calculation and the assigned Text component update.
        /// Note: First update is issued immediately when this script is initialized. </summary>
        [SerializeField, Tooltip("Interval (in seconds) for a periodic FPS calculation and the assigned Text component update.")]
        float updateInterval = 1f;

        int lastFrameCount;
        float lastUpdateTime;
        
        void Awake()
        {
            if (!text)
            {
                Debug.LogError(string.Format(Constants.DebugMessages.ComponentNotLinkedDefault, typeof(Text)));                
                text = GetComponent<Text>() ?? gameObject.AddComponent<Text>();
            }

            const float invalidUpdateInterval = 0f;
            if (Mathf.Approximately(updateInterval, invalidUpdateInterval))
            {
                const float fallbackUpdateInterval = 1f;
                Debug.LogError(string.Format("Update interval {0} is not allowed, falling back to {1}.", updateInterval, fallbackUpdateInterval));
                updateInterval = fallbackUpdateInterval;
            }

            MarkTimeInfo();
            InvokeRepeating("UpdateText", 0f, updateInterval);            
        }

        void UpdateText()
        {
            float fps = CalculateFPS();
            MarkTimeInfo();
            text.text = string.Format("{0:N0} FPS", fps);
        }

        void MarkTimeInfo()
        {
            lastFrameCount = Time.frameCount;
            lastUpdateTime = Time.time;
        }

        float CalculateFPS()
        {
            int framesPassed = Time.frameCount - lastFrameCount;
            float timePassed = Time.time - lastUpdateTime;

            // Note: Division by zero is allowed in IEEE 754 and produces infinity or NaN
            float fps = framesPassed / timePassed;

            return fps.IsFinite() ? fps : 0f;
        }
    }
}