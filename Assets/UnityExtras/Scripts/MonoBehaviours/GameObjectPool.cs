﻿/*
-----------------------------------------------------------------------------
This source file is part of UnityExtras

Copyright (c) 2017-2018 Daniel Dorotík

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
using UnityEngine;
using UnityExtras.ExtensionMethods;

namespace UnityExtras
{
    /// <summary>
    /// A basic pool of GameObject types, grouped by a GameObject type (e.g. a prefab).
    /// Caller asks the pool for a clone of supplied prefab and the pool either pops an existing item or creates a new clone.
    /// When GameObject is pushed back to the stack, it's deactivated, reparented and its transform is set to match the original GameObject (group prefab). </summary>
    public class GameObjectPool : GroupPool<GameObject, GameObject>
    {
        /// <summary>
        /// Sets whether a GameObject popped from the pool shall be activated.
        /// Note: If a caller reparents the popped GameObject, it's less expensive to reparent the GameObject when it's still inactive.
        /// Default: true </summary>
        public bool ActivatePopped { get; set; }

        protected override void Awake()
        {
            base.Awake();
            ActivatePopped = true;
        }

        protected override GameObject CreateItem(GameObject group)
        {
            return Instantiate(group, transform);
        }

        protected override void PrepareItem(GameObject item, GameObject group)
        {
            if (!item)
            {
                Debug.LogWarning("Cannot prepare a null item");
                return;
            }

            if(ActivatePopped)
                item.SetActive(true);
        }

        protected override void CleanupItem(GameObject item, GameObject group)
        {
            if (!item)
            {
                Debug.LogWarning("Cannot cleanup a null item");
                return;
            }

            item.SetActive(false);

            if (item.transform.parent != transform)
                item.transform.SetParent(transform);
                
            if (group)
                item.transform.Copy(group.transform);
            else
                Debug.LogWarning(string.Format("Group for a cleanup item '{0}' is null", item.name));            
        }

        protected override void DestroyItem(GameObject item, GameObject group)
        {
            if (!item)
            {
                Debug.LogWarning("Cannot destroy a null item");
                return;
            }

            Destroy(item);
        }
    }
}
