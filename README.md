## Summary
**Plant Party** is a mobile music game allowing players to have the same experience a musician feels when playing along to a music freely. 

The game is designed to be accessible to most of the players, despite their gaming experience, age or impairment.

Plant Party is made with [Unity](https://unity3d.com/) and C#, closing to its release on the Google Android platform.

## UnityExtras
**UnityExtras** is a set of features not specific to the game, useful also for other projects. Highlights:

* [Pooling](Docs/UnityExtras/Pooling.md)
    * base class for a generic type pool, where pooled instances are kept in groups identified by a custom generic type
    * a descendant class [`GameObjectPool`][GameObjectPool] pools GameObjects in groups identified by their original prefab
* [Data storage](Docs/UnityExtras/DataStorage.md)
    * model to store/restore `ScriptableObject` or any other `UnityEngine.Object` to/from an arbitrary Data Source, using an arbitrary Serializer, in [IoC][IoC] fashion
* [Services](Docs/UnityExtras/Services.md)
    * model to handle global states in [IoC][IoC] fashion without any singletons, by use of `ScriptableObject` instances stored as assets
* [Span](Docs/UnityExtras/Span.md)
    * a value type which represents a continuous interval, supplied with its own `PropertyDrawer` and a custom range attribute
* [PointDB](Docs/UnityExtras/PointDB.md)
    * a spatial database for efficient storage/lookup of generic type records, each representing a point in 2D space
* [ExtensionMethods](Assets/UnityExtras/Scripts/ExtensionMethods.cs)
    * a set of extension methods for standard Unity and .NET types

The complete list of UnityExtras sources can be found [here](Docs/UnityExtras/SourcesList.md).

## License
Plant Party is released under the [MIT License](https://opensource.org/licenses/MIT).

[IoC]: https://en.wikipedia.org/wiki/Inversion_of_control
[GameObjectPool]: Assets/UnityExtras/Scripts/MonoBehaviours/GameObjectPool.cs