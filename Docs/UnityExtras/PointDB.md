## PointDB

The [`PointDB`][PointDB] is a spatial database designed for efficient storage/lookup of generic type records, each representing a point in 2D space. 

Its efficiency is based on a continuous space division into discrete intervals. Thus it's possible to fine-tune the database by setting a discrete interval which fits the most to the given use case.

The database currently supports **rectangular** and **circular** spatial queries. For sake of efficiency, queries are designed in a NonAlloc fashion.

See the detailed documentation in the [`PointDB`][PointDB] source.

## Associated sources
1. [`PointDB.cs`][PointDB]
1. [`PointDBTable.cs`][PointDBTable]
1. [`PointDBTypes.cs`][PointDBTypes]

[PointDB]: ../../Assets/UnityExtras/Scripts/PointDB.cs
[PointDBTable]: ../../Assets/UnityExtras/Scripts/PointDBTable.cs
[PointDBTypes]: ../../Assets/UnityExtras/Scripts/PointDBTypes.cs
