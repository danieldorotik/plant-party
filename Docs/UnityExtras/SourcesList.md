## UnityExtras sources

Below is the list of UnityExtras sources, categorized by their subject:

* Various
	1. [`Area.cs`](../../Assets/UnityExtras/Scripts/Area.cs) | area in 2D space specified by two arbitrary Vector2 points. Contains static methods for various Area operations
	1. [`ExponentialMovingAverage.cs`](../../Assets/UnityExtras/Scripts/ExponentialMovingAverage.cs)
        * calculates the exponential moving average of an input signal
        * input value doesn't have to be sampled at discrete time intervals (supports variable deltaTime)
        * basically works as a low-pass filter
	1. [`ExtensionMethods.cs`](../../Assets/UnityExtras/Scripts/ExtensionMethods.cs) | a set of extension methods for standard Unity and .NET types
	1. [`Auxiliaries.cs`](../../Assets/UnityExtras/Scripts/Auxiliaries.cs) | a set of general-purpose utility methods, notably the QueryAutoExpand method for a simple use of NonAlloc-style queries
	1. [`GenericHistory.cs`](../../Assets/UnityExtras/Scripts/GenericHistory.cs) | an optimized queue with an O(1) lookup complexity, where exceeding the specified queue size automatically drops historical elements
	1. [`LiteStateMachine.cs`](../../Assets/UnityExtras/Scripts/LiteStateMachine.cs) | a lightweight state machine where states can be easily defined by anonymous functions and identified by a generic value type, for example an enum
	1. [`CustomStateMachineBehavior.cs`](../../Assets/UnityExtras/Scripts/CustomStateMachineBehavior.cs) | allows setting custom persistent callbacks to a state of an Animation Controller. Also useful along with the [UnityExtras Services](Services.md#markdown-header-appendix-a-persistent-callback)
	1. [`IInteractive.cs`](../../Assets/UnityExtras/Scripts/IInteractive.cs) | an interface for object which can be interacted by a user
	1. [`FPSCounter.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/FPSCounter.cs) | a basic FPS counter MonoBehaviour which periodically updates a specified Text component in a specified time interval
	1. [`VisibilityCallback.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/VisibilityCallback.cs) | A MonoBehaviour which exposes events that are invoked during its OnBecameVisible / OnBecameInvisible. Useful for propagating visibility change messages from a GameObject which has a Renderer to a (parent) GameObject which handles logic
	1. [`SmoothDSPTime.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/SmoothDSPTime.cs) | provides an interpolated version of the AudioSettings.dspTime, which unlike the original is suitable for driving animations since it changes every frame accurately

***
* Pooling ([more info](Pooling.md))
    1. [`GroupPool.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/GroupPool.cs)
    1. [`Pool.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/Pool.cs)
    1. [`RecyclingList.cs`](../../Assets/UnityExtras/Scripts/RecyclingList.cs)
    1. [`GameObjectPool.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/GameObjectPool.cs)

***
* Data storage ([more info](DataStorage.md))
    1. [`StoredObject.cs`](../../Assets/UnityExtras/Scripts/ScriptableObjects/StoredObject.cs)
    1. [`IStoredObject.cs`](../../Assets/UnityExtras/Scripts/IStoredObject.cs)
    1. [`IDataSource.cs`](../../Assets/UnityExtras/Scripts/IDataSource.cs)
    1. [`FileDataSource.cs`](../../Assets/UnityExtras/Scripts/ScriptableObjects/FileDataSource.cs)
    1. [`ISerializer.cs`](../../Assets/UnityExtras/Scripts/ISerializer.cs)
    1. [`JsonSerializer.cs`](../../Assets/UnityExtras/Scripts/ScriptableObjects/JsonSerializer.cs)

***
* Services ([more info](Services.md))
    1. [`PrefabServiceProvider.cs`](../../Assets/UnityExtras/Scripts/ScriptableObjects/PrefabServiceProvider.cs)
    1. [`PermanentService.cs`](../../Assets/UnityExtras/Scripts/PermanentService.cs)
    1. [`IServiceProvider.cs`](../../Assets/UnityExtras/Scripts/IServiceProvider.cs)
    1. [`ServiceExtensions.cs`](../../Assets/UnityExtras/Scripts/ServiceExtensions.cs)
    1. [`Initializer.cs`](../../Assets/UnityExtras/Scripts/MonoBehaviours/Initializer.cs)
    1. [`IInitializable.cs`](../../Assets/UnityExtras/Scripts/IInitializable.cs)

***
* Span ([more info](Span.md))
    1. [`Span.cs`](../../Assets/UnityExtras/Scripts/Span.cs)
    1. [`SpanDrawer.cs`](../../Assets/UnityExtras/Scripts/Editor/SpanDrawer.cs)
    1. [`SpanRangeAttribute.cs`](../../Assets/UnityExtras/Scripts/SpanRangeAttribute.cs)

***
* PointDB ([more info](PointDB.md))
    1. [`PointDB.cs`](../../Assets/UnityExtras/Scripts/PointDB.cs)
    1. [`PointDBTable.cs`](../../Assets/UnityExtras/Scripts/PointDBTable.cs)
    1. [`PointDBTypes.cs`](../../Assets/UnityExtras/Scripts/PointDBTypes.cs)

***
* Constants:	
	1. [`Arithmetic.cs`](../../Assets/UnityExtras/Scripts/Constants/Arithmetic.cs) - defines constants useful in common calculations
	1. [`AssetMenu.cs`](../../Assets/UnityExtras/Scripts/Constants/AssetMenu.cs) - defines constants utilized by various UnityExtras sources in their CreateAssetMenu calls
	1. [`DebugMessages.cs`](../../Assets/UnityExtras/Scripts/Constants/DebugMessages.cs) - defines common messages used for debug logging
	1. [`UnityProperties.cs`](../../Assets/UnityExtras/Scripts/Constants/UnityProperties.cs) - defines constants specific to the Unity Scripting API