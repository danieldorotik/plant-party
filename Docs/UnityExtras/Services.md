## Services
Services is a UnityExtras approach to handle global states in [IoC][IoC] fashion without any singletons.

In a nutshell, given **service** instance is maintained by its **service provider** instance, which is a `UnityEngine.ScriptableObject` instance binded into an .asset file. Every **client** code provided with a reference to this asset will in effect use the same **service provider** instance (see the [ScriptableObject][ScriptableObject] docs), through which it will reach the **service** instance.

This approach allows for service to be any `UnityEngine.Object`, for example, a prefab instance. This way a service can have an asset-style configuration and also receive Update, LateUpdate, etc. calls. 

UnityExtras Services also provides means to explicitly instantiate service and to prevent it from being garbage-collected when it's no longer referenced by any live client code.

## Example

The example below shows how to use a custom functionality as a UnityExtras Service.

We have created a script *MyService*, added it into a prefab, configured its public members and now we would like to use it as a UnityExtras Service:

1. First, we create a class *MyServiceProvider*, which is a descendant of [`PrefabServiceProvider<T>`][PrefabServiceProvider] and use *MyService* as its generic type. Since it is also a `UnityEngine.ScriptableObject`, we will store its instance into a *MyServiceProvider.asset* file.

    * **Note #1:** The [`PrefabServiceProvider<T>`][PrefabServiceProvider] will take care of lazy initialization of our *MyService*. It has a public member *servicePrefab* of type `UnityEngine.GameObject`, from which it will create the cloned instance. It also searches this instance for an occurrence of our *MyService* script, which it returns when asked for the Instance.

    * **Note #2:** The [`PrefabServiceProvider<T>`][PrefabServiceProvider] has an optional functionality to make our service permanent, which means it's kept from garbage collection in case no live client code references it. It achieves this by using the [`PermanentService`][PermanentService] static functionality. This functionality creates a `DontDestroyOnLoad` GameObject with a script containing references to the registered service providers. The GameObject also serves as a parent for the prefab service instances in order to keep them from being destroyed during a scene change.

    * **Note #3:** The UnityExtras Services approach does not require the use of [`PrefabServiceProvider<T>`][PrefabServiceProvider]. It only requires a service provider to implement the [`IServiceProvider<T>`][IServiceProvider] interface. Thus a service can be any `UnityEngine.Object` type.

1. With our service provider instance stored in the .asset file, we will provide it to any client code which will use our service. The client shall have a public member of type `UnityEngine.Object`, so it can receive a reference to our *MyServiceProvider.asset*. Then the client can use [`AsServiceProvider<T>`][ServiceExtensions] extension method to easily convert the service provider object into [`IServiceProvider<T>`][IServiceProvider] interface, respectively into `IServiceProvider<MyService>` in our case. Through this interface (specifically through its `Instance` property) the client can start to use our *MyService* service.

    * **Note:** Since Awake effectively serves as a MonoBehaviour constructor, it's a good rule of thumb to avoid calling the `Instance` property of [`IServiceProvider<T>`][IServiceProvider] type in Awake method. That's because services can be configured to use one another and attempting this while not yet being fully instantiated would result in a stack overflow.

## Explicit instantiation

As UnityExtras Services approach uses lazy initialization through the [`PrefabServiceProvider<T>`][PrefabServiceProvider], a service is created the first time it is used by a client code. Nevertheless, there are cases in which the service must be started at a given point. For such occasion, there is an [`Initializer`][Initializer] `MonoBehaviour` script.

[`Initializer`][Initializer] has a public List of `UnityEngine.Object` references, expected to be the service provider(s) for initialization. Since the [`IServiceProvider<T>`][IServiceProvider] inherits from an [`IInitializable`][IInitializable], the [`Initializer`][Initializer] will attempt to initialize the assigned references through the [`IInitializable`][IInitializable] interface. In case of [`PrefabServiceProvider<T>`][PrefabServiceProvider] descendant or of a type which properly implements the [`IServiceProvider<T>`][IServiceProvider] interface, this will result in the creation of a service instance.

* **Note:** The UnityExtras Services approach does not require the use of [`PrefabServiceProvider<T>`][PrefabServiceProvider]. It only requires a service provider to implement the [`IServiceProvider<T>`][IServiceProvider] interface.

## Appendix A: Persistent callback

It is possible to set up a persistent (serialized) link to a given method of a UnityExtras Service, for example from an `onClick` event of `UI.Button`. That is because Unity can serialize callbacks through its `UnityEvent` type.

To make use of this functionality, we add a method to our service provider which will relay the call to its service instance. This method would act as a proxy. See the example below:

    [CreateAssetMenu]
    public class ServiceXYZProvider : PrefabServiceProvider<ServiceXYZ>
    {
        public void MethodX()
        {
            Instance.MethodX();
        }
    }
    
...then we need to store a service provider instance into an .asset file (see [`CreateAssetMenu`](https://docs.unity3d.com/ScriptReference/CreateAssetMenuAttribute.html)), so its proxy method can be permanently linked with the desired `UnityEvent`.

It's also possible to hookup a state of a Unity Animator Controller to a given service method through a [`CustomStateMachineBehavior`](../../Assets/UnityExtras/Scripts/CustomStateMachineBehavior.cs).

## Associated sources
1. [`PrefabServiceProvider.cs`][PrefabServiceProvider]
1. [`PermanentService.cs`][PermanentService]
1. [`IServiceProvider.cs`][IServiceProvider]
1. [`ServiceExtensions.cs`][ServiceExtensions]
1. [`Initializer.cs`][Initializer]
1. [`IInitializable.cs`][IInitializable]

[IoC]: https://en.wikipedia.org/wiki/Inversion_of_control
[ScriptableObject]: https://docs.unity3d.com/Manual/class-ScriptableObject.html
[PrefabServiceProvider]: ../../Assets/UnityExtras/Scripts/ScriptableObjects/PrefabServiceProvider.cs
[PermanentService]: ../../Assets/UnityExtras/Scripts/PermanentService.cs
[IServiceProvider]: ../../Assets/UnityExtras/Scripts/IServiceProvider.cs
[ServiceExtensions]: ../../Assets/UnityExtras/Scripts/ServiceExtensions.cs
[Initializer]: ../../Assets/UnityExtras/Scripts/MonoBehaviours/Initializer.cs
[IInitializable]: ../../Assets/UnityExtras/Scripts/IInitializable.cs