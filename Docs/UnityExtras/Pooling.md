## Pooling
UnityExtras provides the following generic pooling functionality:

1. [`GroupPool<TItem, TGroup>`][GroupPool] - base class for pooling of `TItem` generic types, separated into groups identified by a `TGroup` generic type.
    * For example, a descendant class can utilize the `TGroup` as a `UnityEngine.GameObject`, so it can pool its items based on a prefab reference (see [`GameObjectPool`][GameObjectPool] for such implementation).
    * This class is a `MonoBehaviour`.
1. [`Pool<T>`][Pool] - a descendant of the [`GroupPool<TItem, TGroup>`][GroupPool], useful for cases where grouping is not necessary.
1. [`RecyclingList<T>`][RecyclingList] - a generic collection similar to List with the support of item reuse, having an O(1) complexity. 
    * basically a lean pool, useful for example as an intermediate local pool in cases when a frequent use of global pooling would result in performance overhead (e.g. GameObject reparenting). 
    * Does not inherit anything, but implements the standard `ICollection<T>` interface.
    * Despite being a custom collection, has a **GC-free foreach usage** and can be edited while being iterated (Recycle & Dismiss methods).

The included [`GameObjectPool`][GameObjectPool] is a basic pool of `UnityEngine.GameObject` instances which are grouped by a `UnityEngine.GameObject` reference (e.g. a prefab). This pool is a descendant of [`GroupPool<TItem, TGroup>`][GroupPool].

## Associated sources
1. [`GroupPool.cs`][GroupPool]
1. [`Pool.cs`][Pool]
1. [`RecyclingList.cs`][RecyclingList]
1. [`GameObjectPool.cs`][GameObjectPool]

[GroupPool]: ../../Assets/UnityExtras/Scripts/MonoBehaviours/GroupPool.cs
[Pool]: ../../Assets/UnityExtras/Scripts/MonoBehaviours/Pool.cs
[RecyclingList]: ../../Assets/UnityExtras/Scripts/RecyclingList.cs
[GameObjectPool]: ../../Assets/UnityExtras/Scripts/MonoBehaviours/GameObjectPool.cs