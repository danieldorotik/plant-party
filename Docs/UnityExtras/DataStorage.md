## Data storage
One of the convenient ways to handle data (player's progress, game settings, level data, etc.) in Unity is to use `[Serializable]` structs or classes together with a `ScriptableObject`, binded into .asset files. This way data is compatible with Unity's serializer and can be inspected/edited in the Inspector window of Unity Editor.

The problem arises when data is changed during a standalone runtime session and has to be stored/restored between sessions. Storing into .asset is not possible at this point thus the data has to be stored somewhere else (for example as a file placed in the [`Application.persistentDataPath`](https://docs.unity3d.com/ScriptReference/Application-persistentDataPath.html)).

With  **UnityExtras Data storage** model we tackle this problem by defining a [`StoredObject<T>`][StoredObject] for our specific `ScriptableObject` which can be used to store/restore our object from an arbitrary DataSource through an arbitrary Serializer. The model bases its simplicity upon a fact that the Unity Editor serves as a kind of a dependency injector by allowing to conveniently set up object configuration including the references to the other set up objects.

The model is explained by the following **example**:

1. We have created a class *GameProgress* which we want to store/restore during runtime. The class is a `ScriptableObject` (thus also an `UnityEngine.Object`). We have prepared its default instance and saved it into a *GameProgress.asset* file.

1. For the *GameProgress* data storage, we've created a *StoredGameProgress* class, which descends from the [`StoredObject<T>`][StoredObject] generic class using the *GameProgress* as its generic type. Since the [`StoredObject<T>`][StoredObject] is also a `ScriptableObject`, we have saved our *StoredGameProgress* instance in a *StoredGameProgress.asset* file.

1. Since our *StoredGameProgress* instance is a [`StoredObject<T>`][StoredObject], it expects to be provided with a *dataSource* `UnityEngine.Object` link and with an *originalObject* (*GameProgress* type) link. When it is used to **restore** the *GameProgress* instance, it either uses the *dataSource* (expects the [`IDataSource`][IDataSource] interface) or makes a clone of the *originalObject* (if the *dataSource* reports no found instance). When it's used to **store** the *GameProgress* instance, it again uses the *dataSource* through the [`IDataSource`][IDataSource] interface.

Because the [`IDataSource`][IDataSource] is an Object-based (instead of type-specific) interface, it allows having universal and simple data sources like the [`FileDataSource`][FileDataSource]. This one serializes/deserializes an object from a specified file *path* using the provided *serializer* `UnityEngine.Object`. It expects the *serializer* to implement the  [`ISerializer`][ISerializer] interface (see [`JsonSerializer`][JsonSerializer]).

In the example above, we have made the following asset files:

* *GameProgress.asset* | the default instance of our *GameProgress* class
* *StoredGameProgress.asset* | handles storing/restoring of our *GameProgress* based on the original *GameProgress.asset* and the *GameProgressFile.asset* data source
    * Note: objects which are going to use the *GameProgress* instance are meant to be provided with a link to this asset. The [`StoredObject<T>`][StoredObject] descendant serves as a kind of a dependency injector/loader/storage
* *GameProgressFile.asset* | a [`FileDataSource`][FileDataSource] instance set with a specific file *path* and provided with a link to the *JsonSerializer.asset*
* *JsonSerializer.asset* | a [`JsonSerializer`][JsonSerializer] instance - only one asset file out of this class is needed in total, as it's universal and has no settings)

## Associated sources
1. [`StoredObject.cs`][StoredObject]
1. [`IStoredObject.cs`][IStoredObject]
1. [`IDataSource.cs`][IDataSource]
1. [`FileDataSource.cs`][FileDataSource]
1. [`ISerializer.cs`][ISerializer]
1. [`JsonSerializer.cs`][JsonSerializer]

[IoC]: https://en.wikipedia.org/wiki/Inversion_of_control
[StoredObject]: ../../Assets/UnityExtras/Scripts/ScriptableObjects/StoredObject.cs
[IStoredObject]: ../../Assets/UnityExtras/Scripts/IStoredObject.cs
[IDataSource]: ../../Assets/UnityExtras/Scripts/IDataSource.cs
[FileDataSource]: ../../Assets/UnityExtras/Scripts/ScriptableObjects/FileDataSource.cs
[ISerializer]: ../../Assets/UnityExtras/Scripts/ISerializer.cs
[JsonSerializer]: ../../Assets/UnityExtras/Scripts/ScriptableObjects/JsonSerializer.cs