## Span

The [`Span`][Span] value type is suitable for defining a continuous interval specified by two arbitrary `float` values. It also contains static methods for span Intersection and Union.

## SpanDrawer

There is a `PropertyDrawer` descendant called [`SpanDrawer`][SpanDrawer], which visualizes the [`Span`][Span] type in the Unity editor as illustrated below:

![SpanDrawer](../Media/SpanDrawer.png "SpanDrawer")

This drawer supports the [`SpanRangeAttribute`][SpanRangeAttribute], a custom attribute for the [`Span`][Span] type. It uses it to conveniently represent a span as a MinMaxSlider UI editor drawer and also to check the deserialized span values/clamp edited values.

The picture below shows a tooltip information displayed when hovering over a [`SpanDrawer`][SpanDrawer] notifying about a deserialized [`Span`][Span] values violation of the limits specified by a [`SpanRangeAttribute`][SpanRangeAttribute]:

![SpanDrawer range violation](../Media/SpanDrawer_outOfRange.png "SpanDrawer range violation")

## Associated sources
1. [`Span.cs`][Span]
1. [`SpanDrawer.cs`][SpanDrawer]
1. [`SpanRangeAttribute.cs`][SpanRangeAttribute]

[Span]: ../../Assets/UnityExtras/Scripts/Span.cs
[SpanDrawer]: ../../Assets/UnityExtras/Scripts/Editor/SpanDrawer.cs
[SpanRangeAttribute]: ../../Assets/UnityExtras/Scripts/SpanRangeAttribute.cs
